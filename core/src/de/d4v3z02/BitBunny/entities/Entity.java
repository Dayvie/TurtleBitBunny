package de.d4v3z02.BitBunny.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.levelhandlers.Animation;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public abstract class Entity {

	protected Sprite unanmitedSprite;
	protected ContentManager contentManager;
	protected SpriteBatch sb;
	protected Body body;
	protected Animation animation;
	protected float width;
	protected float height;
	protected World world;
	protected PolygonShape shape;
	protected volatile boolean removeFlag;
	
	public Entity(){
		contentManager = ContentManager.getInstance();
		animation = new Animation();
	}
	
	public SpriteBatch getSpriteBatch(){
		return this.sb;
	}
	
	public Entity(World world, SpriteBatch sb){
		contentManager = ContentManager.getInstance();
		this.world = world;
		this.sb = sb;
		animation = new Animation();
	}
	
	public boolean shouldBeRemoved() {
		return removeFlag;
	}

	public void remove(){
		this.removeFlag = true;
	}
	
	public void setAnimation(Sprite reg, float delay) {
		setAnimation(new Sprite[] { reg }, delay);
	}
	
	public void setAnimation(Sprite[] reg, float delay){
		animation.setFrames(reg, delay);
	}
	
	public void update(float dt){
		animation.update(dt);
	}
	
	public void render(){
		sb.begin();
		sb.draw(animation.getFrame(),
				body.getPosition().x*Numbers.PPM.value - width /2,
				body.getPosition().y*Numbers.PPM.value - height /2);
		sb.end();
	}
	
	/**
	 * Loads the Texturefile for the class and waits until its finished loading
	 * @param path of the SpriteTexture
	 */
	protected void loadTextureFile(String path){
		contentManager.preLoadSafely(path, Texture.class);
	}
	
	public Body getBody(){
		return this.body;
	}
	
	public Vector2 getPosition(){
		return body.getPosition();
	}
	
	public float getWidth(){
		return width;
	}
	
	public float getHeight(){
		return height;
	}

	/**
	 * @param path of the loaded Texture
	 * @param pictureScaleX width of the sprite
	 * @param pictureScaleY height of the sprite
	 */
	protected void loadTexture(String path, int pictureScaleX, int pictureScaleY){
		width = pictureScaleX;
		height = pictureScaleY;
		loadTextureFile(path);
		Texture tex = contentManager.get(path, Texture.class);
		TextureRegion[] textureRegions = TextureRegion.split(tex, pictureScaleX, pictureScaleY)[0];
		unanmitedSprite = new Sprite(textureRegions[0]);
		Sprite[] animationSprites = new Sprite[textureRegions.length];
		for (int j = 0; j < textureRegions.length; j++) {
			animationSprites[j] = new Sprite(textureRegions[j]);
		}
		setAnimation(animationSprites, 1 /12f);
	}
	
	/**
	 * 
	 * @param path of the loaded Texture
	 * @param pictureScaleX width of the sprite
	 * @param pictureScaleY height of the sprite
	 * @param indexX x index on the spritemap
	 * @param indexY y index on the spritemap
	 */
	protected void loadTexture(String path, int pictureScaleX, int pictureScaleY, int indexX,  int indexY){
		width = pictureScaleX;
		height = pictureScaleY;
		loadTextureFile(path);
		Texture tex = contentManager.get(path, Texture.class);
		TextureRegion[] textureRegions = TextureRegion.split(tex, pictureScaleX, pictureScaleY)[indexY];
		unanmitedSprite = new Sprite(textureRegions[indexX]);
		Sprite[] animationSprites = new Sprite[textureRegions.length];
		for (int j = indexX; j < textureRegions.length; j++) {
			animationSprites[j] = new Sprite(textureRegions[j]);
		}
		setAnimation(animationSprites, 1 /12f);
	}
	
	/**
	 * 
	 * @param path of the loaded Texture
	 * @param pictureScaleX width of the sprite
	 * @param pictureScaleY height of the sprite
	 * @param indexX x index on the spritemap
	 * @param indexY y index on the spritemap
	 * @param xBorder the x index where the animation stops
	 */
	protected void loadTexture(String path, int pictureScaleX, int pictureScaleY, int indexX,  int indexY, int xBorder){
		width = pictureScaleX;
		height = pictureScaleY;
		loadTextureFile(path);
		Texture tex = contentManager.get(path, Texture.class);
		TextureRegion[] textureRegions = TextureRegion.split(tex, pictureScaleX, pictureScaleY)[indexY];
		unanmitedSprite = new Sprite(textureRegions[indexX]);
		Sprite[] animationSprites = new Sprite[xBorder-indexX+1];
		int i = 0;
		for (int j = indexX; j <= indexX+(xBorder-indexX); j++) {
			animationSprites[i] = new Sprite(textureRegions[j]);
			i++;
		}
		setAnimation(animationSprites, 1 /12f);
	}
	
	protected void createBody(){
	}
}