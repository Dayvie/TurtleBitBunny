package de.d4v3z02.BitBunny.entities;
//test
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Pool.Poolable;

import de.d4v3z02.BitBunny.entities.characters.Character;
import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.entities.characters.enemies.ShootingSici;
import de.d4v3z02.BitBunny.entities.characters.enemies.BigSiciBoss;
import de.d4v3z02.BitBunny.entities.characters.enemies.ShootingSiciBoss;
import de.d4v3z02.BitBunny.entities.characters.enemies.WSSici;
import de.d4v3z02.BitBunny.fixtureDefs.EnemyProjectileDef;
import de.d4v3z02.BitBunny.fixtureDefs.PlayerProjectileDef;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.levelhandlers.Animation;
import de.d4v3z02.BitBunny.informer.enums.Numbers;
/**
 * 
 * @author David
 *	Projectiles are held in a pool container when they are inactive
 */
public class Projectile extends Entity implements Poolable {

	private Character character;
	private float x;
	private float y;
	private boolean facesRight;
	private volatile Body characterBody;
	private float timePassed;
	private volatile BodyDef bodyDef;
	
	public Projectile(World world, SpriteBatch sb, Character character, String texturePath, int textureWidth, int textureHeight) {
		super(world, sb);
		this.character = character;
		this.characterBody = character.getBody();
		this.facesRight = character.facesRight();
		loadTexture(texturePath, textureWidth, textureHeight);
		createBody(character);
	}
	
	@Override 
	public void render(){
		sb.begin();
		if(facesRight){
			sb.draw(animation.getFrame(),
					body.getPosition().x*Numbers.PPM.value - width /2,
					body.getPosition().y*Numbers.PPM.value - height /2);	
		}else{
			sb.draw(animation.getFrame(),
					body.getPosition().x*Numbers.PPM.value - width /2 + width, 
					body.getPosition().y*Numbers.PPM.value - height /2,
					-width, height);
		}
		sb.end();
	}
	
	@Override
	public void update(float dt){
		animation.update(dt);
		timePassed += dt;
		if(timePassed > 1.5){
			removeFlag = true;
			character.decreaseActiveProjectilesNum();
		}
	}
	
	@Override
	public void remove(){
		removeFlag = false;
		character.decreaseActiveProjectilesNum();
	}
	
	public float getTimePassed(){
		return timePassed;
	}
	
	protected void createBody(Character character){
		x = characterBody.getPosition().x;
		y = characterBody.getPosition().y;
		bodyDef = new BodyDef();
		bodyDef.type= BodyType.DynamicBody;
		bodyDef.position.set(x, y);
		body = world.createBody(bodyDef);
		CircleShape cShape = new CircleShape();
		if(character.getClass() == Player.class){
			body.createFixture(new PlayerProjectileDef(cShape)).setUserData("playerFireBall");
		}else{
			if((character.getClass() == ShootingSici.class)	|| (character.getClass() == BigSiciBoss.class
					|| (character.getClass() == WSSici.class) || character.getClass() == ShootingSiciBoss.class)){
				body.createFixture(new EnemyProjectileDef(cShape)).setUserData("enemyFireBall");
			}
		}
		body.setUserData(this);
		body.setGravityScale(0.0f);
		bodyDef = null;
		cShape.dispose();
		if(facesRight){
			body.setLinearVelocity(character.getProjectileSpeed() , 0);
		}else{
			body.setLinearVelocity(-character.getProjectileSpeed(), 0);
		} 
		body.setBullet(true);
	}
	
	public Character getCharacter(){
		return this.character;
	}

	@Override
	public void reset() {
		body.setActive(false);
		body.setLinearVelocity(0, 0);
		bodyDef = null;
		shape = null;
		width = 0;
		height = 0;
		facesRight = false;
		characterBody = null;
		world = null;
		sb = null;
		unanmitedSprite = null;
		animation = null;
		removeFlag = false;
		x= 0;
		y = 0;
		timePassed = 0;
		contentManager = null;
		character = null;
	}
	
	public void initiation(World world, Character character,  SpriteBatch sb, String texturePath, int textureWidth, int textureHeight){
		contentManager = ContentManager.getInstance();
		this.character = character;
		body.setActive(true);
		animation = new Animation();
		loadTexture(texturePath, textureWidth, textureHeight);
		this.world = world;
		characterBody = character.getBody();
		this.sb = sb;
		x = characterBody.getPosition().x;
		y = characterBody.getPosition().y;
		body.setTransform(x, y, 1);
		facesRight = character.facesRight();
		if(facesRight){
			body.setLinearVelocity(character.getProjectileSpeed() , 0);
		}else{
			body.setLinearVelocity(-character.getProjectileSpeed(), 0);
		}
	}
}