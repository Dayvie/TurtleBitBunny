package de.d4v3z02.BitBunny.entities.characters;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.handlers.levelhandlers.ContactManager;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.FloatingCombatTextHolder;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.RisingCombatTextNumber;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.StaticCombatTextNumber;
import de.d4v3z02.BitBunny.informer.enums.CharacterState;
import de.d4v3z02.BitBunny.informer.enums.ElementalTypes;
import de.d4v3z02.BitBunny.informer.enums.Fonts;
import de.d4v3z02.BitBunny.informer.enums.Numbers;


public abstract class Character extends Entity {
	/*
	 * Characters are Entities which influence the gameplay like enemies, the player and other npcs
	 */
	protected MusicHandler musicHandler;
	protected int activeProjectilesNum;
	protected boolean facesRight;
	protected ContactManager contactManager;
	protected float walkingSpeed;
	protected float runningSpeed;
	protected int jumpHeight;
	protected CharacterState characterState;
    protected float accumulator;
	protected int projectileSpeed;
	protected int hp;
	protected int maxHp;
	protected int BodyDmg;
	protected int projectileDmg;
	protected boolean isActive;
	protected FloatingCombatTextHolder floatingCombatTextHolder;
	protected Map<ElementalTypes, Integer> eMultiplier;
	protected Map<ElementalTypes, Integer> eResistances;
	
	public Character(World world, SpriteBatch sb) {
		super(world, sb);
		eMultiplier = new HashMap<>();
		eResistances = new HashMap<>();
		musicHandler = MusicHandler.getInstance();
		maxHp = 2;
		projectileDmg = 1;
		hp = maxHp;
		BodyDmg = 1;
		facesRight = true;
		walkingSpeed = 1f;
		runningSpeed = 2f;
		jumpHeight = 25;
		projectileSpeed = 8;
		contentManager = ContentManager.getInstance();
		contactManager = ContactManager.getInstance();
		characterState = CharacterState.STANDING;
		setStandardEMultipliers();
		setStandardResistances();
		floatingCombatTextHolder = FloatingCombatTextHolder.getInstance();
	}
	
	private void setStandardEMultipliers(){
		for(ElementalTypes eType : ElementalTypes.values()){
			eMultiplier.put(eType, 1);
		}
	}

	public int getJumpHeight(){
		return  jumpHeight;
	}

	public void shoot(){
	}
	
	private void setStandardResistances(){
		for(ElementalTypes eType : ElementalTypes.values()){
			eResistances.put(eType, 0);
		}
	}

	public boolean isActive(){
		return isActive;
	}
	
	public int getProjectileDmg(){
		return this.projectileDmg;
	}
	
	public int getActiveProjectilesNum(){
		return this.activeProjectilesNum;
	}
	
	public void increaseActiveProjectilesNum(){
		activeProjectilesNum++;
	}
	
	public void decreaseActiveProjectilesNum(){
		activeProjectilesNum--;
	}
	
	public int getMaxHp(){
		return this.maxHp;
	}
	
	public void increaseMaxHp(){
		maxHp++;
	}
	
	public int getBodyDmg() {
		return BodyDmg;
	}

	public void setBodyDmg(int bodyDmg) {
		BodyDmg = bodyDmg;
	}

	public int getHP() {
		return hp;
	}
	
	public void increaseHP(){
		if(hp + 1 <= maxHp){
			hp++;
			floatingCombatTextHolder.add(new RisingCombatTextNumber(1, Fonts.HEAL.font, sb, new Vector2(body.getPosition())));
		}
	}

	public void increaseAccumulator(float dt){
		accumulator += dt;
	}

	public void decreaseAccumulator(float dt){
		accumulator -= dt;
	}

	public float getAccumulator(){
		return accumulator;
	}
	
	public void increaseHP(int heal){
		if(hp + heal <= maxHp){
			hp+=heal;
			floatingCombatTextHolder.add(new RisingCombatTextNumber(heal, Fonts.HEAL.font, sb, new Vector2(body.getPosition())));
		}else{
			hp = maxHp;
		}
	}
	
	public void decreaseHP(int damage, ElementalTypes eType){
		int calculatedDamage = (damage*eMultiplier.get(eType)) - eResistances.get(eType);
		if(calculatedDamage > 0){
			hp-=calculatedDamage;
			floatingCombatTextHolder.add(new StaticCombatTextNumber(calculatedDamage, eType.font, sb, new Vector2(body.getPosition())));
		}
		playHitSound();
		checkForDeath();
	}
	
	public void checkForDeath(){
		if(hp<=0){
			removeFlag = true;
		}
	}
	
	protected void playHitSound(){
		musicHandler.playSound(contentManager.get("res/sfx/hit.wav", Music.class)); 
	}
	
	public int getProjectileSpeed(){
		return projectileSpeed;
	}
	
	@Override
	public void render(){
		sb.begin();
		switch (characterState){
		case STANDING:
			drawStanding();	
			break;
		case WALKING:
			drawWalking();
			break;
		case RUNNING:
			drawWalking();
			break;
		case FROZEN:
			drawStanding();
			break;
		default:
			break;
		}
		sb.end();
	}
	
	protected void drawWalking(){
		if(facesRight){	
			sb.draw(animation.getFrame(),
					body.getPosition().x*Numbers.PPM.value - width /2, 
					body.getPosition().y*Numbers.PPM.value - height /2,
					width, height);
		}else{
			sb.draw(animation.getFrame(),
					body.getPosition().x*Numbers.PPM.value - width /2 +width, 
					body.getPosition().y*Numbers.PPM.value - height /2,
					-width, height);
		}
	}
	
	protected void drawStanding(){
		if(facesRight){
			sb.draw(unanmitedSprite, 
					body.getPosition().x*Numbers.PPM.value - width /2,
					body.getPosition().y*Numbers.PPM.value - height /2,
					width, height);
		}else{
			sb.draw(unanmitedSprite,
					body.getPosition().x*Numbers.PPM.value - width /2 + width, 
					body.getPosition().y*Numbers.PPM.value - height /2,
					-width, height);
		}
	}
	
	public boolean facesRight(){
		return facesRight;
	}
	
	public void setFacesRight(boolean b){
		this.facesRight = b;
	}
	
	public void jump() {
		body.applyForceToCenter(0, jumpHeight, true);	
	}
	
	public void moveRight(){
		facesRight= true;
		if(characterState == CharacterState.RUNNING){
			body.setLinearVelocity(runningSpeed, body.getLinearVelocity().y);
			return;
		}
		characterState = CharacterState.WALKING;
		body.setLinearVelocity(walkingSpeed, body.getLinearVelocity().y);
	}

    public boolean doesMoveLeft(){
        return true;
    }
	
	public void moveLeft(){
		facesRight = false;
		if(characterState == CharacterState.RUNNING){
			body.setLinearVelocity(-runningSpeed, body.getLinearVelocity().y);
			return;
		}
		characterState = CharacterState.WALKING;
		body.setLinearVelocity(-walkingSpeed, body.getLinearVelocity().y);
	}
	
	public Body getBody() {
		return this.body;
	}

	public void setState(CharacterState state){
		this.characterState = state;
	}
	
	public void setRunSpeed(float speed){
		runningSpeed = speed;
	}
	
	public void setWalkSpeed(float speed){
		walkingSpeed = speed;
	}
}
