package de.d4v3z02.BitBunny.entities.characters;

 import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Checkpoint;
import de.d4v3z02.BitBunny.entities.entitiesonmap.SwitchBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.SwitchBlock.Color;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.filehandlers.disposeMusicListener;
import de.d4v3z02.BitBunny.handlers.levelhandlers.Animation;
import de.d4v3z02.BitBunny.handlers.levelhandlers.ColorHolder;
import de.d4v3z02.BitBunny.handlers.levelhandlers.ContactManager;
import de.d4v3z02.BitBunny.handlers.levelhandlers.PlayerAnimation;
import de.d4v3z02.BitBunny.handlers.levelhandlers.EntityService;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.RisingCombatTextNumber;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.StaticCombatTextNumber;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.CharacterState;
import de.d4v3z02.BitBunny.informer.enums.ElementalTypes;
import de.d4v3z02.BitBunny.informer.enums.Fonts;
import de.d4v3z02.BitBunny.informer.enums.LevelAsset;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

/**
 * represents the player in the game
 */
public class Player extends Character {
	
	private boolean isWhite;
	private Map<String, Animation> animations = new HashMap<>();
	private boolean invincible;
	private float invincibilityCounter;
	private GameFile gameFile;
	private CharacterState oldState;
	private World world;
	private Vector2 spawnPosition;
	private ContactManager contactManager = ContactManager.getInstance();
	private SwitchBlock.Color color;
	private ColorHolder colorSlave = ColorHolder.getInstance();
	private Array<Projectile> activePlayerProjectiles = new Array<Projectile>();
	private Pool<Projectile> fireBallPool = new Pool<Projectile>(){
		@Override
	    protected Projectile newObject() {
	        return new Projectile(world, sb, Player.this, "res/images/fireball.png", 32 ,32);
	    }
	};
	
	public Player(World world, SpriteBatch sb, GameFile gameFile) {
		super(world, sb);
		this.gameFile = gameFile;
		this.projectileDmg = gameFile.getFireBallDmg();
		this.world = world;
		loadAllAnimations();
		maxHp = gameFile.getPlayerMaxHp();
		hp = gameFile.getPlayerHp();
		color = Color.RED;
		activatePowerUp();
		determinateSpawnPosition();
		createBody();
	}
	
	private void activateCheckpointOnPosition(){
		Entity entity = EntityService.getInstance().getCheckpoint(gameFile.getCheckpointID());
		spawnPosition = entity.getBody().getPosition();
		((Checkpoint)entity).activate();
	}
	
	public void freeze(){
		oldState = characterState;
		characterState = CharacterState.FROZEN;
		body.setLinearVelocity(0, 0);
	}
	
	public void unfreeze(){
		characterState = oldState;
	}
	
	public void collectGold(int gold){
		gameFile.increaseGold(gold);
		floatingCombatTextHolder.add(new RisingCombatTextNumber(gold, Fonts.PLAYERMONEY.font, sb,
				new Vector2(body.getPosition())));
	}
	
	public boolean spendGold(int gold){
		return gameFile.decreaseGold(gold);
	}
	
	public GameFile getGameFile(){
		return this.gameFile;
	}
	
	public Array<Projectile> getActiveProjectiles(){
		return this.activePlayerProjectiles;
	}
	
	public void setSpawnPosition(Vector2 spawnPosition){
		this.spawnPosition = spawnPosition;
	}

	public PowerUpState getPowerUp(){
		return gameFile.getLastPowerUp();
	}

	public Color getColor(){
		return this.color;
	}
	
	public CharacterState getState(){
		return this.characterState;
	}
	
	public Pool<Projectile> getFireBallPool(){
		return this.fireBallPool;
	}
	
	@Override
	public void checkForDeath(){
		if(hp<=0){
			removeFlag = true;
		}
	}
	
	public void collectHearthcontainer(){
		maxHp++;
		hp++;
		gameFile.saveGameFile();
	}
	
	@Override
	public void decreaseHP(int damage, ElementalTypes eType){
		if(!invincible){
			animation = animations.get(PlayerAnimation.WHITE.name());
			unanmitedSprite = animation.getUnanimatedSprite();
			isWhite = true;
			playHitSound();
			int calculatedDamage = (damage*eMultiplier.get(eType)) - eResistances.get(eType);
			if(calculatedDamage > 0){
				playHitSound();
				hp-=calculatedDamage;
				floatingCombatTextHolder.add(new StaticCombatTextNumber(calculatedDamage, Fonts.DAMAGE.font,
						sb, new Vector2(body.getPosition())));
				checkForDeath();
			}
			removeEnemyCollision();
			invincible = true;
		}
	}
	
	private void removeEnemyCollision(){
		Array<Fixture> fixtures = body.getFixtureList();
		for(Fixture fixture: fixtures){
			Filter filter = fixture.getFilterData();
			filter.maskBits = (short)(filter.maskBits & ~Bits.ENEMY.value);
			fixture.setFilterData(filter);
		}
	}
	
	private void addEnemyCollision(){
		Array<Fixture> fixtures = body.getFixtureList();
		for(Fixture fixture: fixtures){
			Filter filter = fixture.getFilterData();
			filter.maskBits = (short)(filter.maskBits | Bits.ENEMY.value);
			fixture.setFilterData(filter);
		}
	}

	/**
	 * updates the player logic, is called by the framework
	 * @param dt passed time
     */
	public void update(float dt){
		animation.update(dt);
		if(invincible){
			invincibilityCounter +=dt;
			invincibilityFlickering();
			if(invincibilityCounter > 1.8){
				animation = animations.get(gameFile.getLastPowerUp().key);
				unanmitedSprite = animation.getUnanimatedSprite();
				invincible = false;
				addEnemyCollision();
				isWhite = false;
				invincibilityCounter = 0;
			}
		}
	}
	
	private int invincibilityFlickeringCounter;
	private int flickeringThreshold = 3;
	
	private void invincibilityFlickering(){
		invincibilityFlickeringCounter++;
		if(isWhite && invincibilityFlickeringCounter == flickeringThreshold){
			animateBlackAnimation();
			isWhite = false;
			invincibilityFlickeringCounter = 0;
		}
		if(!isWhite && invincibilityFlickeringCounter == flickeringThreshold){
			animateWhiteAnimation();
			isWhite = true;
			invincibilityFlickeringCounter = 0;
		}
	}
	
	private void animateBlackAnimation(){
		animation = animations.get(PlayerAnimation.BLACK.name());
		unanmitedSprite = animation.getUnanimatedSprite();
	}
	
	private void animateWhiteAnimation(){
		animation = animations.get(PlayerAnimation.WHITE.name());
		unanmitedSprite = animation.getUnanimatedSprite();
	}
	
	private void determinateSpawnPosition(){
		switch(gameFile.getSpawnPosition()){
		case END:
			spawnPosition = gameFile.getEndPos();
			break;
		case NORMAL:
			declareNormalSpawnPosition();
			break;
		case SECRETEND:
			spawnPosition = gameFile.getSEndPos();
			break;
		case CP:
			activateCheckpointOnPosition();
			break;
		default:
			break;
		}
	}
	
	private void declareNormalSpawnPosition(){
		int level = gameFile.getLastLevel();
		int index = 1;
		for(LevelAsset asset: LevelAsset.values()){
			if(level == index){
				spawnPosition = asset.spawn;
			}
			index++;
		}
	}
	
	public void deactivatePowerUp(PowerUpState p){
		switch(p){
		case BLUECHERRY:
			if(runningSpeed > 3){
				runningSpeed = runningSpeed/Numbers.SAWNIC.value;
			}
			break;
		case FIRECHERRY:
			break;
		case NONE:
			break;
		default:
			break;
		}
	}
	
	public void nextPowerUp(){
		deactivatePowerUp(gameFile.getLastPowerUp());
		gameFile.nextPowerUp();
		activatePowerUp();
	}
	
	private void activatePowerUp(){
		PowerUpState pow = gameFile.getLastPowerUp();
		switch(pow){
		case BLUECHERRY:
			runningSpeed = runningSpeed*Numbers.SAWNIC.value;
			break;
		case NONE:
			break;
		case FIRECHERRY:
			break;
		default:
			break;
	}
		activateAnimation(pow);
	}
	
	public void useAbillity(){
		PowerUpState pow = gameFile.getLastPowerUp();
		switch(pow){
		case BLUECHERRY:
			break;
		case NONE:
			break;
		case FIRECHERRY:
			shootFireBall();
			break;
		default:
			break;
		}
	}
	
	private void shootFireBall(){
		if(activeProjectilesNum < 4){
			activeProjectilesNum++;
			Projectile fireball = fireBallPool.obtain();
			fireball.initiation(world, this, sb, "res/images/fireball.png", 32, 32);
			activePlayerProjectiles.add(fireball);
			Music m = contentManager.get("res/sfx/smallFireball.mp3", Music.class);
			m.setVolume(0.2f);
			m.play();
			m.setOnCompletionListener(new disposeMusicListener());
		}
	}
	
	private void activateAnimation(PowerUpState pow){
		if(!invincible){
			switch(pow){
			case BLUECHERRY:
				animation = animations.get(PlayerAnimation.BLUE.name());
				unanmitedSprite = animation.getUnanimatedSprite();
				break;
			case FIRECHERRY:
				animation = animations.get(PlayerAnimation.ORANGE.name());
				unanmitedSprite = animation.getUnanimatedSprite();
				break;
			case NONE:
				animation = animations.get(PlayerAnimation.TEAL.name());
				unanmitedSprite = animation.getUnanimatedSprite();
				break;
			default:
				break;
			}
		}	
	}
	
	
	
	private void loadAllAnimations(){
		for (PlayerAnimation playerAnimation: PlayerAnimation.values()){
			loadPlayerAnimation(playerAnimation);
		}
	}
	
	private void loadPlayerAnimation(PlayerAnimation playerAnimation){
		Animation anim = new Animation();
		loadAnimation(anim, playerAnimation.textureMap, playerAnimation.widthInPixel, playerAnimation.heightInPixel, playerAnimation.indexX , playerAnimation.spriteMapIndexY, playerAnimation.lastSpriteIndex);
		animations.put(playerAnimation.name(), anim);
	}
	
	protected void loadAnimation(Animation animation, String path,
							   int widthInPixel, int heightInPixel, int indexX,  int spriteMapIndexY, int lastSpriteIndex){
		width = widthInPixel;
		height = heightInPixel;
		loadTextureFile(path);
		Texture tex = contentManager.get(path, Texture.class);
		TextureRegion[] textureRegions = TextureRegion.split(tex, widthInPixel, heightInPixel)[spriteMapIndexY];
		unanmitedSprite = new Sprite(textureRegions[indexX]);
		Sprite[] animationSprites = new Sprite[lastSpriteIndex - indexX];
		for (int j = indexX; j < lastSpriteIndex - indexX; j++) {
			animationSprites[j] = new Sprite(textureRegions[j]);
		}
		animation.setFrames(animationSprites, 1 /12f);
	}
	
	public void createBody(){
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(spawnPosition);
		bodyDef.fixedRotation = true;
		body = world.createBody(bodyDef);
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(13/ Numbers.PPM.value, 13/ Numbers.PPM.value);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.restitution = 0;
		fixtureDef.density = 1;
		fixtureDef.friction = 0;
		fixtureDef.filter.categoryBits = Bits.PLAYER.value;
		fixtureDef.filter.maskBits = (short) (Bits.RED.value | Bits.CRYSTAL.value
				| Bits.GROUND.value | Bits.ENEMY.value | Bits.PASSABLE.value| Bits.POWERUP.value);
		body.createFixture(fixtureDef).setUserData("player");
		body.setUserData(this);
		shape.dispose();
		/**
		 * Foot Sensor
		 */
		shape = new PolygonShape();
		shape.setAsBox(12 / Numbers.PPM.value, 3 / Numbers.PPM.value, new Vector2(0, -14 / Numbers.PPM.value), 0);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.FOOT.value;
		fixtureDef.filter.maskBits = (short) (Bits.RED.value | Bits.GROUND.value | Bits.PASSABLE.value | Bits.ENEMY.value);
		body.createFixture(fixtureDef).setUserData("foot");	
		shape.dispose();
		body.setUserData(this);
	}
	
	public void switchBlocks(){
		Filter filter = body.getFixtureList().
				first().getFilterData();
		short bits =  filter.maskBits;
		if((bits& Bits.RED.value) != 0){
			bits &= ~Bits.RED.value;
			bits |= Bits.GREEN.value;
		}else if((bits& Bits.GREEN.value) != 0){
			bits &= ~Bits.GREEN.value;
			bits |= Bits.BLUE.value;
		}else if((bits& Bits.BLUE.value) != 0){
			bits &= ~Bits.BLUE.value;
			bits |= Bits.RED.value;
		}
		filter.maskBits = bits;
		body.getFixtureList().first().setFilterData(filter);
		filter = body.getFixtureList().get(1).getFilterData();
		bits &= ~Bits.CRYSTAL.value;
		filter.maskBits = bits;
		body.getFixtureList().get(1).setFilterData(filter);
		switch(color){
			case BLUE:
				color = Color.RED;
				colorSlave.setColor(Color.RED);
				break;
			case GREEN:
				color = Color.BLUE;
				colorSlave.setColor(Color.BLUE);
				break;
			case RED:
				color = Color.GREEN;
				colorSlave.setColor(Color.GREEN);
				break;
			default:
				break;
		}
	}
	
	@Override
	public void jump() {
		if(characterState != CharacterState.FROZEN){
			if(contactManager.isPlayerOnGround()) {
				body.applyForceToCenter(0, jumpHeight, true);	
			}
		}
	}

	public enum PowerUpState{
		NONE(PlayerAnimation.TEAL.name()), BLUECHERRY(PlayerAnimation.BLUE.name()), FIRECHERRY(PlayerAnimation.ORANGE.name());
		public String key;
		PowerUpState(String s){
			key = s;
		}
	}
}