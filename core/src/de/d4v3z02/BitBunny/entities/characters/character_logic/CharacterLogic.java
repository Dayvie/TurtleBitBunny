package de.d4v3z02.BitBunny.entities.characters.character_logic;

import de.d4v3z02.BitBunny.entities.characters.Character;

/**
 * Created by dayvie on 01.05.2016.
 */
public abstract class CharacterLogic {

    protected Character character;

    public CharacterLogic(){
    }

    public CharacterLogic(Character character){
        this.character = character;
    }

    public abstract void executeLogic(float dt);
}
