package de.d4v3z02.BitBunny.entities.characters.character_logic;

import com.badlogic.gdx.math.Vector2;
import de.d4v3z02.BitBunny.entities.characters.Character;

/**
 * Created by dayvie on 01.05.2016.
 */
public class FlyingSiciLogic extends CharacterLogic {

    public FlyingSiciLogic(Character character){
        super();
        this.character = character;
    }

    @Override
    public void executeLogic(float dt) {
        if(character.isActive()){
            character.increaseAccumulator(dt);
            if(character.getAccumulator() >= 4){
                character.decreaseAccumulator(4);
            }
            if(character.getAccumulator() > 2){
                floatDown();
            }else{
                floatUp();
            }
        }
    }

    private void floatUp(){
        character.getBody().setLinearVelocity(new Vector2(0, character.getJumpHeight()/2));
    }

    private void floatDown(){
        character.getBody().setLinearVelocity(new Vector2(0, -character.getJumpHeight()/2));
    }
}
