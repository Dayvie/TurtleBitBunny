package de.d4v3z02.BitBunny.entities.characters.character_logic;

import de.d4v3z02.BitBunny.entities.characters.Character;

/**
 * Created by dayvie on 01.05.2016.
 */
public class JumpingSiciLogic extends CharacterLogic {

    public JumpingSiciLogic(Character character){
        super();
        this.character = character;
    }

    @Override
    public void executeLogic(float dt) {
        if(character.isActive()){
            character.moveLeft();
            character.increaseAccumulator(dt);
            if(character.getAccumulator() > 2){
                character.jump();
                character.decreaseAccumulator(2);
            }
        }
    }
}
