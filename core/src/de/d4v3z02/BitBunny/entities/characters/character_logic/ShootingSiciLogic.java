package de.d4v3z02.BitBunny.entities.characters.character_logic;

import de.d4v3z02.BitBunny.entities.characters.Character;

import java.util.Random;

/**
 * Created by dayvie on 01.05.2016.
 */
public class ShootingSiciLogic extends CharacterLogic {

    protected Random rng;
    protected float shootingDelay;

    public ShootingSiciLogic(Character character, float shootingDelay){
        super();
        this.character = character;
        rng = new Random();
        this.shootingDelay = shootingDelay;
    }

    @Override
    public void executeLogic(float dt) {
        if(character.isActive()){
            character.increaseAccumulator(dt);
            if(character.getAccumulator() >= shootingDelay){
                if(doesShoot()){
                    character.shoot();
                    character.decreaseAccumulator(shootingDelay);
                }
            }
        }
    }

    protected boolean doesShoot(){
        if((float)(rng.nextInt(8) / 7) == 1){
            return true;
        }
        return false;
    }
}
