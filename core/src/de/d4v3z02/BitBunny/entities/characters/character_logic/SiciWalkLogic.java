package de.d4v3z02.BitBunny.entities.characters.character_logic;

import de.d4v3z02.BitBunny.entities.characters.Character;

/**
 * Created by dayvie on 01.05.2016.
 */
public class SiciWalkLogic extends CharacterLogic{

    public SiciWalkLogic(Character character){
        super();
        this.character = character;
    }

    @Override
    public void executeLogic(float dt){
        if(this.character.doesMoveLeft()){
            this.character.moveLeft();
            this.character.checkForDeath();
        }else{
            this.character.moveRight();
            this.character.checkForDeath();
        }
    }
}
