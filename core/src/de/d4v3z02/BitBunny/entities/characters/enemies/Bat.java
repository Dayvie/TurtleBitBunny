package de.d4v3z02.BitBunny.entities.characters.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import de.d4v3z02.BitBunny.entities.characters.character_logic.FlyingSiciLogic;

import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.fixtureDefs.BatHead;
import de.d4v3z02.BitBunny.fixtureDefs.SiciBody;
import de.d4v3z02.BitBunny.fixtureDefs.SiciHead;
import de.d4v3z02.BitBunny.informer.enums.CharacterState;
import de.d4v3z02.BitBunny.informer.enums.ElementalTypes;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class Bat extends Enemy {

	public Bat(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles, boolean facesRight) {
		super(world, sb, mo, activeProjectiles, facesRight);
		jumpHeight = 2;
		characterState = CharacterState.WALKING;
		loadTexture("res/images/bat.png", 32, 32, 0 , 0, 4);
		eResistances.put(ElementalTypes.PHYSICAL, 1);
		createBody();
	}
	
	protected void drawWalking(){
		if(facesRight){	
			sb.draw(animation.getFrame(),
					body.getPosition().x*Numbers.PPM.value - width /2-15, 
					body.getPosition().y*Numbers.PPM.value - height /2,
					width, height);
		}else{
			sb.draw(animation.getFrame(),
					body.getPosition().x*Numbers.PPM.value - width /2 +width-15, 
					body.getPosition().y*Numbers.PPM.value - height /2,
					-width, height);
		}
	}

	@Override
	public void addLogic(){
		characterLogics.add(new FlyingSiciLogic(this));
	}

	protected void drawStanding(){
		if(facesRight){
			sb.draw(unanmitedSprite, 
					body.getPosition().x*Numbers.PPM.value - width /2-15,
					body.getPosition().y*Numbers.PPM.value - height /2,
					width, height);
		}else{
			sb.draw(unanmitedSprite,
					body.getPosition().x*Numbers.PPM.value - width /2 + width-15, 
					body.getPosition().y*Numbers.PPM.value - height /2,
					-width, height);
		}
	}

	@Override
	public void createBody(){	
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.KinematicBody;
		bodyDef.position.set(x, y);
		bodyDef.fixedRotation = true;
		body = world.createBody(bodyDef);
		this.shape = new PolygonShape();
		body.createFixture(new SiciBody(this.shape)).setUserData("enemy");
		this.shape = new PolygonShape();
		body.createFixture(new BatHead(this.shape)).setUserData("enemyhead");
		this.shape.dispose();
		body.setUserData(this);	
	}
}
