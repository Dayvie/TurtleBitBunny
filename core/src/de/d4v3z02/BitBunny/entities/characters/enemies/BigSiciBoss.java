package de.d4v3z02.BitBunny.entities.characters.enemies;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.fixtureDefs.SiciBossBody;
import de.d4v3z02.BitBunny.fixtureDefs.SiciBossHead;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.levelhandlers.EntityFactory;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class BigSiciBoss extends ShootingSici implements Boss {

	private float jumper;
	private Random rng;
	private EntityFactory entityFactory;
	private GameFile gameFile;
	
	public BigSiciBoss(World world, SpriteBatch sb, MapObject mo,
					   Array<Projectile> activeProjectiles, EntityFactory entityFactory, GameFile gameFile,
					   boolean facesRight) {
		super(world, sb, mo, activeProjectiles, facesRight);
		this.gameFile = gameFile;
		rng = new Random();
		this.entityFactory = entityFactory;
		facesRight = true;
		movesLeft = true;
		height = 64;
		width = 64;
		maxHp = 100;
		hp = 100;
		jumper = 3;
		jumpHeight = 75;
		walkingSpeed = walkingSpeed*3;
	}
	
	@Override
	public void createBody(){	
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(x, y);
		bodyDef.fixedRotation = true;
		body = world.createBody(bodyDef);
		this.shape = new PolygonShape();
		shape = new PolygonShape();
		body.createFixture(new SiciBossBody(shape)).setUserData("enemy");
		this.shape = new PolygonShape();
		body.createFixture(new SiciBossHead(this.shape)).setUserData("enemyhead");	
		this.shape.dispose();
		body.setUserData(this);
	}
	
	@Override
	public void changeDirection() {
		jumper = 0;
		this.movesLeft = !movesLeft;
	}
	
	@Override
	public void checkForDeath(){
		if(hp<=0){
			handleDeath();
		}
		if(body.getPosition().y < 0){
			handleDeath();
		}
	}
	
	public void handleDeath(){
		removeFlag = true;
		entityFactory.removeBossBlocks();
		gameFile.playerDefeatedBoss(this.getClass());
	}

	@Override
	public void update(float dt){
		this.animation.update(dt);
		checkForDeath();
		if(isActive){
			handleMovement(dt);
			if(hp < 90){
				handleShooting();
			}
			handleJumping(dt);
		}
	}
	
	private void handleMovement(float dt){
		accumulator += dt;
		if(movesLeft){
			moveLeft();
		}else{
			moveRight();
		}
	}

	@Override
	public String getName() {
		return "Sicarion";
	}

	private void handleShooting(){
		if(accumulator >= 3f){
			shoot();
			accumulator = 0;
		}
		if(accumulator >= 1 && accumulator < 2f){
			if(rng.nextInt(15) > 8){
				shoot();
			}
		}
	}

	public int getID(){
		return 1337;
	}
	
	private void handleJumping(float dt){
		jumper += dt;
		if (jumper > 1 && jumper < 3){
			if(rng.nextBoolean()){
				jump();
			}
			jumper += 3;
		}
	}
}