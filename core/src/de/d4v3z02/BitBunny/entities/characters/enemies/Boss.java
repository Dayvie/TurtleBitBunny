package de.d4v3z02.BitBunny.entities.characters.enemies;

/**
 * Created by dayvie on 16.06.2016.
 * defines the functions a Boss has to implement
 */
public interface Boss {
    void handleDeath();
    void checkForDeath();
    int getID();
    String getName();
}
