package de.d4v3z02.BitBunny.entities.characters.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.Character;
import de.d4v3z02.BitBunny.entities.characters.character_logic.CharacterLogic;

public abstract class Enemy extends Character{
	
	protected boolean movesLeft;
	protected MapObject mo;
	protected Array<Projectile> activeProjectiles;
	protected Array<CharacterLogic> characterLogics;
	
	public Enemy(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles, boolean facesRight) {
		super(world, sb);
		this.facesRight = facesRight;
		this.mo = mo;
		this.characterLogics = new Array<>();
		this.activeProjectiles = activeProjectiles;
		movesLeft = true;
		jumpHeight = 25;
		isActive = false;
		addLogic();
	}
	
	public void activate(){
		isActive = true;
	}
	
	@Override
	public void checkForDeath(){
		if(hp<=0){
			removeFlag = true;
		}
		if(body.getPosition().y < 0){
			removeFlag = true;
		}
	}

	@Override
	public void update(float dt){
		this.animation.update(dt);
		for (CharacterLogic characterLogic: characterLogics){
			characterLogic.executeLogic(dt);
		}
	}

	public abstract void addLogic();

	@Override
	public boolean doesMoveLeft() {
		return movesLeft;
	}

	public void changeDirection() {
		this.movesLeft = !movesLeft;
	}
}