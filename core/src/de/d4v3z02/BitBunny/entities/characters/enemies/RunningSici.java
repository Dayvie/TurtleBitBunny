package de.d4v3z02.BitBunny.entities.characters.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Projectile;

public class RunningSici extends Sici {

	public RunningSici(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles, boolean facesRight){
		super(world, sb, mo, activeProjectiles, facesRight);
		walkingSpeed = runningSpeed;
	}
}