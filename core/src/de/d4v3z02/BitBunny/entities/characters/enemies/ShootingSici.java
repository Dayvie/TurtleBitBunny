package de.d4v3z02.BitBunny.entities.characters.enemies;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.character_logic.ShootingSiciLogic;

public class ShootingSici extends Sici {

	public ShootingSici(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles, boolean facesRight) {
		super(world, sb, mo, activeProjectiles, facesRight);
	}
	
	public void shoot(){
		activeProjectiles.add(new Projectile(world, sb, this, "res/images/fireball.png", 32, 32));
	}

	public void addLogic(){
		characterLogics.add(new ShootingSiciLogic(this, 1.5f));
	}
}
