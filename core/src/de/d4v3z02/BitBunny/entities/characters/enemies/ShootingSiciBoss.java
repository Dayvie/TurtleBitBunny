package de.d4v3z02.BitBunny.entities.characters.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.handlers.levelhandlers.ContactManager;
import de.d4v3z02.BitBunny.handlers.levelhandlers.EntityFactory;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.UserInterfaceManager;
import de.d4v3z02.BitBunny.informer.enums.Assets;

/**
 * Created by dayvie on 16.06.2016.
 */
public class ShootingSiciBoss extends WSSici implements Boss {

    public EntityFactory entityFactory;
    public GameFile gameFile;

    public ShootingSiciBoss(World world, SpriteBatch sb, MapObject mo,
                       Array<Projectile> activeProjectiles, EntityFactory entityFactory, GameFile gameFile,
                       boolean facesRight) {
        super(world, sb, mo, activeProjectiles, facesRight);
        this.gameFile = gameFile;
        this.entityFactory = entityFactory;
        movesLeft = true;
        height = 32;
        width = 32;
        maxHp = 9;
        hp = 9;
        jumpHeight = 75;
        walkingSpeed = walkingSpeed*1.5f;
        if(gameFile.bossDefeated(this.getClass())){
            handleDeath();
        }
    }

    public int getID(){
        return 13337;
    }

    @Override
    public String getName() {
        return "Flamer";
    }

    public void handleDeath(){
        removeFlag = true;
        entityFactory.removeBossBlocks();
        gameFile.playerDefeatedBoss(this.getClass());
        UserInterfaceManager userInterfaceManager = ContactManager.getInstance().getUserInterfaceManager();
        if(userInterfaceManager != null) {
        	userInterfaceManager.closeLastGUI();
        }
        MusicHandler.getInstance().stopMusic();
        MusicHandler.getInstance().playLoopingMusic(Assets.GREENLANESONG.path);
    }

    public void checkForDeath(){
        if(hp<=0){
            handleDeath();
        }
    }
}
