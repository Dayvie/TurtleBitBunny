package de.d4v3z02.BitBunny.entities.characters.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.character_logic.SiciWalkLogic;
import de.d4v3z02.BitBunny.fixtureDefs.SiciBody;
import de.d4v3z02.BitBunny.fixtureDefs.SiciHead;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class Sici extends Enemy{
	
	public Sici(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles, boolean facesRight) {
		super(world, sb, mo, activeProjectiles, facesRight);
		jumpHeight = 25;
		loadTexture("res/images/bunnies.png", 32, 32, 0 , 3, 3);
		addLogic();
		createBody();
	}
	
	public void createBody(){	
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(x, y);
		bodyDef.fixedRotation = true;
		body = world.createBody(bodyDef);
		this.shape = new PolygonShape();
		body.createFixture(new SiciBody(this.shape)).setUserData("enemy");
		this.shape = new PolygonShape();
		body.createFixture(new SiciHead(this.shape)).setUserData("enemyhead");	
		this.shape.dispose();
		body.setUserData(this);
	}

	@Override
	public void addLogic(){
		this.characterLogics.add(new SiciWalkLogic(this));
	}
}
