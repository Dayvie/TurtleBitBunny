package de.d4v3z02.BitBunny.entities.characters.enemies;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.character_logic.ShootingSiciLogic;
import de.d4v3z02.BitBunny.entities.characters.character_logic.SiciWalkLogic;

public class WSSici extends ShootingSici {

	public WSSici(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles, boolean facesRight) {
		super(world, sb, mo, activeProjectiles, facesRight);
	}
	
	public void addLogic(){
		this.characterLogics.add((new SiciWalkLogic(this)));
		this.characterLogics.add(new ShootingSiciLogic(this, 1.7f));
	}
}