package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.SwitchBlock.Color;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class BlueBlock extends ColorBlock {

	public BlueBlock(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		createBody(Bits.BLUE.value, "plattform");
		loadTexture("res/maps/BOSSMAP.png", 12);
	}
	
	public void render(){
		sb.begin();
		if(color == Color.BLUE){
			sb.draw(activeBlock, x*Numbers.PPM.value - 16 , y *Numbers.PPM.value - 16);
		}else{
			sb.draw(inactiveBlock, x*Numbers.PPM.value - 16, y *Numbers.PPM.value - 16);
		}
		sb.end();
	}
}