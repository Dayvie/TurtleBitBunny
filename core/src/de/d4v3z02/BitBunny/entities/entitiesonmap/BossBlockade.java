package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class BossBlockade extends RemovedByTriggerBlock {

	public BossBlockade(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
	}
	
	@Override
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		bodyDef.type = BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(Numbers.HALFTILESIZE_WORLD.value, Numbers.HALFTILESIZE_WORLD.value);
		fixtureDef.shape = shape;
		fixtureDef.filter.categoryBits = Bits.GROUND.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.FOOT.value | Bits.ENEMY.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("plattform");
		body.setUserData(this);
		shape.dispose();
	}
}