package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.IdentifiableEntity;
import de.d4v3z02.BitBunny.handlers.levelhandlers.ContactManager;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class Checkpoint extends IdentifiableEntity{
	
	private boolean activated;

	public Checkpoint(World world, SpriteBatch sb, MapObject mo, int ID) {
		super(world, sb, mo, ID);
		loadTexture("res/images/Orb.png", 32, 32);
		createBody();
	}

	@Override
	protected void createBody() {
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		final float oneTile = 16 / Numbers.PPM.value;
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(oneTile, oneTile);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("checkpoint");
		body.setUserData(this);
		shape.dispose();
	}
	
	protected void drawActivated(){
		sb.draw(animation.getFrame(),
			body.getPosition().x*Numbers.PPM.value - 16,
			body.getPosition().y*Numbers.PPM.value - 16);	
	}
	
	protected void drawUnactivated(){
		sb.draw(unanmitedSprite, body.getPosition().x*Numbers.PPM.value - 16,
			body.getPosition().y*Numbers.PPM.value - 16);
	}
	
	@Override
	public void render(){
		sb.begin();
		if(activated){
			drawActivated();
		}else{
			drawUnactivated();
		}
		sb.end();
	}
	
	public void activate(){
		activated = true;
	}
	
	public void deactivate(){
		activated = false;
	}
	
	public boolean isActive(){
		return activated;
	}
}