package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.SwitchBlock.Color;
import de.d4v3z02.BitBunny.handlers.levelhandlers.ColorHolder;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public abstract class ColorBlock extends EntityOnMap {

	protected TextureRegion activeBlock;
	protected TextureRegion inactiveBlock;
	protected ColorHolder colorHolder;
	protected Color color;
	protected Sprite[] animationSprites;
	protected float x;
	protected float y;
	protected float tileSizeDividedByTwo;
	
	public ColorBlock(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		colorHolder = ColorHolder.getInstance();
	}
	
	protected void createBody(short bits, String userData){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		tileSizeDividedByTwo = 0.16f;
		bodyDef.type = BodyType.StaticBody;
		x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		ChainShape cShape = new ChainShape();
		createBlockRectangle(cShape, tileSizeDividedByTwo);
		fixtureDef.shape = cShape;
		fixtureDef.filter.categoryBits = bits;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.ENEMY.value| Bits.FOOT.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData(userData);
		body.setUserData(this);
		cShape.dispose();
	}
	
	protected void loadTexture(String path, int index){
		Texture tex = contentManager.get(path);
		activeBlock = TextureRegion.split(tex, 32, 32)[0][index];
		inactiveBlock = TextureRegion.split(tex, 32, 32)[0][index+3];
	}
	
	public void update(float dt){
		animation.update(dt);
		color = colorHolder.getColor();
	}
	
	private ChainShape createBlockRectangle(ChainShape cs, float tileSizeDividedByTwo){
		Vector2[] v = new Vector2[5];
		v[0] = new Vector2(-tileSizeDividedByTwo, -tileSizeDividedByTwo);
		v[1] = new Vector2(-tileSizeDividedByTwo, tileSizeDividedByTwo);
		v[2] = new Vector2(tileSizeDividedByTwo, tileSizeDividedByTwo);
		v[3] = new Vector2(tileSizeDividedByTwo, -tileSizeDividedByTwo);
		v[4] = new Vector2(-tileSizeDividedByTwo, -tileSizeDividedByTwo);
		cs.createChain(v);
		return cs;
	}
}