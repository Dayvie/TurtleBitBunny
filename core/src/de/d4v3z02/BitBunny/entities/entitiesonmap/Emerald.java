package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

public class Emerald extends Gold {

	public Emerald(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		value = 1;
		loadTexture(1, 0);
	}
	
	public Emerald(World world, SpriteBatch sb, float x, float y){
		super(world, sb, x, y);
		value = 1;
		loadTexture(1, 0);
	}
}