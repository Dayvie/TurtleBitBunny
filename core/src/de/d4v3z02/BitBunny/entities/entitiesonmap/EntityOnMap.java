package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public abstract class EntityOnMap extends Entity{

	protected MapObject mo;
	
	public EntityOnMap(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb);
		this.mo = mo;
	}
	
	public EntityOnMap(){
	}
	
	public void jump(){
		body.setLinearVelocity(0, 3);
	}

	/**
	 * readjusts the entity on the tilegrid. takes a x or y value and returns the centralized value
	 * @param mapCoord value to centralize in the tile
	 * @return centralized value
     */
	public float layoutOnGrid(float mapCoord){
		int val = (int)(mapCoord*Numbers.PPM.value);
		return (((val/32)*32)+16)/Numbers.PPM.value;
	}
}