package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;
/**
 * Currency
 * @author David
 *
 */
public abstract class Gold extends EntityOnMap {

	protected TextureRegion textureRegion;
	protected int value;
	protected float halfWidth;
	protected float halfHeight;
	protected float x;
	protected float y;
	
	public Gold(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		animation = null;
		width = 16;
		halfWidth = width/2;
		height = width;
		halfHeight = height/2;
		value = 0;
		createBody();
	}
	
	public Gold(World world, SpriteBatch sb, float x, float y) {
		this.world = world;
		this.sb = sb;
		this.x = x;
		this.y = y;
		animation = null;
		width = 16;
		halfWidth = width/2;
		height = width;
		halfHeight = height/2;
		value = 0;
		createBody();
	}
	
	public int getValue(){
		return value;
	}
	
	protected  void loadTexture(int x, int y) {
		Texture tex  = contentManager.get("res/images/gems.png");
		textureRegion = TextureRegion.split(tex, 32, 32)[y][x];
	}
	
	@Override
	public void render(){
		sb.begin();
		sb.draw(textureRegion, body.getPosition().x*Numbers.PPM.value-halfWidth, body.getPosition().y*Numbers.PPM.value-halfHeight, width, height);
		sb.end();
	}
	
	@Override
	public void update(float dt){
		checkForDeath();
	}
	
	private void checkForDeath(){
		if(body.getPosition().x < 0){
			this.removeFlag = true;
		}
	}
	
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		float halfWidth = this.halfWidth/Numbers.PPM.value;
		bodyDef.type= BodyType.DynamicBody;
		if(mo != null){
			x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
			y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		}
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(halfWidth, halfWidth);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.PASSABLE.value | Bits.GROUND.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("gold");
		body.setUserData(this);
		shape.dispose();
		fixtureDef = new FixtureDef();
		shape = new PolygonShape();
		shape.setAsBox(halfWidth, halfWidth);
		fixtureDef.shape = shape;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = (short)(Bits.PASSABLE.value | Bits.GROUND.value);
		body.createFixture(fixtureDef).setUserData("gold");
		shape.dispose();
	}
}