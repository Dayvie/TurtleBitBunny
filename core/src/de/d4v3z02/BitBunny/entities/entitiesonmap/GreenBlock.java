package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.SwitchBlock.Color;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class GreenBlock extends ColorBlock {

	public GreenBlock(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		createBody(Bits.GREEN.value, "plattform");
		loadTexture("res/maps/BOSSMAP.png", 11);
	}
	
	public void render(){
		sb.begin();
		if(color == Color.GREEN){
			sb.draw(activeBlock, x*Numbers.PPM.value - 16 , y *Numbers.PPM.value - 16);
		}else{
			sb.draw(inactiveBlock, x*Numbers.PPM.value - 16, y *Numbers.PPM.value - 16);
		}
		sb.end();
	}

}
