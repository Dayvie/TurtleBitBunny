package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class MovingPlattform extends EntityOnMap {
	
	private MovingDirection mDir;
	
	public enum MovingDirection{
		UP, DOWN, LEFT, RIGHT, NULL;
	}
	
	public MovingPlattform(World world, SpriteBatch sb, MapObject mo){
		super(world, sb, mo);
		loadTexture("res/maps/BOSSMAP.png", 96, 32, 0, 8, 3);
		createBody();
	}
	
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		bodyDef.type= BodyType.KinematicBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(width/2/Numbers.PPM.value, height/2/Numbers.PPM.value);
		fixtureDef.shape = shape;
		fixtureDef.filter.categoryBits = Bits.GROUND.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.ENEMY.value | Bits.SENSOR.value| Bits.GROUND.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("mp");
		body.setUserData(this);
		shape.dispose();
	}
	
	public void setMovingDirection(MovingDirection mDir){
		this.mDir = mDir;
	}
	
	@Override
	public void update(float dt){
		animation.update(dt);
		handleMovement();
	}
	
	private void handleMovement(){
		if(mDir == null){
			return;
		}
		switch(mDir){
		case DOWN:
			body.setLinearVelocity(new Vector2(0, -0.5f));
			break;
		case LEFT:
			body.setLinearVelocity(new Vector2(-0.5f, 0));
			break;
		case RIGHT:
			body.setLinearVelocity(new Vector2(0.5f, 0));
			break;
		case UP:
			body.setLinearVelocity(new Vector2(0, 0.5f));
			break;
		case NULL:
			body.setLinearVelocity(new Vector2(0, 0));
			break;
		default:
			break;
		}
	}
}
