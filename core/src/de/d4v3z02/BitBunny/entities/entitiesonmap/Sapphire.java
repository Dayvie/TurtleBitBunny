package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

public class Sapphire extends Gold {

	public Sapphire(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		value = 5;
		loadTexture(0, 0);
	}
	
	public Sapphire(World world, SpriteBatch sb, float x, float y){
		super(world, sb, x, y);
		value = 5;
		loadTexture(0, 0);
	}
}