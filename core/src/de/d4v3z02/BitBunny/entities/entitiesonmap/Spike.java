package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class Spike extends EntityOnMap {
	
	public Spike(World world, SpriteBatch sb, MapObject mo){
		super(world, sb, mo);
		loadTexture("res/maps/BOSSMAP.png", 32, 32, 16, 0);
		createBody();
	}
	
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		ChainShape cs = new ChainShape();
		cs = createTriangle(cs, x , y);
		fixtureDef.shape = cs;
		fixtureDef.filter.categoryBits = Bits.GROUND.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.PASSABLE.value | Bits.GROUND.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("enemy");
		body.setUserData(this);
		cs.dispose();
	}
	
	private ChainShape createTriangle(ChainShape cs, float x, float y){
		Vector2[] v = new Vector2[4];
		Vector2 absoluteZero = new Vector2(-Numbers.HALFTILESIZE_WORLD.value, -Numbers.HALFTILESIZE_WORLD.value);
		v[0] = absoluteZero;
		v[1] = new Vector2(Numbers.HALFTILESIZE_WORLD.value, -Numbers.HALFTILESIZE_WORLD.value);
		v[2] = new Vector2(0, 0);
		v[3] = absoluteZero;
		cs.createChain(v);
		return cs;
	}
	
	public float layoutOnGrid(float mapCoord){
		int val = (int)(mapCoord*Numbers.PPM.value);
		return (((val/32)*32)+16)/Numbers.PPM.value;
	}
}