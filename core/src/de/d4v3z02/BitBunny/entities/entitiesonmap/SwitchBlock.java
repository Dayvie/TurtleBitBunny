package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.handlers.levelhandlers.ColorHolder;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class SwitchBlock extends EntityOnMap{
	
	private ColorHolder colorInformer;
	private Color color;
	private Vector2 position;
	private Sprite[] animationSprites;
	
	public enum Color{
		RED, GREEN, BLUE;
	}

	public SwitchBlock(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		color = Color.RED;
		loadTextureFile("res/images/colororbs.png");
		createBody();
		loadTexture();
		colorInformer = ColorHolder.getInstance();
	}
	
	private void loadTexture(){
		Texture tex = contentManager.get("res/images/colororbs.png");
		TextureRegion[] textureRegions = TextureRegion.split(tex, 32, 32)[0];
		animationSprites = new Sprite[textureRegions.length];
		for (int j = 0; j < textureRegions.length; j++) {
			animationSprites[j] = new Sprite(textureRegions[j]);
		}
	}

	@Override
	protected void createBody() {
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		final float oneTile = 16 / Numbers.PPM.value;
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		position = new Vector2(x, y);
		shape = new PolygonShape();
		shape.setAsBox(oneTile, oneTile);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("switchblock");
		body.setUserData(this);
		shape.dispose();
	}
	
	@Override
	public void update(float dt){
		animation.update(dt);
		color = colorInformer.getColor();
	}
	
	@Override
	public void render(){
		sb.begin();
		switch (color) {
		case BLUE:
			sb.draw(animationSprites[2], body.getPosition().x*Numbers.PPM.value -16,
					body.getPosition().y*Numbers.PPM.value -16);
			break;
		case GREEN:
			sb.draw(animationSprites[1], body.getPosition().x*Numbers.PPM.value -16,
					body.getPosition().y*Numbers.PPM.value -16);
			break;
		case RED:
			sb.draw(animationSprites[0], body.getPosition().x*Numbers.PPM.value -16,
					body.getPosition().y*Numbers.PPM.value -16);
			break;
		default:
			break;
		}
		sb.end();
	}
	
	public void setColor(Color color){
		this.color = color;
	}
}