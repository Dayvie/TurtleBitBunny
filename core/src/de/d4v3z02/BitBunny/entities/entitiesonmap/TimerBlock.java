package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class TimerBlock extends EntityOnMap {
	
	public enum TimerBlockState{
		ZERO, ONE, TWO, THREE;
	}
	
	private Sprite[] animationSprites;
	private TimerBlockState timerBlockState;
	private boolean active;
	float timerAccumulator;
	
	public TimerBlock(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		timerBlockState = TimerBlockState.THREE;
		createBody();
		loadTextures();
	}
	
	private void loadTextures(){
		Texture tex = contentManager.get("res/maps/BOSSMAP.png");
		TextureRegion[] textureRegions = TextureRegion.split(tex, 32, 32)[0];
		animationSprites = new Sprite[4];
		int i = 0;
		for (int j = 18; j < 22; j++){
			animationSprites[i] = new Sprite(textureRegions[j]);
			i++;
		}
	}
	
	public void activate(){
		active = true;
	}
	
	public boolean isActive(){
		return active;
	}
	
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		ChainShape cShape = new ChainShape();
		createBlockRectangle(cShape, Numbers.HALFTILESIZE_WORLD.value);
		fixtureDef.shape = cShape;
		fixtureDef.filter.categoryBits = Bits.GROUND.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.ENEMY.value| Bits.FOOT.value);
		this.body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("plattform");
		body.setUserData(this);
		cShape.dispose();
	}
	
	private ChainShape createBlockRectangle(ChainShape cs, float tileSizeDividedByTwo){
		Vector2[] v = new Vector2[5];
		v[0] = new Vector2(-tileSizeDividedByTwo, -tileSizeDividedByTwo);
		v[1] = new Vector2(-tileSizeDividedByTwo, tileSizeDividedByTwo);
		v[2] = new Vector2(tileSizeDividedByTwo, tileSizeDividedByTwo);
		v[3] = new Vector2(tileSizeDividedByTwo, -tileSizeDividedByTwo);
		v[4] = new Vector2(-tileSizeDividedByTwo, -tileSizeDividedByTwo);
		cs.createChain(v);
		return cs;
	}
	
	@Override
	public void update(float dt){
		if(active){
			timerAccumulator += dt;
			if(timerAccumulator > 1){
				if(timerAccumulator > 2){
					if(timerAccumulator > 3){
						if(timerAccumulator > 4){
							removeFlag = true;
							return;
						}
						timerBlockState  = TimerBlockState.ZERO;
						return;
					}
					timerBlockState  = TimerBlockState.ONE;
					return;
				}
				timerBlockState  = TimerBlockState.TWO;
				return;
			}
		}
	}
	
	@Override
	public void render(){
		sb.begin();
		switch(timerBlockState){
			case ZERO:
				sb.draw(animationSprites[3], body.getPosition().x*Numbers.PPM.value - 16,
						body.getPosition().y*Numbers.PPM.value - 16);
				break;
			case ONE:
				sb.draw(animationSprites[0], body.getPosition().x*Numbers.PPM.value - 16,
						body.getPosition().y*Numbers.PPM.value - 16);
				break;
			case TWO:
				sb.draw(animationSprites[1], body.getPosition().x*Numbers.PPM.value - 16,
						body.getPosition().y*Numbers.PPM.value - 16);
				break;
			case THREE:
				sb.draw(animationSprites[2], body.getPosition().x*Numbers.PPM.value - 16,
						body.getPosition().y*Numbers.PPM.value - 16);
				break;
			default:
				break;
		}
		sb.end();
	}
}