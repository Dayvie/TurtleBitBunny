package de.d4v3z02.BitBunny.entities.entitiesonmap;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.*;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;
import de.d4v3z02.BitBunny.informer.enums.Assets;

/**
 * Created by dayvie on 18.06.2016.
 */
public class TrapBlock extends RemovedByTriggerBlock {

    private boolean isActive;
    private FixtureDef fixtureDef;
    protected TextureRegion activeBlock;
    protected TextureRegion inactiveBlock;

    public TrapBlock(World world, SpriteBatch sb, MapObject mo) {
        super(world, sb, mo);
        isActive = false;
        readjustBody();
        loadTexture(22);
    }

    protected void loadTexture(int index){
        Texture tex = contentManager.get(Assets.BOSSMAP.path);
        activeBlock = TextureRegion.split(tex, 32, 32)[0][index];
        inactiveBlock = TextureRegion.split(tex, 32, 32)[0][index+1];
    }

    @Override
    protected void createBody(){
        BodyDef bodyDef = new BodyDef();
        fixtureDef = new FixtureDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
        float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
        x = layoutOnGrid(x);
        y = layoutOnGrid(y);
        bodyDef.position.set(x, y);
        shape = new PolygonShape();
        shape.setAsBox(Numbers.HALFTILESIZE_WORLD.value, Numbers.HALFTILESIZE_WORLD.value);
        fixtureDef.shape = shape;
        fixtureDef.filter.categoryBits = Bits.GROUND.value;
        fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.FOOT.value | Bits.ENEMY.value);
        body = world.createBody(bodyDef);
        body.createFixture(fixtureDef).setUserData("plattform");
        body.setUserData(this);
        shape.dispose();
    }

    public void readjustBody(){
        if(isActive){
            Filter filter = body.getFixtureList().
                    first().getFilterData();
            filter.categoryBits = Bits.GROUND.value;
            body.getFixtureList().first().setFilterData(filter);
        }else{
            Filter filter = body.getFixtureList().
                    first().getFilterData();
            filter.categoryBits = 0;
            body.getFixtureList().first().setFilterData(filter);
        }
    }

    public void activate(){
        isActive = true;
        readjustBody();
    }

    public void deactivate(){
        isActive = false;
        readjustBody();
    }

    @Override
    public void render(){
        if(isActive){
            sb.begin();
            sb.draw(activeBlock,
                    body.getPosition().x*Numbers.PPM.value - width /2,
                    body.getPosition().y*Numbers.PPM.value - height /2);
            sb.end();
        }else{
            sb.begin();
            sb.draw(inactiveBlock,
                    body.getPosition().x*Numbers.PPM.value - width /2,
                    body.getPosition().y*Numbers.PPM.value - height /2);
            sb.end();
        }

    }
}
