package de.d4v3z02.BitBunny.entities.entitiesonmap.areas;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

/*
 * @author David Yesil
 * old class for triggers. Not recommended to use
 */

public class Goal extends EntityOnMap{
	
	public Goal(World world, SpriteBatch sb, MapObject mo, float x, float y) {
		super(world, sb, mo);
		createBody(x, y);
	}
	
	public Goal(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		createBody();
	}
	
	protected void createBody(float x, float y){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		final float fourtiles = 32/ Numbers.PPM.value;
		bodyDef.type= BodyType.StaticBody;
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(fourtiles, fourtiles);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("goal");
		body.setUserData(this);
		shape.dispose();
	}
	
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		final float fourtiles = 32/ Numbers.PPM.value;
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(fourtiles, fourtiles);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("goal");
		body.setUserData(this);
		shape.dispose();
	}
	
	public void render(){
		
	}
}