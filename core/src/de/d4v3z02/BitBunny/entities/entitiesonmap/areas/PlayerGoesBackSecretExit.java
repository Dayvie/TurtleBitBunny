package de.d4v3z02.BitBunny.entities.entitiesonmap.areas;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;
/**
 * 
 * @author DavidYesil
 * The area which lets the player go back a level if he entered a entrance which increments the levelcounter only by 1.
 *
 */
public class PlayerGoesBackSecretExit extends Goal {
	public PlayerGoesBackSecretExit(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
	}

	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		final float fourtiles = 16/ Numbers.PPM.value;
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(fourtiles, fourtiles);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("sbgoal");
		body.setUserData("sbgoal");
		shape.dispose();
	}
}