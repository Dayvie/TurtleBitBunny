package de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;

public abstract class IdentifiableEntity extends EntityOnMap {
	
	protected final int ID;
	
	public IdentifiableEntity(World world, SpriteBatch sb, MapObject mo, int ID){
		super(world, sb, mo);
		this.ID = ID;
	}
	
	public IdentifiableEntity(){
		ID = 5555;
	}
	
	public int getID(){
		return ID;
	}
}
