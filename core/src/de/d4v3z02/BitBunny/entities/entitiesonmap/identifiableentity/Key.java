package de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class Key extends IdentifiableEntity {

	protected float x;
	protected float y;
	
	public Key(World world, SpriteBatch sb, MapObject mo, int ID) {
		super(world, sb, mo, ID);
		width = 32;
		height = width;
		loadTexture("res/maps/BOSSMAP.png", 32, 32, 16, 1, 16);
		createBody();
	}
	
	public Key(World world, SpriteBatch sb, float x, float y) {
		this.world = world;
		this.sb = sb;
		this.x = x;
		this.y = y;
		width = 32;
		height = width;
		loadTexture("res/maps/BOSSMAP.png", 32, 32, 16, 1, 16);
		createBody();
	}
	
	public void render(){
		sb.begin();
		sb.draw(animation.getFrame(), body.getPosition().x*Numbers.PPM.value-width/2,
				body.getPosition().y*Numbers.PPM.value-height/2+8, width, height);
		sb.end();
	}
	
	@Override
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		float halfWidth = width/2/Numbers.PPM.value;
		bodyDef.type= BodyType.DynamicBody;
		if(mo != null){
			x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
			y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		}
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(halfWidth, halfWidth/2);
		createSensorDef(fixtureDef);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("powerup");
		body.setUserData(this);
		shape.dispose();
		fixtureDef = new FixtureDef();
		shape = new PolygonShape();
		shape.setAsBox(halfWidth, halfWidth/2);
		fixtureDef.shape = shape;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = (short)(Bits.PASSABLE.value | Bits.GROUND.value);
		body.createFixture(fixtureDef).setUserData("powerup");
		shape.dispose();
	}
	
	private void createSensorDef(FixtureDef fixtureDef){
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value);
	}
}
