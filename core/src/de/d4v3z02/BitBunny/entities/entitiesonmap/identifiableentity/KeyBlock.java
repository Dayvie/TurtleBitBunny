package de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class KeyBlock extends IdentifiableEntity {
	
	public KeyBlock(World world, SpriteBatch sb, MapObject mo, int ID) {
		super(world, sb, mo, ID);
		loadTexture("res/maps/BOSSMAP.png", 32 , 32, 19, 1, 19);
		createBody();
		width = 32;
		height = width;
	}	
	
	@Override
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		bodyDef.type = BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(width/2/Numbers.PPM.value, width/2/Numbers.PPM.value);
		fixtureDef.shape = shape;
		fixtureDef.filter.categoryBits = Bits.GROUND.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.ENEMY.value| Bits.FOOT.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("keyblock");
		body.setUserData(this);
		shape.dispose();
	}
}