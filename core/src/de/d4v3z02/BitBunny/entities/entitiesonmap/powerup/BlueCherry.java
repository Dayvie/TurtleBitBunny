package de.d4v3z02.BitBunny.entities.entitiesonmap.powerup;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

//TODO find a better texture
public class BlueCherry extends PowerUp {
	
	public BlueCherry(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		loadTexture("res/images/bluecherry.png", 32, 32);
	}
}