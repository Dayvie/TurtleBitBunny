package de.d4v3z02.BitBunny.entities.entitiesonmap.powerup;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

public class FireCherry extends PowerUp {
	
	public FireCherry(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		loadTexture("res/images/cherry.png", 32, 32);
	}
}