package de.d4v3z02.BitBunny.entities.entitiesonmap.powerup;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class Heart extends EntityOnMap{

private AtlasRegion heartcontainer;
protected float x;
protected float y;

	public Heart(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		this.width = 16;
		this.height = width;
		loadTexture();
		createBody();
		animation = null;
	}
	
	public Heart(World world, SpriteBatch sb, float x, float y) {
		this.world = world;
		this.sb = sb;
		this.x = x;
		this.y = y;
		loadTexture();
		animation = null;
		width = 16;
		height = width;
		createBody();
	}

	@Override
	public void update(float dt){
	}
	
	public void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		float eightPixels = width/2/ Numbers.PPM.value;
		bodyDef.type= BodyType.DynamicBody;
		if(mo != null){
			x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
			y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		}
		bodyDef.position.set(x, y);
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(eightPixels, eightPixels);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = (short)(Bits.PLAYER.value | Bits.PASSABLE.value | Bits.GROUND.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("powerup");
		body.setUserData(this);
		shape.dispose();
		fixtureDef = new FixtureDef();
		shape = new PolygonShape();
		shape.setAsBox(eightPixels, eightPixels);
		fixtureDef.shape = shape;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = (short)(Bits.PASSABLE.value | Bits.GROUND.value);
		body.createFixture(fixtureDef).setUserData("gold");
		shape.dispose();
	}
	
	protected  void loadTexture() {
		contentManager.preLoadSafely("res/images/hearts.pack",TextureAtlas.class);
		TextureAtlas heartAtlas = contentManager.get("res/images/hearts.pack");
		heartcontainer = heartAtlas.findRegion("hud_heartFull");	
	}
	
	@Override
	public void render(){
		sb.begin();
		sb.draw(heartcontainer,
				body.getPosition().x*Numbers.PPM.value - width/2,
				body.getPosition().y*Numbers.PPM.value - height/2, width, height);
		sb.end();
	}
}