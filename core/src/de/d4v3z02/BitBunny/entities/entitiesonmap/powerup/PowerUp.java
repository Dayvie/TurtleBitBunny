package de.d4v3z02.BitBunny.entities.entitiesonmap.powerup;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class PowerUp extends EntityOnMap {
	
	public PowerUp(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
		createBody();
	}
	
	protected  void loadTexture(String s) {
		loadTextureFile(s);
		Texture tex  = contentManager.get(s);
		unanmitedSprite = new Sprite(tex);
		setAnimation(unanmitedSprite, 1);
	}

	@Override
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		float eightPixels = 16/ Numbers.PPM.value;
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		CircleShape cShape = new CircleShape();
		cShape.setRadius(eightPixels);
		fixtureDef.shape = cShape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.POWERUP.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("powerup");
		body.setUserData(this);
		cShape.dispose();
	}
}