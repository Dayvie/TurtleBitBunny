package de.d4v3z02.BitBunny.entities.entitiesonmap.triggers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;
import de.d4v3z02.BitBunny.informer.enums.Triggertype;
/**
 * 
 * @author David
 * A more convenient class for Triggers. Triggers get identified by the triggertype
 */
public class Trigger extends EntityOnMap {

	private Triggertype triggertype;
	
	public Trigger(World world, SpriteBatch sb, MapObject mo, Triggertype triggertype) {
		super(world, sb, mo);
		this.triggertype = triggertype;
		createBody();
	}

	public Trigger(World world, SpriteBatch sb, MapObject mo, Triggertype triggertype, float width, float height) {
		this.triggertype = triggertype;
		createBody(width, height);
	}
	
	public Triggertype getTriggerType(){
		return this.triggertype;
	}

	protected void createBody(float width, float height){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(width/Numbers.PPM.value, height/Numbers.PPM.value);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("trigger");
		body.setUserData(this);
		shape.dispose();
	}
	
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		final float fourtiles = 32/ Numbers.PPM.value;
		bodyDef.type= BodyType.StaticBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(fourtiles, fourtiles);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.CRYSTAL.value;
		fixtureDef.filter.maskBits = Bits.PLAYER.value;
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("trigger");
		body.setUserData(this);
		shape.dispose();
	}
	
	@Override
	public void update(float dt){
	}
	
	@Override
	public void render(){
	}
}
