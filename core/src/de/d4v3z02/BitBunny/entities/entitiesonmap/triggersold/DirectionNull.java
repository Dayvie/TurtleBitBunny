package de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

public class DirectionNull extends DirectionPointer {

	public DirectionNull(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
	}
}