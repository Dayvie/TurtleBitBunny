package de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

/**
 * Sensor which are used to guide AI
 * @author David
 *
 */
public abstract class DirectionPointer extends EntityOnMap {

	public DirectionPointer(World world, SpriteBatch sb, MapObject mo){
		super(world, sb, mo);
		createBody();
	}
	
	protected void createBody(){
		BodyDef bodyDef = new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
		bodyDef.type= BodyType.DynamicBody;
		float x = (float) mo.getProperties().get("x", Float.class) / Numbers.PPM.value;
		float y = (float) mo.getProperties().get("y", Float.class) / Numbers.PPM.value;
		x = layoutOnGrid(x);
		y = layoutOnGrid(y);
		bodyDef.position.set(x, y);
		shape = new PolygonShape();
		shape.setAsBox(Numbers.HALFTILESIZE_WORLD.value, Numbers.HALFTILESIZE_WORLD.value);
		fixtureDef.shape = shape;
		fixtureDef.isSensor = true;
		fixtureDef.filter.categoryBits = Bits.SENSOR.value;
		fixtureDef.filter.maskBits = (short)(Bits.GROUND.value);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef).setUserData("dp");
		body.setUserData(this);
		body.setGravityScale(0.0f);
		shape.dispose();
	}
	
	@Override
	public void render(){
		
	}
}