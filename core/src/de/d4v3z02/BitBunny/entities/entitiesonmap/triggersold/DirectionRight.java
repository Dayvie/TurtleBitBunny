package de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;

public class DirectionRight extends DirectionPointer {

	public DirectionRight(World world, SpriteBatch sb, MapObject mo) {
		super(world, sb, mo);
	}
}
