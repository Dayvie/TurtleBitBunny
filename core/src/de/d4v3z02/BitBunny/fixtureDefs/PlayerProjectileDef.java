package de.d4v3z02.BitBunny.fixtureDefs;

import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class PlayerProjectileDef extends FixtureDef{

	public PlayerProjectileDef(CircleShape cShape){
		createThisBody(cShape);
	}
	
	private void createThisBody(CircleShape cShape){
		float eightPixels = 8/ Numbers.PPM.value;
		cShape.setRadius(eightPixels);
		restitution = 0;
		density = 0;
		friction = 0;
		shape = cShape;
		isSensor = true;
		filter.categoryBits = Bits.DEADLYCOLLISIONFORENEMY.value;
		filter.maskBits = Bits.ENEMY.value;
	}
}
