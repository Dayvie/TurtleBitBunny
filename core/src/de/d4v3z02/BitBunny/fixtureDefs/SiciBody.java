package de.d4v3z02.BitBunny.fixtureDefs;

import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;


public class SiciBody extends FixtureDef {
	
	public SiciBody(PolygonShape shape){
		shape = new PolygonShape();
		shape.setAsBox(13/ Numbers.PPM.value, 13/ Numbers.PPM.value);
		this.shape = shape;
		this.restitution = 0;
		this.density = 1;
		this.friction = 0;
		this.filter.categoryBits = Bits.ENEMY.value;
		this.filter.maskBits = (short) (Bits.RED.value | Bits.GREEN.value | Bits.BLUE.value |Bits.GROUND.value | Bits.PLAYER.value| Bits.PASSABLE.value | Bits.DEADLYCOLLISIONFORENEMY.value);
	}
}
