package de.d4v3z02.BitBunny.fixtureDefs;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class SiciBossHead extends FixtureDef {
	
	public SiciBossHead(PolygonShape headShape){
		headShape = new PolygonShape();
		headShape.setAsBox(27 / Numbers.PPM.value, 2 / Numbers.PPM.value, new Vector2(0, 27 / Numbers.PPM.value), 0);
		this.shape = headShape;
		this.isSensor = true;
		this.filter.categoryBits = Bits.ENEMY.value;
		this.filter.maskBits = (short) (Bits.PLAYER.value | Bits.FOOT.value| Bits.PASSABLE.value);
	}
}
