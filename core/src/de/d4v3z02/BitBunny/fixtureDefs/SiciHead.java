package de.d4v3z02.BitBunny.fixtureDefs;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.Numbers;



public class SiciHead extends FixtureDef {

	public SiciHead(PolygonShape headShape){
		headShape = new PolygonShape();
		headShape.setAsBox(14 / Numbers.PPM.value, 4 / Numbers.PPM.value, new Vector2(0, 14.1f / Numbers.PPM.value), 0);
		this.shape = headShape;
		this.isSensor = true;
		this.filter.categoryBits = Bits.ENEMY.value;
		this.filter.maskBits = (short) (Bits.PLAYER.value | Bits.FOOT.value| Bits.PASSABLE.value);
	}
}
