package de.d4v3z02.BitBunny.handlers.filehandlers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;

import de.d4v3z02.BitBunny.informer.enums.Assets;
import de.d4v3z02.BitBunny.informer.enums.LevelAsset;

public class ContentManager extends AssetManager {
	
	private static ContentManager singleInstance;
	
	private ContentManager(){
		super();
	}
	
	public static ContentManager getInstance(){
		if(singleInstance == null){
			singleInstance = new ContentManager();
		}
		return singleInstance;
	}
	
	public void preLoadSafely(String path, Class<?> type){
		if(!isLoaded(path)){
			load(path, type);
			finishLoadingAsset(path);
		}
	}
	
	public void loadLevel(LevelAsset levelAsset){
		loadUniversialMusic();
		preLoadSafely(Assets.TEXTFIELD.path, Texture.class);
		preLoadSafely(Assets.BUNNIES.path, Texture.class);
		preLoadSafely(Assets.GEMS.path, Texture.class);
		preLoadSafely(levelAsset.bg, Texture.class);
		preLoadSafely(Assets.BOSSMAP.path, Texture.class);
		preLoadSafely(Assets.BOWSERSONG.path, Music.class);
		preLoadSafely(levelAsset.music, Music.class);
	}
	
	private void loadUniversialMusic(){
		preLoadSafely("res/sfx/smallFireball.mp3", Music.class);
		preLoadSafely("res/sfx/crystal.mp3", Music.class);
		preLoadSafely("res/sfx/coin.mp3", Music.class);
		preLoadSafely("res/sfx/hit.wav", Music.class);
		preLoadSafely("res/sfx/changeblock.wav", Music.class);
	}
}