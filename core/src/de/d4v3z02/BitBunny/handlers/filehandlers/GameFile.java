package de.d4v3z02.BitBunny.handlers.filehandlers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Vector2;

import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.entities.characters.Player.PowerUpState;
import de.d4v3z02.BitBunny.informer.enums.Shopitem;

public class GameFile implements Serializable {
	
	public enum SpawnPosition{
		NORMAL, END, SECRETEND, CP;
	}
	
	private Map<Integer, LevelData> levelData;
	private Map<Class<?>, Boolean>defeatedBosses;
	private SpawnPosition spawnPosition;
	private GameFileManager gameFileManager;
	private static final long serialVersionUID = 5401051373581339582L;
	private int playerMaxHp;
	private int playerHp;
	private int lastLevel;
	private PowerUpState lastPowerUp;
	private PowerUpState[] collectedPowerUps;
	private String playerName;
	private int collectedCrystals;
	private int fireBallDmg;
	private Vector2 sEndPos;
	private Vector2 endPos;
	private int gold;
	private int index;
	private int checkpointID;
	private int spentCrystals;
	private int keyAmount;
	
	// TODO: Powerups im Falle Playername != dayvie testen, siehe Constructor!, umbau auf List oder Libgdx.Array
	public GameFile(String playerName, int maxHp, int level, PowerUpState pState){
		gameFileManager = GameFileManager.getInstance();
		levelData = new HashMap<>();
		spawnPosition = SpawnPosition.NORMAL;
		defeatedBosses = new HashMap<>();
		collectedPowerUps = new PowerUpState[1]; 
		collectedPowerUps[0] = PowerUpState.NONE;
		index = 0;
		this.playerName = playerName;
		this.playerMaxHp = maxHp;
		this.playerHp = maxHp;
		this.lastLevel = level;
		this.lastPowerUp = pState;
		collectedCrystals = 0;
		gold = 0;
		fireBallDmg = 1;
		spentCrystals = 0;
		keyAmount = 0;
		if(playerName.equals("dayvie")){
			this.lastLevel = 1;
			this.playerMaxHp = 8;
			this.playerHp = 8;
			gold = 9999;
			addPowerUp(PowerUpState.BLUECHERRY);
			addPowerUp(PowerUpState.FIRECHERRY);
			keyAmount = 1337;
		}
		generateLevelData();
	}
	
	public void buyItem(Shopitem item){
		levelData.get(lastLevel).getBoughtItems().add(item);
	}
	
	public void saveGameFile(){
		gameFileManager.savePlayer(playerName);
	}
	
	public int getKeyAmount(){
		return keyAmount;
	}
	
	public void spendCrystals(int i){
		spentCrystals =- i;
	}
	
	public LevelData getLevelData(){
		return levelData.get(lastLevel);
	}
	
	private void generateLevelData(){
		for(int levelIndex = 1; levelIndex<99; levelIndex++){
			levelData.put(levelIndex, new LevelData());
		}
	}
	
	public int getCheckpointID(){
		return this.checkpointID;
	}
	
	public void setCheckpointID(Integer i){
		this.checkpointID = i;
	}
	
	public boolean bossDefeated(Object o){
		Boolean returnValue = defeatedBosses.get(o);
		if(returnValue == null){
			return false;
		}
		return returnValue.booleanValue();
	}
	
	public void playerDefeatedBoss(Class<?> o){
		if(!(defeatedBosses.containsKey(o))){
			defeatedBosses.put(o, true);
		}
	}
	
	private void extendPowArr(){
		PowerUpState[] newCollectedPowerUps = new PowerUpState[collectedPowerUps.length+1];
		for(int i = 0; i < collectedPowerUps.length;i++){
			newCollectedPowerUps[i] = collectedPowerUps[i];
		}
		this.collectedPowerUps = newCollectedPowerUps;
	}
	
	private void nextPowIndex(){
		index++;
		if(index >= collectedPowerUps.length){
			index = 0;
		}
	}
	
	public void nextPowerUp(){
		nextPowIndex();
		lastPowerUp = collectedPowerUps[index];
	}
	
	public void addPowerUp(PowerUpState pow){
		if(powDoesntContain(pow)){
			extendPowArr();
			collectedPowerUps[collectedPowerUps.length-1] = pow;
		}
	}
	
	public boolean powDoesntContain(PowerUpState pow){
		for(int i = 0; i < collectedPowerUps.length; i++){
			if(collectedPowerUps[i] == pow){
				return false;
			}
		}
		return true;
	}
	
	public int getGold(){
		return this.gold;
	}
	
	public void increaseGold(int gold){
		this.gold += gold;
	}
	
	public boolean decreaseGold(int gold){
		if(this.gold >= gold){
			this.gold -= gold;
			return true;
		}
		return false;
	}
	
	public void spendKey(){
		keyAmount--;
	}
	
	public void collectKey(int ID){
		keyAmount++;
		levelData.get(lastLevel).getItems().put(ID, true);
	}
	
	public boolean isItemActivated(int ID){
		Boolean isCollected = levelData.get(lastLevel).getItems().get(ID);
		if(isCollected == null){
			return isCrystalCollected(ID);
		}
		return isCollected.booleanValue();
	}
	
	public void activateItem(int ID){
		levelData.get(lastLevel).getItems().put(ID, true);
	}
	
	public void collectCrystal(int ID){
		collectedCrystals++;
		levelData.get(lastLevel).getCrystals().put(ID, true);
	}
	
	public boolean isCrystalCollected(int ID){
		Boolean isCollected = levelData.get(lastLevel).getCrystals().get(ID);
		if(isCollected == null){
			return false;
		}
		return isCollected.booleanValue();
	}
	
	public Vector2 getEndPos(){
		return this.endPos;
	}
	
	public Vector2 getSEndPos(){
		return this.sEndPos;
	}
	
	public void setSEndPos(Vector2 sEndPos){
		this.sEndPos = sEndPos;
	}
	
	public void setEndPos(Vector2 endPos){
		this.endPos = endPos;
	}
	
	public SpawnPosition getSpawnPosition(){
		return spawnPosition;
	}
	
	public void setSpawnPosition(SpawnPosition sp){
		this.spawnPosition = sp;
	}
	
	public int getPlayerHp() {
		return playerHp;
	}
	
	public void updateHP(Player player){
		this.playerMaxHp = player.getMaxHp();
		this.playerHp = player.getHP();
	}
	
	public void fillHP(Player player){
		this.playerHp = player.getMaxHp();
	}

	public int getFireBallDmg(){
		return this.fireBallDmg;
	}
	
	public void increaseFireBallDmg(int dmg){
		fireBallDmg += dmg;
	}
	
	public int getPlayerCrystalNum(){
		return collectedCrystals-spentCrystals;
	}
	
	public int getCollectedCrystals(){
		Map<Integer, Boolean> crystals = levelData.get(lastLevel).getCrystals();
		int i = 0;
		for(Boolean isCollected : crystals.values()){
			if(isCollected){
				i++;
			}
		}
		return i;
	}
	
	public int getCollectedCrystals(int level){
		Map<Integer, Boolean> crystals = levelData.get(level).getCrystals();
		int i = 0;
		for(Boolean isCollected : crystals.values()){
			if(isCollected){
				i++;
			}
		}
		return i;
	}

	public int getPlayerMaxHp() {
		return this.playerMaxHp;
	}

	public int getLastLevel() {
		return lastLevel;
	}

	public void setLastLevel(int lastLevel) {
		this.lastLevel = lastLevel;
	}

	public PowerUpState getLastPowerUp() {
		return lastPowerUp;
	}

	public void setLastPowerUp(PowerUpState lastPowerUp) {
		this.lastPowerUp = lastPowerUp;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
}