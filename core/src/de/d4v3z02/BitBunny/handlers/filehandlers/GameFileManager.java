package de.d4v3z02.BitBunny.handlers.filehandlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GameFileManager implements Serializable {
	
	private static final long serialVersionUID = -8117509062371762490L;
	private Map<String, GameFile> gameFiles;
	private static GameFileManager gameFileManager  = null;
	private List<String> playerNames;

	private GameFileManager(){
		gameFiles = new HashMap<>();
		playerNames = new ArrayList<String>();
		loadAll();
	}
	
	public static GameFileManager getInstance(){
		if(gameFileManager == null){
			gameFileManager = new GameFileManager();
		}
		return gameFileManager;
	}
	
	public List<String> getAllPlayerNames(){
		return playerNames;
	}
	
	public GameFile getGameFile(String playerName){
		return gameFiles.get(playerName);
	}
	
	public void addGameFile(String playerName, GameFile gameFile){
		if(gameFiles.containsKey(playerName)){
			return;
		}
		gameFiles.put(playerName, gameFile);
		playerNames.add(playerName);
		saveAll();
	}
	
	public void deleteGameFile(String playerName){
		gameFiles.remove(playerName);
		for(Iterator<String> i = playerNames.iterator(); i.hasNext();){
			String s = i.next();
			if(s.equals(playerName)){
				i.remove();
			}
		}
		File file = new File("GameData/Saves/"+playerName+".sav");
		if (!file.delete()) {
		    String message = file.exists() ? "is in use by another app" : "does not exist";
		    try {
				throw new IOException("Cannot delete file, because file " + message + ".");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		saveAll();
	}
	
	public void saveAll(){
		try {
			ObjectOutputStream objectOutputStream;
			for(GameFile gf: gameFiles.values()){
				objectOutputStream = new ObjectOutputStream(
				        new FileOutputStream("GameData/Saves/"+gf.getPlayerName()+ ".sav"));
				objectOutputStream.writeObject(gf);
				objectOutputStream.flush();
				objectOutputStream.close();
			}
			objectOutputStream = new ObjectOutputStream(
			        new FileOutputStream("GameData/Saves/names.sav"));
			objectOutputStream.writeObject(playerNames);
			objectOutputStream.flush();
			objectOutputStream.close();	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void savePlayer(String playername){
		try {
			ObjectOutputStream objectOutputStream;
			GameFile gf = gameFiles.get(playername);
			objectOutputStream = new ObjectOutputStream(
			        new FileOutputStream("GameData/Saves/"+gf.getPlayerName()+ ".sav"));
			objectOutputStream.writeObject(gf);
			objectOutputStream.flush();
			objectOutputStream.close();
			objectOutputStream = new ObjectOutputStream(
			        new FileOutputStream("GameData/Saves/names.sav"));
			objectOutputStream.writeObject(playerNames);
			objectOutputStream.flush();
			objectOutputStream.close();	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void loadAll(){
		try {
			new File("GameData/Saves/").mkdirs();
			File file = new File("GameData/Saves/names.sav");
			ObjectInputStream ois;
			if(file.exists()){
				ois = new ObjectInputStream(new FileInputStream("GameData/Saves/names.sav"));
				playerNames = (List<String>) ois.readObject();
				ois.close();
			}
		for(String playerName:this.playerNames){
			ois = new ObjectInputStream(new FileInputStream("GameData/Saves/"+playerName + ".sav"));
			GameFile gf = (GameFile) ois.readObject();
			gameFiles.put(playerName, gf);
			ois.close();
		}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}