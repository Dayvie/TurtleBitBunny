package de.d4v3z02.BitBunny.handlers.filehandlers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.d4v3z02.BitBunny.informer.enums.Shopitem;

public class LevelData implements Serializable {

	private static final long serialVersionUID = 142416850309886429L;
	private Map<Integer, Boolean> items;
	private Map<Integer, Boolean> crystals;
	private List<Shopitem> boughtItems;
	
	public LevelData(){
		items = new HashMap<>();
		crystals = new HashMap<>();
		boughtItems = new ArrayList<>();
	}
	
	public Map<Integer, Boolean> getItems(){
		return items;
	}
	
	public Map<Integer, Boolean> getCrystals(){
		return crystals;
	}
	
	public List<Shopitem> getBoughtItems(){
		return boughtItems;
	}
}