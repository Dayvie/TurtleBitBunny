package de.d4v3z02.BitBunny.handlers.filehandlers;

import com.badlogic.gdx.audio.Music;
/**
 * 
 * @author David
 * The MusicHandler handles common tasks related to music.
 * This class makes sure that only one Song is played and that recently played ones are disposed.
 */
public class MusicHandler{

	private String path;
	private ContentManager contentManager;
	private boolean isPlayingMusic;
	private Music playingMusic;
	private static MusicHandler musicHandler;
	
	private MusicHandler(){
		contentManager = ContentManager.getInstance();
	}
	
	public static MusicHandler getInstance(){
		if(musicHandler == null){
			musicHandler = new MusicHandler();
		}
		return musicHandler;
	}
	
	public void playMusicOnce(Music m){
		if(isPlayingMusic){
			return;
		}
		if(playingMusic != null){
			playingMusic.stop();
			playingMusic.dispose();
			playingMusic = null;
		}
		isPlayingMusic = true;
		playingMusic = m;
		m.play();
	}
	
	public void playLoopingMusic(Music m){
		if(isPlayingMusic){
			return;
		}
		if(playingMusic != null){
			playingMusic.stop();
			playingMusic.dispose();
			playingMusic = null;
		}
		isPlayingMusic = true;
		playingMusic = m;
		playingMusic.setLooping(true);
		m.play();
	}
	
	public void playMusicOnce(String s){
		if(isPlayingMusic){
			return;
		}
		Music m = contentManager.get(s);
		if(playingMusic != null){
			playingMusic.stop();
			playingMusic.dispose();
			playingMusic = null;
		}
		path = s;
		isPlayingMusic = true;
		playingMusic = m;
		m.play();
	}
	
	public void playLoopingMusic(String s){
		if(isPlayingMusic){
			return;
		}
		Music m = contentManager.get(s);
		if(playingMusic != null){
			playingMusic.stop();
			playingMusic.dispose();
			playingMusic = null;
		}
		isPlayingMusic = true;
		playingMusic = m;
		path = s;
		playingMusic.setLooping(true);
		m.play();
	}
	
	public void stopMusic(){
		if(playingMusic != null){
			isPlayingMusic = false;
			playingMusic.stop();
			playingMusic.dispose();
		}
	}
	
	public void playSound(Music m){
		m.play();
		m.setOnCompletionListener(new disposeMusicListener());
	}
	
	public void playSound(Music m, float volume){
		m.setVolume(volume);
		m.play();
		m.setOnCompletionListener(new disposeMusicListener());
	}
	
	public void setVolume(float volume){
		if(playingMusic!= null){
			playingMusic.setVolume(volume);
		}
	}

	public boolean isPlayingMusic() {
		return isPlayingMusic;
	}
	
	public boolean isPlayingMusicEqual(String s){
		if(s.equals(path)){
			return true;
		}
		return false;
	}
}