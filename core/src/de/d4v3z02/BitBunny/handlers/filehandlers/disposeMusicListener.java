package de.d4v3z02.BitBunny.handlers.filehandlers;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Music.OnCompletionListener;

/**
 * 
 * @author David
 * disposes songs which have been played automatically
 */
public class disposeMusicListener implements OnCompletionListener {

	@Override
	public void onCompletion(Music music) {
		music.stop();
		music.dispose();
	}
}