package de.d4v3z02.BitBunny.handlers.inputhandlers;

import java.util.HashMap;
import java.util.Map;

import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.informer.enums.CharacterState;
import de.d4v3z02.BitBunny.informer.enums.KeyValues;

public class InGameInputHandler {

	private static InGameInputHandler inGameInputHandler;
	public int x;
	public int y;
	public boolean down;
	public boolean pdown;
	private final int NUM_KEYS;
	private Map<KeyValues, Boolean> keyValues;
	private Map<KeyValues, Boolean> pKeys;
	private Player player;

	private InGameInputHandler(){
		NUM_KEYS  = 5;
		keyValues = new HashMap<KeyValues, Boolean>();
		pKeys = new HashMap<KeyValues, Boolean>();
		setKey(KeyValues.JUMP, Boolean.FALSE);
		setKey(KeyValues.SPECIAL, Boolean.FALSE);
		setKey(KeyValues.LEFT, Boolean.FALSE);
		setKey(KeyValues.RIGHT, Boolean.FALSE);
		setKey(KeyValues.SHIFT, Boolean.FALSE);
		setKey(KeyValues.NEXT, Boolean.FALSE);
	}

	public void registerPlayer(Player player){
		this.player = player;
	}

	private void walk(){
		if(inGameInputHandler.isDown(KeyValues.LEFT)){
			player.moveLeft();
		}
		if(inGameInputHandler.isDown(KeyValues.RIGHT)){
			player.moveRight();
		}
	}

	private void stand(){
		player.getBody().setLinearVelocity(0, player.getBody().getLinearVelocity().y);
		player.setState(CharacterState.STANDING);
	}

	public void handleInput(){
		if(player.getState() != CharacterState.FROZEN){
			if(inGameInputHandler.isPressed(KeyValues.NEXT)){
				player.nextPowerUp();
			}
			if(inGameInputHandler.isPressed(KeyValues.JUMP)){
				player.jump();
			}
			if(inGameInputHandler.isPressed(KeyValues.SPECIAL)){
				player.useAbillity();
			}
			if(inGameInputHandler.isDown(KeyValues.SHIFT)){
				if(inGameInputHandler.isDown(KeyValues.LEFT) || inGameInputHandler.isDown(KeyValues.RIGHT)){
					player.setState(CharacterState.RUNNING);
					walk();
				}else{
					stand();
				}
			}else{
				if(inGameInputHandler.isDown(KeyValues.LEFT) || inGameInputHandler.isDown(KeyValues.RIGHT)){
					player.setState(CharacterState.WALKING);
					walk();
				}else{
					stand();
				}
			}
		}
	}

	public static InGameInputHandler getInstance(){
		if(inGameInputHandler == null){
			inGameInputHandler = new InGameInputHandler();
		}
		return inGameInputHandler;
	}
	/**
	 * @return the down
	 */
	public boolean isDown() {
		return down;
	}

	/**
	 * @param down the down to set
	 */
	public void setDown(boolean down) {
		this.down = down;
	}

	/**
	 * @return the pdown
	 */
	public boolean isPdown() {
		return pdown;
	}

	/**
	 * @param pdown the pdown to set
	 */
	public void setPdown(boolean pdown) {
		this.pdown = pdown;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the nUM_KEYS
	 */
	public int getNUM_KEYS() {
		return NUM_KEYS;
	}
	
	public void setKey(KeyValues keyValue, Boolean b){
		keyValues.put(keyValue, b);
	}
	
	public void update(){
		if(player != null){
			handleInput();
		}
		for (KeyValues key : KeyValues.values()) {	
			Boolean b = keyValues.get(key);
			pKeys.put(key, b);
		}
	}
	
	/**
	 * @param keyValue index
	 * @return boolean if the is pressed
	 */
	public boolean isDown(KeyValues keyValue){
		return keyValues.get(keyValue);
	}
	
	/**
	 * @param keyValue index
	 * @return boolean if the is was just pressed
	 */
	public boolean isPressed(KeyValues keyValue){
		return (keyValues.get(keyValue) && !pKeys.get(keyValue));
	}
	
	public boolean isPressed() {
		return down && !pdown; 
	}
	
	public boolean isReleased() {
		return !down && pdown; 
	}
}