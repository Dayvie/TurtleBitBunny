package de.d4v3z02.BitBunny.handlers.inputhandlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.informer.enums.KeyValues;
import de.d4v3z02.BitBunny.main.GameManager;
import de.d4v3z02.BitBunny.states.CreatePlayerScreen;
import de.d4v3z02.BitBunny.states.PlayScreen;
import de.d4v3z02.BitBunny.states.SelectGameFileScreen;
import de.d4v3z02.BitBunny.states.TitleScreen;

public class InGameInputProcessor extends InputAdapter implements ControllerListener {
	
	private boolean hasController;
	private static InGameInputProcessor inGameInputProcessor;
	private final InGameInputHandler INPUTHANDLER;
	private GameManager gsm;
	private final MusicHandler MUSICHANDLER;
	
	private InGameInputProcessor(){
		INPUTHANDLER = InGameInputHandler.getInstance();
		MUSICHANDLER = MusicHandler.getInstance();
		this.gsm = null;
		checkForCon();
	}

	public void registerGameScreenManager(GameManager gameScreenManager){
		this.gsm = gameScreenManager;
	}
	
	private void checkForCon(){
		try{
			Controllers.addListener(this);
			Controllers.getControllers().first();
			hasController = true;
		} catch (Exception e) {
			System.out.println("No Controller found");
		}
	}
	
	public static InGameInputProcessor getInstance(){
		if(inGameInputProcessor == null){
			inGameInputProcessor = new InGameInputProcessor();
		}
		return inGameInputProcessor;
	}
	
	public boolean mouseMoved(int x, int y) {
		INPUTHANDLER.setX(x);
		INPUTHANDLER.setY(y);
		return true;
	}
	
	@Override
	public boolean keyDown(int k){
		if (k == Keys.SPACE) {	
			INPUTHANDLER.setKey(KeyValues.JUMP, Boolean.TRUE);
		}
		if (k == Keys.S) {
			INPUTHANDLER.setKey(KeyValues.SPECIAL, Boolean.TRUE);
		}
		if (k == Keys.A) {
			INPUTHANDLER.setKey(KeyValues.LEFT, Boolean.TRUE);
		}
		if (k == Keys.D) {
			INPUTHANDLER.setKey(KeyValues.RIGHT, Boolean.TRUE);
		}
		if (k == Keys.W) {
			INPUTHANDLER.setKey(KeyValues.NEXT, Boolean.TRUE);
		}
		if (k == Keys.SHIFT_LEFT) {
			INPUTHANDLER.setKey(KeyValues.SHIFT, Boolean.TRUE);
		}
		if (k == Keys.ESCAPE){
			goBack();
		}
		return true;
	}

	public void goBack(){
		if(this.gsm != null){
			MUSICHANDLER.stopMusic();
			Class<?> screenClass = this.gsm.getScreen().getClass();
			if(screenClass == TitleScreen.class){
				Gdx.app.exit();
			}
            if(screenClass == PlayScreen.class){
                this.gsm.setScreen(new SelectGameFileScreen(gsm));
            }
            if(screenClass == SelectGameFileScreen.class){
                this.gsm.setScreen(new TitleScreen(gsm));
            }
            if(screenClass == CreatePlayerScreen.class){
                this.gsm.setScreen(new SelectGameFileScreen(gsm));
            }
		}
	}
	
	@Override
	public boolean keyUp(int k){
		if (k == Keys.SPACE) {
			INPUTHANDLER.setKey(KeyValues.JUMP, Boolean.FALSE);
		}
		if (k == Keys.S) {
			INPUTHANDLER.setKey(KeyValues.SPECIAL, Boolean.FALSE);
		}
		if (k == Keys.A) {
			INPUTHANDLER.setKey(KeyValues.LEFT, Boolean.FALSE);
		}
		if (k == Keys.D) {
			INPUTHANDLER.setKey(KeyValues.RIGHT, Boolean.FALSE);
		}
		if (k == Keys.W) {
			INPUTHANDLER.setKey(KeyValues.NEXT, Boolean.FALSE);
		}
		if (k == Keys.SHIFT_LEFT) {
			INPUTHANDLER.setKey(KeyValues.SHIFT, Boolean.FALSE);
		}
		return true;
	}

	@Override
	public void connected(Controller controller) {
		hasController = true;
	}

	@Override
	public void disconnected(Controller controller) {
		hasController = false;
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		if(buttonCode == 3){
			INPUTHANDLER.setKey(KeyValues.NEXT, Boolean.TRUE);
		}	
        if(buttonCode == 0){
        	INPUTHANDLER.setKey(KeyValues.JUMP, Boolean.TRUE);
        }	
        if(buttonCode == 2){
        	INPUTHANDLER.setKey(KeyValues.SHIFT, Boolean.TRUE);
        }	
        if(buttonCode == 5){
        	INPUTHANDLER.setKey(KeyValues.SHIFT, Boolean.TRUE);
        }	
        if(buttonCode == 1){
        	INPUTHANDLER.setKey(KeyValues.SPECIAL, Boolean.TRUE);
        }
		return false;
	}

	@Override
	public boolean buttonUp(Controller controller, int buttonCode) {
		if(buttonCode == 3){
			INPUTHANDLER.setKey(KeyValues.NEXT, Boolean.FALSE);
		}	
		if(buttonCode == 0)
        	INPUTHANDLER.setKey(KeyValues.JUMP, Boolean.FALSE);
        if(buttonCode == 2)
        	INPUTHANDLER.setKey(KeyValues.SHIFT, Boolean.FALSE);
        if(buttonCode == 5){
        	INPUTHANDLER.setKey(KeyValues.SHIFT, Boolean.FALSE);
        }	
        if(buttonCode == 1)
        	INPUTHANDLER.setKey(KeyValues.SPECIAL, Boolean.FALSE);
		return false; 
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
        if(axisCode == 1 && value >0.4){
        	INPUTHANDLER.setKey(KeyValues.LEFT, Boolean.FALSE);
        		INPUTHANDLER.setKey(KeyValues.RIGHT, Boolean.TRUE);
        }
        
        if(axisCode == 1 && value < 0.4){
        	INPUTHANDLER.setKey(KeyValues.RIGHT, Boolean.FALSE);
    		INPUTHANDLER.setKey(KeyValues.LEFT, Boolean.TRUE);
        }
        if(value < 0.4 && value > -0.4 && axisCode == 1){
        	INPUTHANDLER.setKey(KeyValues.LEFT, Boolean.FALSE);
        	INPUTHANDLER.setKey(KeyValues.RIGHT, Boolean.FALSE);
        }
        return false;
	}

	@Override
	public boolean povMoved(Controller controller, int povCode,
			PovDirection value) {
		if(value == PovDirection.east){
			INPUTHANDLER.setKey(KeyValues.RIGHT, Boolean.TRUE);
		}
		if(value == PovDirection.west){
			INPUTHANDLER.setKey(KeyValues.LEFT, Boolean.TRUE);
		}
		if(value == PovDirection.center){
			INPUTHANDLER.setKey(KeyValues.RIGHT, Boolean.FALSE);
			INPUTHANDLER.setKey(KeyValues.LEFT, Boolean.FALSE);
		}
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller controller, int sliderCode,
			boolean value) {
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller controller, int sliderCode,
			boolean value) {
		return false;
	}

	@Override
	public boolean accelerometerMoved(Controller controller,
			int accelerometerCode, Vector3 value) {
		return false;
	}

	public boolean hasController() {
		return hasController;
	}
}