package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.graphics.g2d.Sprite;



public class Animation{
	
	private Sprite unanimatedSprite;
	private Sprite[] frames;
	private float time;
	private float delay;
	private int currentFrame;
	private int timesPlayed;
	
	public Animation(){
		
	}
	
	public Animation(Sprite[] frames){
		this(frames, 1/12f);
	}
	
	public Sprite getUnanimatedSprite(){
		return unanimatedSprite;
	}
	
	public Animation(Sprite[] frames, float delay){
		setFrames(frames, delay);
	}
	
//	public void setInvcibillityFrames(Sprite[] frames, float delay){
//		Sprite[] invincibillity = new Sprite[frames.length*2];
//		int invincibillityIndex = 0;	
//		for(Sprite sprite: frames){
//			invincibillity[invincibillityIndex]  = sprite;
//			invincibillityIndex+=2;
//		}
//		this.frames = invincibillity;
//		this.delay = delay;
//		time = 0;
//		currentFrame = 0;
//		timesPlayed = 0;
//	}
	
	public void setFrames(Sprite[] frames, float delay){
		this.frames = frames;
		this.unanimatedSprite = frames[0];
		this.delay = delay;
		time = 0;
		currentFrame = 0;
		timesPlayed = 0;
	}
	
	public void update(float dt){
		if(delay<=0){
			return;
		}
		time += dt;
		while(time>=delay){
			step();
		}
	}
	
	private void step(){
		time -= delay;
		currentFrame++;
		if(currentFrame == frames.length){
			currentFrame = 0;
			timesPlayed++;
		}
	}
	
	public Sprite getFrame(){
		return frames[currentFrame];
	}
	
	public int getTimesPlayed(){
		return timesPlayed;
	}
	
	public String toString(){
		return "animationlength: "+ frames.length;
	}
}