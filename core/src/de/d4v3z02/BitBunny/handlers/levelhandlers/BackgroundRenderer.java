package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import de.d4v3z02.BitBunny.handlers.BoundedCamera;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.informer.enums.LevelAsset;
import de.d4v3z02.BitBunny.main.GameManager;

/**
 * Created by dayvie on 23.06.2016.
 */
public class BackgroundRenderer {

    private Array<Background> backgrounds;
    private BoundedCamera gameCam;
    private SpriteBatch spriteBatch;

    public BackgroundRenderer(LevelAsset levelAsset, BoundedCamera gameCam, SpriteBatch spriteBatch){
        backgrounds = new Array<>();
        this.spriteBatch = spriteBatch;
        this.gameCam = gameCam;
        Texture bgs = ContentManager.getInstance().get(levelAsset.bg, Texture.class);
        TextureRegion sky = new TextureRegion(bgs, 0, 0, GameManager.GAME_WORLD_WIDTH, GameManager.GAME_WORLD_HEIGHT);
        TextureRegion clouds = new TextureRegion(bgs, 0, 480, GameManager.GAME_WORLD_WIDTH, GameManager.GAME_WORLD_HEIGHT);
        TextureRegion mountains = new TextureRegion(bgs, 0, 960, 640, GameManager.GAME_WORLD_HEIGHT);
        backgrounds = new Array<>(3);
        backgrounds.add(new Background(sky, gameCam, 0f, spriteBatch));
        backgrounds.add(new Background(clouds, gameCam, 0.1f, spriteBatch));
        backgrounds.add(new Background(mountains, gameCam, 0f, spriteBatch));
    }

    public void render(){
        for (Background background: backgrounds){
            background.render();
        }
    }
}
