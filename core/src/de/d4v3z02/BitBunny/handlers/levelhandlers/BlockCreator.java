package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.BlockTypes;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

public class BlockCreator {

	private TiledMap tileMap;
	private World world;
	
	public BlockCreator(TiledMap tileMap, World world){
		this.tileMap = tileMap;
		this.world = world;
	}
	
	public void createAllBlocks() {
		TiledMapTileLayer layer;
		for(BlockTypes blockType: BlockTypes.values()){
			layer = (TiledMapTileLayer) tileMap.getLayers().get(blockType.loadName);
			if(layer != null){
				createBlocks(layer, blockType.categoryBit.value);
			}
		}
	}
	
	private void createBlocks(TiledMapTileLayer layer, short bits) {
		float ts = layer.getTileWidth()/ Numbers.PPM.value;
		float tileSizeDividedByTwo = ts / 2;
		for(int row = 0; row < layer.getHeight(); row++) {
			for(int col = 0; col < layer.getWidth(); col++) {
				Cell cell = layer.getCell(col, row);
				if(cell == null) continue;
				if(cell.getTile() == null) continue;		
				ChainShape cs = new ChainShape();
				cs = createBlockRectangle(cs, tileSizeDividedByTwo);
				BodyDef bdef = new BodyDef();
				bdef.type = BodyType.StaticBody;
				bdef.position.set((col + 0.5f) * ts, (row + 0.5f) * ts);
				FixtureDef fd = new FixtureDef();
				fd.friction = 0;
				fd.shape = cs;
				fd.filter.categoryBits = bits;
				fd.filter.maskBits = (short)(Bits.PLAYER.value | Bits.ENEMY.value| Bits.FOOT.value | Bits.CRYSTAL.value | Bits.GROUND.value | Bits.POWERUP.value);
				Body body = world.createBody(bdef);
				body.createFixture(fd).setUserData("plattform");	
				body.setUserData(bits);
				cs.dispose();
			}
		}
	}
	
	private ChainShape createBlockRectangle(ChainShape cs, float tileSizeDividedByTwo){
		Vector2[] v = new Vector2[5];
		v[0] = new Vector2(-tileSizeDividedByTwo, -tileSizeDividedByTwo);
		v[1] = new Vector2(-tileSizeDividedByTwo, tileSizeDividedByTwo);
		v[2] = new Vector2(tileSizeDividedByTwo, tileSizeDividedByTwo);
		v[3] = new Vector2(tileSizeDividedByTwo, -tileSizeDividedByTwo);
		v[4] = new Vector2(-tileSizeDividedByTwo, -tileSizeDividedByTwo);
		cs.createChain(v);
		return cs;
	}
}