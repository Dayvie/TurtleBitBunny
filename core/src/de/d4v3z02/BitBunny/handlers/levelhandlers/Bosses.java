package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.utils.Array;
import de.d4v3z02.BitBunny.entities.characters.enemies.Enemy;

/**
 * Created by dayvie on 02.07.2016.
 */
public class Bosses {

    private Array<Enemy> enemies;
    private static Bosses bosses;

    private Bosses(){
        enemies = new Array<>();
    }

    public static Bosses getInstance(){
        if(bosses == null){
            bosses = new Bosses();
        }
        return bosses;
    }

    public void add(Enemy boss){
        enemies.add(boss);
    }

    public void activateBosses(){
        for(Enemy boss: enemies){
            EntityService.getInstance().add(boss);
            boss.activate();
        }
        enemies.clear();
    }

    public Enemy getFirstBoss(){
        return enemies.get(0);
    }
}
