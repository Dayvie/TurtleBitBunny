package de.d4v3z02.BitBunny.handlers.levelhandlers;

import de.d4v3z02.BitBunny.entities.entitiesonmap.SwitchBlock.Color;

public class ColorHolder {
	
	private static ColorHolder singleInstance;
	
	private Color color;
	
	private ColorHolder(){
		this.setColor(Color.RED);
	}
	
	public static ColorHolder getInstance(){
		if(singleInstance == null){
			singleInstance =  new ColorHolder();
		}
		return singleInstance;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public void reset(){
		this.color = Color.RED;
	}
}