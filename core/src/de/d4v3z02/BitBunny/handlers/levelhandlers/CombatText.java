package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import de.d4v3z02.BitBunny.informer.enums.Numbers;

public abstract class CombatText {

	protected String combatText;
	protected float lifetime;
	protected BitmapFont font;
	protected Vector2 position;
	protected SpriteBatch sb;
	protected boolean removeFlag;
	protected float lifeConstant;
	
	public CombatText(String combatText, BitmapFont font, SpriteBatch sb, Vector2 position){
		this.position = position;
		this.sb = sb;
		this.combatText = combatText;
		this.font = font;
		lifetime = 0;
		lifeConstant = 32;
	}
	
	public boolean shouldBeRemoved(){
		return this.removeFlag;
	}
	
	public BitmapFont getFont(){
		return this.font;
	}
	
	public String getCombatText(){
		return combatText;
	}
	
	public void update(float dt){
		lifetime += dt;		
	}
	
	public void render(){
		sb.begin();
		font.draw(sb, combatText, position.x * Numbers.PPM.value + (lifeConstant* lifetime)/4 + 16,
			position.y * Numbers.PPM.value + (lifeConstant* lifetime) + 16 );
		sb.end();
	}
}
