package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public abstract class CombatTextNumber extends CombatText{
	
	public CombatTextNumber(int number, BitmapFont font, SpriteBatch sb, Vector2 position){
		super(number+"", font, sb, position);
	}
}