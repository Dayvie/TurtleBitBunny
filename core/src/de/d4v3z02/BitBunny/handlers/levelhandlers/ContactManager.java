package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.entities.characters.Player.PowerUpState;
import de.d4v3z02.BitBunny.entities.characters.enemies.Enemy;
import de.d4v3z02.BitBunny.entities.entitiesonmap.*;
import de.d4v3z02.BitBunny.entities.entitiesonmap.MovingPlattform.MovingDirection;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.Crystal;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.Heartcontainer;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.Key;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.KeyBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.powerup.BlueCherry;
import de.d4v3z02.BitBunny.entities.entitiesonmap.powerup.FireCherry;
import de.d4v3z02.BitBunny.entities.entitiesonmap.powerup.Heart;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggers.Trigger;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionDown;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionLeft;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionNull;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionRight;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionUp;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile.SpawnPosition;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFileManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.*;
import de.d4v3z02.BitBunny.informer.enums.Assets;
import de.d4v3z02.BitBunny.informer.enums.Bits;
import de.d4v3z02.BitBunny.informer.enums.ElementalTypes;
import de.d4v3z02.BitBunny.informer.enums.Fonts;
/**
 * 
 * @author David
 *Class which handles collisions of entities ingame
 */
public class ContactManager implements ContactListener {
	
	public enum PlayerGoes{
		NOWHERE, AHEAD, BACK, SECRET, SECRETBACK;
	}
	private boolean blockswitched;
	private static ContactManager contactManager;
	private int numFootContacts;
	private ContentManager contentManager;
	private Player player;
	private GameFileManager gameFileManager;
	private MusicHandler musicHandler;
	private PlayerGoes playerGoes;
	private PlayerDamageHandler playerDamageHandler;
	private GameFile gameFile;
	private UserInterfaceManager userInterfaceManager;
	private FloatingCombatTextHolder floatingCombatTextHolder;
	
	private ContactManager(){
		playerDamageHandler = PlayerDamageHandler.getInstance();
		playerGoes = PlayerGoes.NOWHERE;
		contentManager = ContentManager.getInstance();
		gameFileManager = GameFileManager.getInstance();
		musicHandler = MusicHandler.getInstance();
		floatingCombatTextHolder = FloatingCombatTextHolder.getInstance();
	}
	
	public static ContactManager getInstance(){
		if(contactManager == null){
			contactManager = new ContactManager();
		}
		return contactManager;
	}
	
	public void setPlayer(Player player){
		this.player = player;
		gameFile = player.getGameFile();
		userInterfaceManager = new UserInterfaceManager(player);
	}

	public UserInterfaceManager getUserInterfaceManager(){
		return userInterfaceManager;
	}

	/**
	 * Collision detected
	 * Works, when two objects start to be imposed. prokaet only within the limits of a step.
	 */
	@Override
	public void beginContact(Contact contact) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();
		if(fa == null || fb == null) return;
		if (fa.getUserData().equals("mp")){
			if(fb.getUserData().equals("dp")){
				handlePlattformMovement(fa, fb);
			}
			if(fb.getUserData().equals("player")){
				((MovingPlattform)fa.getBody().getUserData()).setMovingDirection(MovingDirection.RIGHT);
			}
			return;
		}
		if (fb.getUserData().equals("mp")){
			if(fa.getUserData().equals("dp")){
				handlePlattformMovement(fb, fa);
			}
			if(fa.getUserData().equals("player")){
					((MovingPlattform)fb.getBody().getUserData()).setMovingDirection(MovingDirection.RIGHT);
			}
			return;
		}
		if (fb.getUserData().equals("player")){
			if(fa.getUserData().equals("trigger")){
				playerCollidesWithTrigger(fa);
			}
			if(fa.getUserData().equals("checkpoint")) {
				playerReachesCheckpoint(fa);
				return;
			}
			if(fa.getUserData().equals("bgoal")) {
				playerGoes = PlayerGoes.BACK;
				return;
			}
			if(fa.getUserData().equals("goal")) {
				playerGoes = PlayerGoes.AHEAD;
				return;
			}
			if(fa.getUserData().equals("enemy")){
				playerDamageHandler.handleBodyDamage(player, (Enemy)fa.getBody().getUserData());
				return;
			}	
			if(fa.getUserData().equals("gold")) {
				playerCollectsGold(fa);
				return;
			}		
			if(fa.getUserData().equals("enemyFireBall")){
				player.decreaseHP(((Projectile)fa.getBody().getUserData()).getCharacter().getProjectileDmg(),
						ElementalTypes.FIRE);
				System.out.print("playerdmg");
				return;
			}			
			if(fa.getUserData().equals("powerup")){
				playerCollectsItem(fa);
				return;
			}		
			if(fa.getUserData().equals("keyblock")) {
				handleKeyBlock(fa);
				return;
			}
			if(fa.getUserData().equals("switchblock")) {
				player.switchBlocks();
				musicHandler.playSound((Music) contentManager.get("res/sfx/hit.wav"));
				return;
			}
			if(fa.getUserData().equals("sgoal")) {
				playerGoes = PlayerGoes.SECRET;
				return;
			}
			if(fa.getUserData().equals("sbgoal")) {
				playerGoes = PlayerGoes.SECRETBACK;
				return;
			}
			if(fa.getUserData().equals("plattform") && fa.getBody().getUserData().getClass() == TimerBlock.class){
				handleTimerBlockActivation(fa);
				return;
			}
		}
		
		if (fa.getUserData().equals("player")){
			if(fb.getUserData().equals("trigger")){
				playerCollidesWithTrigger(fb);
			}
			if (fb.getUserData().equals("bgoal")) {
				playerGoes = PlayerGoes.BACK;
				return;
			}
			if (fb.getUserData().equals("goal")) {
				playerGoes = PlayerGoes.AHEAD;
				return;
			}
			if (fb.getUserData().equals("gold")) {
				playerCollectsGold(fb);
				return;
			}			
			if (fb.getUserData().equals("enemy")){
				playerDamageHandler.handleBodyDamage(player, (Enemy)fb.getBody().getUserData());
				return;
			}		
			if (fb.getUserData().equals("enemyFireBall")){
				player.decreaseHP(((Projectile)fb.getBody().getUserData()).getCharacter().getProjectileDmg(),
						ElementalTypes.FIRE);
				System.out.print("playerdmg");
				return;
			}			
			if (fb.getUserData().equals("powerup")){
				playerCollectsItem(fb);
				return;
			}		
			if (fb.getUserData().equals("keyblock")) {
				handleKeyBlock(fb);
				return;
			}
			if (fb.getUserData().equals("switchblock")) {
				player.switchBlocks();
				musicHandler.playSound((Music) contentManager.get("res/sfx/hit.wav"));
				return;
			}
			if (fb.getUserData().equals("checkpoint")) {
				playerReachesCheckpoint(fb);
				return;
			}
			if (fb.getUserData().equals("sgoal")) {
				playerGoes = PlayerGoes.SECRET;
				return;
			}
			if (fb.getUserData().equals("sbgoal")) {
				playerGoes = PlayerGoes.SECRETBACK;
				return;
			}
			if (fb.getUserData().equals("plattform") && fb.getBody().getUserData().getClass() == TimerBlock.class){
				handleTimerBlockActivation(fb);
				return;
			}
		}
		
		if (fb.getUserData().equals("foot")){
			if (fa.getUserData().equals("enemyhead")){
				handleEnemyBodyDamage(fa);
				return;
			}
			if (fa.getUserData().equals("plattform")){
				numFootContacts++;	
				return;
			}
		}
		
		if (fa.getUserData().equals("foot")){
			if (fb.getUserData().equals("enemyhead")){
				handleEnemyBodyDamage(fb);
				return;
			}
			if (fb.getUserData().equals("plattform")){
				numFootContacts++;
				return;
			}
		}
		
		if (fa.getUserData().equals("enemy")&& fb.getUserData().equals("edge")){
			((Enemy)fa.getBody().getUserData()).changeDirection();
			return;
		}
		
		if (fb.getUserData().equals("enemy") && fa.getUserData().equals("edge")){
			((Enemy)fb.getBody().getUserData()).changeDirection();
			return;
		}
		
		if (fb.getUserData().equals("playerFireBall")){
			enemyTakesFireBallDmg(fa, fb);
			return;
		}
		
		if (fa.getUserData().equals("playerFireBall")){
			enemyTakesFireBallDmg(fb, fa);
			return;
		}
		
		if (fa.getUserData().equals("crystal")) {
			playerCollectsCrystal(fa);
			return;
		}
		
		if (fb.getUserData().equals("crystal")) {
			playerCollectsCrystal(fb);
			return;
		}
	}

	/**
	 * Collision ended
	 * Works, when two objects stop to adjoin. 
	 * Can be caused, when the body is destroyed, thus, this event can to take place out of a time step.
	 */
	@Override
	public void endContact(Contact contact) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();
		if(fa == null || fb == null) return;
		if (fa.getUserData().equals("plattform")&& fb.getUserData().equals("foot")){
			numFootContacts--;
			return;
		}
		if (fb.getUserData().equals("plattform")&& fb.getUserData().equals("foot")){
			numFootContacts--;
			return;
		}
		
		if (fa.getUserData().equals("enemy")&& fb.getUserData().equals("player")){
			playerDamageHandler.finishHandlingDamage();
			return;
		}
		
		if (fb.getUserData().equals("enemy") && fa.getUserData().equals("player")){
			playerDamageHandler.finishHandlingDamage();
			return;
		}
	}
	
	private void handleKeyBlock(Fixture keyblockFixture){
		KeyBlock keyblock = (KeyBlock) keyblockFixture.getBody().getUserData();
		if(gameFile.getKeyAmount() > 0){
			gameFile.activateItem(keyblock.getID());
			keyblock.remove();
			gameFile.spendKey();
		}
	}
	
	private void playerReachesCheckpoint(Fixture checkpoint){
		Checkpoint checkpointRef = (Checkpoint)checkpoint.getBody().getUserData();
		gameFile.setCheckpointID(checkpointRef.getID());
		if(!checkpointRef.isActive()){
			checkpointRef.activate();
			EntityService.getInstance().deactivateOtherCheckpoints(gameFile.getCheckpointID());
			musicHandler.playSound(contentManager.get("res/sfx/changeblock.wav"));
			gameFile.setSpawnPosition(SpawnPosition.CP);
			gameFileManager.saveAll();
		}
	}

	private void playerCollidesWithTrigger(Fixture triggerFixture){
		switch(((Trigger)triggerFixture.getBody().getUserData()).getTriggerType()){
			case SHOPTRIGGER:
				userInterfaceManager.activateGUI(new ShopWindow(player, userInterfaceManager));
				break;
			case BOSSTRAPTRIGGER:
				activateBossTraps((Trigger)triggerFixture.getBody().getUserData());
				break;
			default:
				break;
		}
	}

	private void activateBossTraps(Trigger trigger){
		trigger.remove();
		musicHandler.stopMusic();
		musicHandler.playLoopingMusic(Assets.BOWSERSONG.path);
		EntityService.getInstance().activateTrapBlocks();
		Bosses bosses = Bosses.getInstance();
		userInterfaceManager.activateGUI(new BossHUD(userInterfaceManager, player, bosses.getFirstBoss()));
		bosses.activateBosses();
	}
	
	private void playerCollectsItem(Fixture powerUp){
		if(powerUp.getBody().getUserData().getClass() == Key.class){
			gameFile.collectKey(((Key)powerUp.getBody().getUserData()).getID());
			floatingCombatTextHolder.add(new RisingCombatText("Gained Key", Fonts.PLAYERMONEY.font, player.getSpriteBatch(),
					player.getBody().getPosition()));
		}
		if(powerUp.getBody().getUserData().getClass() == Heartcontainer.class){
			player.collectHearthcontainer();
			gameFile.activateItem(((Heartcontainer)powerUp.getBody().getUserData()).getID());
			floatingCombatTextHolder.add(new RisingCombatText("Collected Hearthcontainer", Fonts.PLAYERMONEY.font, player.getSpriteBatch(),
					player.getBody().getPosition()));
		}
		if(powerUp.getBody().getUserData().getClass() == FireCherry.class){
			gameFile.addPowerUp(PowerUpState.FIRECHERRY);
			floatingCombatTextHolder.add(new RisingCombatText("Collected Firecherry", Fonts.PLAYERMONEY.font, player.getSpriteBatch(),
					player.getBody().getPosition()));
			userInterfaceManager.activateGUI(new Dialog(userInterfaceManager, player, "You have collected the FireCherry! Press S to unleash the wrath of 1000 Bitbunnies!\n (switch into the Firemode with W)"));
		}
		if(powerUp.getBody().getUserData().getClass() == BlueCherry.class){
			gameFile.addPowerUp(PowerUpState.BLUECHERRY);
			floatingCombatTextHolder.add(new RisingCombatText("Collected Bluecherry", Fonts.PLAYERMONEY.font, player.getSpriteBatch(),
					player.getBody().getPosition()));
		}
		if(powerUp.getBody().getUserData().getClass() == Heart.class){
			player.increaseHP();
		}
		((Entity)powerUp.getBody().getUserData()).remove();
	}
	
	private void enemyTakesFireBallDmg(Fixture enemy, Fixture fireball){
		if(((Enemy)enemy.getBody().getUserData()).isActive()){
			((Entity)fireball.getBody().getUserData()).remove();
			((Enemy)enemy.getBody().getUserData()).decreaseHP(gameFile.getFireBallDmg(),
					ElementalTypes.FIRE);
		}
	}
	
	private void handlePlattformMovement(Fixture plattform, Fixture pointer){
		if(pointer.getUserData().equals("dp")){
			if(pointer.getBody().getUserData().getClass() == DirectionUp.class){
				((MovingPlattform)plattform.getBody().getUserData()).setMovingDirection(MovingDirection.UP);
			}
			if(pointer.getBody().getUserData().getClass() == DirectionDown.class){
				((MovingPlattform)plattform.getBody().getUserData()).setMovingDirection(MovingDirection.DOWN);
			}
			if(pointer.getBody().getUserData().getClass() == DirectionLeft.class){
				((MovingPlattform)plattform.getBody().getUserData()).setMovingDirection(MovingDirection.LEFT);
			}
			if(pointer.getBody().getUserData().getClass() == DirectionRight.class){
				((MovingPlattform)plattform.getBody().getUserData()).setMovingDirection(MovingDirection.RIGHT);
			}
			if(pointer.getBody().getUserData().getClass() == DirectionNull.class){
				((MovingPlattform)plattform.getBody().getUserData()).setMovingDirection(MovingDirection.NULL);
			}
		}
	}
	
	private void handleTimerBlockActivation(Fixture timerBlockFixture){
		TimerBlock timerBlock = (TimerBlock)timerBlockFixture.getBody().getUserData();
		timerBlock.activate();
	}
	
	private void handleEnemyBodyDamage(Fixture enemy){
		player.getBody().setLinearVelocity(player.getBody().getLinearVelocity().x, 5);
		((Enemy)enemy.getBody().getUserData()).decreaseHP(player.getBodyDmg(),
				ElementalTypes.PHYSICAL);
	}
	
	private void playerCollectsGold(Fixture goldFixture){
		Gold gold = (Gold)goldFixture.getBody().getUserData();
		player.collectGold(gold.getValue());
		musicHandler.playSound((Music) contentManager.get("res/sfx/coin.mp3"), 0.1f);
		gold.remove();
	}
	
	private void playerCollectsCrystal(Fixture crystal){
		musicHandler.playSound((Music) contentManager.get("res/sfx/crystal.mp3"), 0.2f);
		((Crystal)crystal.getBody().getUserData()).remove();
		gameFile.collectCrystal(((Crystal)crystal.getBody().getUserData()).getID());
	}

	/**
	 * Works after collision detection, but before its processing.
	 */
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();
		if (fa.getBody().getUserData().equals(Bits.PASSABLE.value)  &&  fb.getUserData().equals("player")){
			if(fb.getBody().getLinearVelocity().y > 0){
				contact.setEnabled(false);
				return;
			}
		}
		if (fb.getBody().getUserData().equals(Bits.PASSABLE.value)  &&  fa.getUserData().equals("player")){
			if(fa.getBody().getLinearVelocity().y > 0){
				contact.setEnabled(false);
				return;
			}
		}
		if (fa.getUserData().equals("plattform")&& fb.getUserData().equals("enemy")){
			if(fb.getBody().getLinearVelocity().y > 0){
				contact.setEnabled(false);
				return;
			}
		}
		if (fb.getUserData().equals("plattform")&& fa.getUserData().equals("enemy")){
			if(fa.getBody().getLinearVelocity().y > 0){
				contact.setEnabled(false);
				return;
			}
		}
	}
	
	/**
	 * The method allows to carry out logic of game who changes physics after contact.
	 *  For example, to deform or destroy object after contact. However, Box2D does not 
	 *  allow you to change physics in a method because you could destroy objects which 
	 *  Box2D now processes, leading to an error. 
	 */
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		Fixture fa = contact.getFixtureA();
		Fixture fb = contact.getFixtureB();
	}
	
	public PlayerGoes playerGoes(){
		if (playerGoes != PlayerGoes.NOWHERE){
			PlayerGoes saveGoes = playerGoes;
			playerGoes = PlayerGoes.NOWHERE;
			return saveGoes;
		}
		return PlayerGoes.NOWHERE;
	}
	
	public boolean isPlayerOnGround(){
		return numFootContacts > 0;
	}

	public void resetContacts(){
		numFootContacts = 0;
	}

	public boolean isBlockswitched() {
		if(blockswitched == true){
			blockswitched = false;
			return true;
		}
		return blockswitched;
	}
	
	public void update(float dt){
		userInterfaceManager.update(dt);
	}
	
	public void render(){
		userInterfaceManager.render();
	}
}