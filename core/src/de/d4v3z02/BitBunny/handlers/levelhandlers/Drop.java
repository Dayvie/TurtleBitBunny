package de.d4v3z02.BitBunny.handlers.levelhandlers;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import de.d4v3z02.BitBunny.entities.Entity;

/**
 * Created by dayvie on 30.06.2016.
 */
public abstract class Drop {

    protected float x, y;
    protected Entity entity;
    protected Vector2 pos;
    protected Object entityClass;
    protected EntityService entities;
    protected World world;
    protected SpriteBatch sb;


    public Drop(Entity entity, World world, SpriteBatch sb){
        this.world = world;
        this.sb = sb;
        entities = EntityService.getInstance();
        this.entity = entity;
        pos = entity.getPosition();
        entityClass = entity.getClass();
        x = pos.x;
        y = pos.y;
        dropItem();
    }

    public abstract void dropItem();
}
