package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.entities.characters.enemies.BigSiciBoss;
import de.d4v3z02.BitBunny.entities.characters.enemies.Boss;
import de.d4v3z02.BitBunny.entities.characters.enemies.JumpingSici;
import de.d4v3z02.BitBunny.entities.characters.enemies.RunningSici;
import de.d4v3z02.BitBunny.entities.characters.enemies.ShootingSici;
import de.d4v3z02.BitBunny.entities.characters.enemies.ShootingSiciBoss;
import de.d4v3z02.BitBunny.entities.characters.enemies.Sici;
import de.d4v3z02.BitBunny.entities.characters.enemies.WSSici;
import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;
import de.d4v3z02.BitBunny.entities.entitiesonmap.TrapBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.Heartcontainer;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggers.Trigger;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.informer.enums.Triggertype;

public class DropFactory {
	
	private Array<Entity> entities;
	private SpriteBatch spriteBatch;
	private World world;
	private GameFile gameFile;
	
	public DropFactory(World world, SpriteBatch sb, GameFile gameFile, Array<Entity> entities){
		this.entities = entities;
		this.gameFile = gameFile;
		this.world = world;
		this.spriteBatch = sb;
	}
	
	public void dropItem(Entity entity){
		Vector2 pos = entity.getPosition();
		Object entityClass = entity.getClass();
		float x = pos.x;
		float y = pos.y;
		if((entityClass == Sici.class) || (entityClass == JumpingSici.class) ||
				(entityClass == RunningSici.class)){
			new SiciDrop(entity, world, spriteBatch);
		}
		if((entityClass == ShootingSici.class) || (entityClass == WSSici.class)){
			new SiciDropLevelTwo(entity, world, spriteBatch);
		}
		if((entityClass == BigSiciBoss.class)){
			handleSiciBossDrop(x, y, (Boss)entity);
		}
		if((entityClass == ShootingSiciBoss.class)){
			handleShootingSiciBossDrop(x, y, (Boss)entity);
		}
	}
	
	private void handleSiciBossDrop(float x, float y, Boss boss){
		EntityOnMap drop;
		if(!gameFile.isItemActivated(boss.getID())){
			drop = new Heartcontainer(world, spriteBatch, x, y, 1337);
			drop.jump();
			entities.add(drop);
		}
	}

	private void handleShootingSiciBossDrop(float x, float y, Boss boss){
		EntityOnMap drop;
		if(!gameFile.isItemActivated(boss.getID())){
			drop = new Heartcontainer(world, spriteBatch, x, y, 1337);
			drop.jump();
			entities.add(drop);
		}
		for(Entity entity: entities){
			if(entity.getClass() == TrapBlock.class){
				((TrapBlock)entity).deactivate();
			}
		}
		removeBossTriggers();
	}

	public void removeBossTriggers(){
		for(Entity entity: entities){
			if(entity.getClass() == Trigger.class && ((Trigger)entity).getTriggerType() == Triggertype.BOSSTRAPTRIGGER){
				entity.remove();
			}
			if(entity.getClass() == TrapBlock.class){
				entity.remove();
			}
		}
	}
}
