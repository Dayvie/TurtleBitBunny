package de.d4v3z02.BitBunny.handlers.levelhandlers;

import java.lang.reflect.InvocationTargetException;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.Player.PowerUpState;
import de.d4v3z02.BitBunny.entities.characters.enemies.BigSiciBoss;
import de.d4v3z02.BitBunny.entities.characters.enemies.Enemy;
import de.d4v3z02.BitBunny.entities.entitiesonmap.BossBlockade;
import de.d4v3z02.BitBunny.entities.entitiesonmap.RemovedByTriggerBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.PlayerWentBackSpawnPoint;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.SecretPlayerWentBackSpawnPoint;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.IdentifiableEntity;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggers.Trigger;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.informer.enums.EntityType;
import de.d4v3z02.BitBunny.informer.enums.Triggertype;

/**
 * Class which is responsible for building all ingame Entities
 */
public class EntityFactory {
	
	private Array<Entity> entities;
	private Array<Enemy> inactiveEnemies;
	private GameFile gameFile;
	private int maxCrystalCount = 0;
	private Array<Projectile> enemyProjectiles;
	private Array<Entity> removeBlocks = new Array<>();
	private Array<Entity> bossBlocks = new Array<>();
	private int constructionID = 0;
	private SpriteBatch spriteBatch;
	private World world;
	private boolean bossDefeated;
	private Bosses bosses = Bosses.getInstance();

	public EntityFactory(World world, SpriteBatch sb, GameFile gameFile, Array<Entity> entities, Array<Enemy> inactiveEnemies, Array<Projectile> activeProjectilves){
		this.entities = entities;
		this.spriteBatch = sb;
		this.world = world;
		this.inactiveEnemies = inactiveEnemies;
		this.gameFile = gameFile;
		this.enemyProjectiles = activeProjectilves;
	}
	
	public int getMaxCrystalCount(){
		return maxCrystalCount;
	}

	/**
	 * removes the blockade ifthe boss has been defeated
	 */
	public void removeBossBlocks(){
		for(Entity removeBlock: bossBlocks){
			removeBlock.remove();
		}
	}
	
	/**
	 * @param entityType Type of built entity
	 * @param mapObject the mapeobject read out of the xml file
	 * @throws IllegalArgumentException when Entitytype is not found
	 */
	public void createEntity(EntityType entityType, MapObject mapObject)throws IllegalArgumentException{
		Entity entity;
		switch(entityType){
			case SICI:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case JUMPINGSICI:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case SHOOTINGSICI:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case SHOOTINGSICIRIGHT:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, true, entityType);
				break;
			case WALKINGSHOOTINGSICI:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case RUNNINGSICI:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case TRAPBLOCK:
				createSimpleEntity(world, spriteBatch, mapObject, entityType.TRAPBLOCK);
				break;
			case BAT:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case BATRIGHT:
				createEnemy(world, spriteBatch, mapObject, enemyProjectiles, true, entityType);
				break;
            case SICIBOSS:
				createBoss(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case SHOOTINGSICIBOSS:
				createBoss(world, spriteBatch, mapObject, enemyProjectiles, false, entityType);
				break;
			case FIRECHERRY:
				if(gameFile.powDoesntContain(PowerUpState.FIRECHERRY)){
					createSimpleEntity(world, spriteBatch, mapObject, entityType);
				}
				break;
			case BLUECHERRY:
				if(gameFile.powDoesntContain(PowerUpState.BLUECHERRY)){
					createSimpleEntity(world, spriteBatch, mapObject, entityType);
				}
				break;
			case CHECKPOINT:
				createIdentifiableEntity(world, spriteBatch, mapObject, entityType);
				break;
			case CRYSTAL:
				maxCrystalCount++;
				createIdentifiableEntity(world, spriteBatch, mapObject, entityType);
				break;
			case HEARTCONTAINER:
				createIdentifiableEntity(world, spriteBatch, mapObject, entityType);
				break;
			case KEY:
				createIdentifiableEntity(world, spriteBatch, mapObject, entityType);
				break;
			case KEYBLOCK:
				createIdentifiableEntity(world, spriteBatch, mapObject, entityType);
				break;
			case EAREA:
				entity = new PlayerWentBackSpawnPoint(world, spriteBatch, mapObject);
				entities.add(entity);
				gameFile.setEndPos(entity.getBody().getPosition());
				break;
			case SEAREA:
				entity = new SecretPlayerWentBackSpawnPoint(world, spriteBatch, mapObject);
				entities.add(entity);
				gameFile.setSEndPos(entity.getBody().getPosition());
				break;
			case SWITCHBLOCK:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case GOAL:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case BGOAL:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case SGOAL:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case SBGOAL:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case RED:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case GREEN:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case BLUE:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case EMERALD:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case SAPPHIRE:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case HEART:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case EDGE:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case SPIKE:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case TIMERBLOCK:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case MOVINGPLATTFORM:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case DIRUP:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case DIRDOWN:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case DIRRIGHT:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case DIRLEFT:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case DIRNULL:
				createSimpleEntity(world, spriteBatch, mapObject, entityType);
				break;
			case SHOPTRIGGER:
				createTrigger(world, spriteBatch, mapObject, Triggertype.SHOPTRIGGER);
				break;
			case BOSSTRIGGER:
				createTrigger(world, spriteBatch, mapObject, Triggertype.BOSSTRAPTRIGGER);
				break;
			case REMOVEBLOCK:
				entity = new RemovedByTriggerBlock(world, spriteBatch, mapObject);
				removeBlocks.add(entity);
				entities.add(entity);
				break;
			case SICIBOSSBLOCKADE:
				if(!gameFile.bossDefeated(BigSiciBoss.class)){
					entity = new BossBlockade(world, spriteBatch, mapObject);
					bossBlocks.add(entity);
					entities.add(entity);
					break;
				}
				break;
			default:
				throw new IllegalArgumentException("Entity Type not available");
		}
	}

	public Trigger createTrigger(World world, SpriteBatch sb, MapObject mo, Triggertype triggertype){
		Trigger trigger = new Trigger(world, sb, mo, triggertype);
		entities.add(trigger);
		return trigger;
	}

	
	public Entity createSimpleEntity(World world, SpriteBatch sb, MapObject mo, EntityType entityType){
		try {
			Class<?> blueprint = Class.forName(entityType.blueprint.toString().split(" ")[1]);
			Entity entity = (Entity) blueprint.getConstructor(
					World.class, SpriteBatch.class, MapObject.class).newInstance(world, sb, mo);
			entities.add(entity);
			return entity;
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
				IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
				SecurityException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("Entity not created: " + entityType.toString());
	}
	
	public Enemy createEnemy(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles,
			boolean facesRight, EntityType entityType){
		try {
			Class<?> blueprint = Class.forName(entityType.blueprint.toString().split(" ")[1]);
			Enemy enemy = (Enemy) blueprint.getConstructor(
					World.class, SpriteBatch.class, MapObject.class, Array.class, boolean.class)
					.newInstance(world, sb, mo, activeProjectiles, facesRight);
			inactiveEnemies.add(enemy);
			return enemy;
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
				IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
				SecurityException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("Entity not created: " + entityType.toString());
	}
	
	public IdentifiableEntity createIdentifiableEntity(World world, SpriteBatch sb, MapObject mo, EntityType entityType){
		try {
			constructionID++;
			if(!gameFile.isItemActivated(constructionID)){
				Class<?> blueprint = Class.forName(entityType.blueprint.toString().split(" ")[1]);
				IdentifiableEntity entity = (IdentifiableEntity) blueprint.getConstructor(
						World.class, SpriteBatch.class, MapObject.class, int.class)
						.newInstance(world, sb, mo, constructionID);
				entities.add(entity);
				return entity;
			}else{
				return null;
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
				IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
				SecurityException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("Entity not created: " + entityType.toString());
	}

	public void createBoss(World world, SpriteBatch sb, MapObject mo, Array<Projectile> activeProjectiles,
							 boolean facesRight, EntityType entityType){
		bossDefeated = gameFile.bossDefeated(entityType.blueprint);
		if(!bossDefeated){
			// System.out.println("created Boss " + entityType.blueprint.toString().split(" ")[1]);
			try {
				Class<?> blueprint = Class.forName(entityType.blueprint.toString().split(" ")[1]);
				Enemy enemy = (Enemy) blueprint.getConstructor(
						World.class, SpriteBatch.class, MapObject.class, Array.class, EntityFactory.class, GameFile.class, boolean.class)
						.newInstance(world, sb, mo, activeProjectiles, this, gameFile, facesRight);
				bosses.add(enemy);
				return;
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException |
					IllegalArgumentException | InvocationTargetException | NoSuchMethodException |
					SecurityException e) {
				e.printStackTrace();
			}
			throw new RuntimeException("Entity not created: " + entityType.toString());
		}
	}

	public boolean bossDefeated(){
		return bossDefeated;
	}
}
