package de.d4v3z02.BitBunny.handlers.levelhandlers;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.entities.characters.enemies.Enemy;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Checkpoint;
import de.d4v3z02.BitBunny.entities.entitiesonmap.TrapBlock;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.informer.enums.EntityType;

import javax.rmi.CORBA.Util;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class EntityService {

    ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    private static final long serialVersionUID = 1L;
    private static EntityService singleInstance;
    private Array<Enemy> inactiveEnemies;
    private EntityFactory entityFactory;
    private Array<Entity> entities = new Array<>();
    private DropFactory dropFactory;

    private EntityService() {

    }

    public void add(Entity entity) {
        this.entities.add(entity);
    }

    public static EntityService getInstance() {
        if (singleInstance == null) {
            singleInstance = new EntityService();
        }
        return singleInstance;
    }

    public void createEntities(GameFile gameFile, Array<Projectile> activeEnemyProjectiles, World world, SpriteBatch sb, TiledMapHandler tiledMapHandler) throws IllegalArgumentException {
        entities.clear();
        inactiveEnemies = new Array<>();
        entityFactory = new EntityFactory(world, sb, gameFile, entities, inactiveEnemies, activeEnemyProjectiles);
        for (EntityType entityType : EntityType.values()) {
            MapLayer layer = tiledMapHandler.getTiledMap().getLayers().get(entityType.layerName);
            if (layer == null) {
                continue;
            }
            for (MapObject mapObject : layer.getObjects()) {
                entityFactory.createEntity(entityType, mapObject);
            }
        }
        dropFactory = new DropFactory(world, sb, gameFile, entities);
        if (entityFactory.bossDefeated()) {
            dropFactory.removeBossTriggers();
        }
    }

    public void renderEntities() {
        for (Entity entity : entities) {
            entity.render();
        }
    }

    public void updateEntities(float dt, World world) {
        //long nt1 = System.nanoTime();
        for (Entity entity : entities) {
            Body body = entity.getBody();
            entity.update(dt);
            if (entity.shouldBeRemoved()) {
                try {
                    dropFactory.dropItem(entity);
                    entities.removeValue(entity, true);
                    body.setUserData(null);
                    world.destroyBody(body);
                } catch (Exception e) {
                    System.err.println("failed to update entity: " + entity);
                    e.printStackTrace();
                }
            }
        }
        //System.out.println(System.nanoTime() - nt1 + " sequentiel");
    }

    /*
    parallel update: too slow at the moment due to synchronization, useful if updating becomes more demanding
    public void updateEntities(float dt, World world) {
        long nt1 = System.nanoTime();
        List<Future<Boolean>> list = new ArrayList<>();
        for (Entity entity : entities) {
            list.add(executor.submit(() -> {
                Body body = entity.getBody();
                entity.update(dt);
                if (entity.shouldBeRemoved()) {
                    try {
                        dropFactory.dropItem(entity);
                        entities.removeValue(entity, true);
                        body.setUserData(null);
                        world.destroyBody(body);
                    } catch (Exception e) {
                        System.err.println("failed to update entity: " + entity);
                        e.printStackTrace();
                        return false;
                    }
                }
                return true;
            }));
        }
        for (Future<Boolean> task : list){
            try {
                task.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println(System.nanoTime() - nt1 + " parallel");
    }
    */

    public void activateInactiveEnemies(Player player) {
        Vector2 playerPos = player.getPosition();
        Vector2 enemyPos;
        Iterator<Enemy> iterator = inactiveEnemies.iterator();
        while (iterator.hasNext()) {
            Enemy enemy = iterator.next();
            enemyPos = enemy.getBody().getPosition();
            if (playerPos.x + 20 > enemyPos.x && playerPos.y + 20 > enemyPos.y) {
                entities.add(enemy);
                enemy.activate();
                iterator.remove();
            }
        }
    }

    public EntityFactory getEntityFactory() {
        return entityFactory;
    }

    public Entity getCheckpoint(int checkpointID){
        for(Entity entity: entities){
            if(entity.getClass() == Checkpoint.class){
                if(((Checkpoint)entity).getID() == checkpointID){
                    return entity;
                }
            }
        }
        return null;
    }

    public void deactivateOtherCheckpoints(int checkpointID){
        for(Entity entity: entities){
            if(entity.getClass() == Checkpoint.class){
                Checkpoint cp = (Checkpoint) entity;
                if(cp.getID() != checkpointID){
                    cp.deactivate();
                }
            }
        }
    }

    public void activateTrapBlocks(){
        for(Entity entity: entities) {
            if (entity.getClass() == TrapBlock.class){
                ((TrapBlock) entity).activate();
            }
        }
    }
}
