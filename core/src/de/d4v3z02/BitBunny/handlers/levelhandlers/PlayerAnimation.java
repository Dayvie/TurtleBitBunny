package de.d4v3z02.BitBunny.handlers.levelhandlers;

public enum PlayerAnimation {

	BLUE("res/images/bunnies.png", 32, 32, 0 , 1, 3), TEAL("res/images/bunnies.png", 32, 32, 0, 0, 3), ORANGE("res/images/bunnies.png", 32, 32, 0 , 2, 3),
	WHITE("res/images/bunnies.png", 32, 32, 0 , 4, 3), BLACK("res/images/bunnies.png", 32, 32, 0 , 5, 3);

	private PlayerAnimation(String textureMap, int widthInPixel, int heightInPixel, int indexX, int spriteMapIndexY, int lastSpriteIndex) {
		this.textureMap = textureMap;
		this.widthInPixel = widthInPixel;
		this.heightInPixel = heightInPixel;
		this.indexX = indexX;
		this.spriteMapIndexY = spriteMapIndexY;
		this.lastSpriteIndex = lastSpriteIndex;
	}

	public String textureMap;
	public int widthInPixel;
	public int heightInPixel;
	public int indexX;
	public int spriteMapIndexY;
	public int lastSpriteIndex;

}
