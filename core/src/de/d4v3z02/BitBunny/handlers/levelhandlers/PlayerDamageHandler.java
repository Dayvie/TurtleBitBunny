package de.d4v3z02.BitBunny.handlers.levelhandlers;

import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.entities.characters.enemies.Enemy;
import de.d4v3z02.BitBunny.informer.enums.ElementalTypes;

public class PlayerDamageHandler {
	
	private Player player;
	private Enemy enemy;
	private static PlayerDamageHandler playerDamageHandler;
	private boolean playerGetsBodyDamage;
	private PlayerDamageHandler(){
		
	}
	
	public static PlayerDamageHandler getInstance(){
		if(playerDamageHandler == null){
			playerDamageHandler  = new PlayerDamageHandler();
		}
		return playerDamageHandler;
	}
	
	public void handleBodyDamage(Player player, Enemy enemy){
		playerGetsBodyDamage = true;
		this.player = player;
		this.enemy = enemy;
	}
	
	public void finishHandlingDamage(){
		playerGetsBodyDamage = false;
	}
	
	public void update (float dt){
		if(playerGetsBodyDamage){
			player.decreaseHP(enemy.getBodyDmg(), ElementalTypes.PHYSICAL);
		}
	}
}
