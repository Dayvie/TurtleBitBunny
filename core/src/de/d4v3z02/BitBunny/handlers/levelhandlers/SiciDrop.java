package de.d4v3z02.BitBunny.handlers.levelhandlers;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;
import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Emerald;
import de.d4v3z02.BitBunny.entities.entitiesonmap.EntityOnMap;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Sapphire;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Topaz;
import de.d4v3z02.BitBunny.entities.entitiesonmap.powerup.Heart;

import java.util.Random;

/**
 * Created by dayvie on 30.06.2016.
 */
public class SiciDrop extends Drop {

    public SiciDrop(Entity entity, World world, SpriteBatch sb){
        super(entity, world, sb);
    }

    public void dropItem(){
        handleSiciDrops(x, y);
    }

    private void handleSiciDrops(float x, float y){
        Random rng = new Random();
        EntityOnMap drop;
        int randomNumber = rng.nextInt(8);
        switch(randomNumber){
            case 0:
                drop = new Heart(world, sb, x, y);
                drop.jump();
                entities.add(drop);
                break;
            case 1:
                drop = new Emerald(world, sb, x, y);
                drop.jump();
                entities.add(drop);
                break;
            case 2:
                drop = new Sapphire(world, sb, x, y);
                drop.jump();
                entities.add(drop);
                break;
            case 3:
                drop = new Emerald(world, sb, x, y);
                drop.jump();
                entities.add(drop);
                break;
            case 4:
                drop = new Emerald(world, sb, x, y);
                drop.jump();
                entities.add(drop);
                break;
            case 5:
                drop = new Sapphire(world, sb, x, y);
                drop.jump();
                entities.add(drop);
                break;
            case 6:
                drop = new Topaz(world, sb, x, y);
                drop.jump();
                entities.add(drop);
                break;
        }
    }
}
