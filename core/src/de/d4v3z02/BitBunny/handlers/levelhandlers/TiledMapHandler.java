package de.d4v3z02.BitBunny.handlers.levelhandlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by dayvie on 23.06.2016.
 */
public class TiledMapHandler {

    private TiledMap tiledMap;
    private int tiledMapWidth;
    private int tiledMapHeight;
    private int tileSize;
    private OrthogonalTiledMapRenderer orthogonalTiledMapRenderer;

    public TiledMapHandler(int levelNumber, World world){
        try {
            tiledMap = new TmxMapLoader().load("res/maps/level" + levelNumber + ".tmx");
        }
        catch(Exception e) {
            System.out.println("Cannot find file: res/maps/level" + levelNumber + ".tmx");
            Gdx.app.exit();
        } // gameFile.getLastLevel()
        tiledMapWidth = (int) getTiledMap().getProperties().get("width", int.class);
        tiledMapHeight = (int) getTiledMap().getProperties().get("height", int.class);
        tileSize = (int) getTiledMap().getProperties().get("tilewidth", int.class);
        orthogonalTiledMapRenderer = new OrthogonalTiledMapRenderer(getTiledMap());
        BlockCreator blockMaker = new BlockCreator(getTiledMap(), world);
        blockMaker.createAllBlocks();
    }

    public TiledMap getTiledMap() {
        return tiledMap;
    }

    public int getTiledMapWidth() {
        return tiledMapWidth;
    }

    public int getTiledMapHeight() {
        return tiledMapHeight;
    }

    public int getTileSize() {
        return tileSize;
    }

    public OrthogonalTiledMapRenderer getOrthogonalTiledMapRenderer() {
        return orthogonalTiledMapRenderer;
    }
}
