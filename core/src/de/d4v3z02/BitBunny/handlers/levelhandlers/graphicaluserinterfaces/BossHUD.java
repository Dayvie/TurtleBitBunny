package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.entities.characters.enemies.Boss;
import de.d4v3z02.BitBunny.entities.characters.enemies.Enemy;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.informer.enums.Fonts;
import de.d4v3z02.BitBunny.main.GameManager;

/**
 * Created by dayvie on 04.07.2016.
 */
public class BossHUD extends IngameUserInterface{

    private Enemy boss;
    private final String bossName;
    private TextureAtlas.AtlasRegion fullheart;
    private int firstRow;
    private TextureRegion[] font;
    private ContentManager contentManager;
    private int secondRow;

    public BossHUD(UserInterfaceManager userInterfaceManager, Player player, Enemy boss){
        super(userInterfaceManager, player);
        this.contentManager = ContentManager.getInstance();
        this.boss = boss;
        fullheart = ((TextureAtlas) contentManager
                .get("res/images/hearts.pack")).findRegion("hud_heartFull");
        firstRow = UI.firstRow.value;
        secondRow = UI.secondRow.value;
        Texture tex = contentManager.get("res/images/hud.png", Texture.class);
        font = new TextureRegion[11];
        for(int i = 0; i < 6; i++) {
            font[i] = new TextureRegion(tex, 32 + i * 9, 16, 9, 9);
        }
        for(int i = 0; i < 5; i++) {
            font[i + 6] = new TextureRegion(tex, 32 + i * 9, 25, 9, 9);
        }
        bossName = ((Boss)boss).getName();
    }

    @Override
    public void render() {
        int x = 1400;
        spriteBatch.begin();
        Fonts.BOSS.font.draw(spriteBatch, bossName, x, firstRow+UI.objectSize.value);
        spriteBatch.draw(fullheart, x, secondRow, UI.objectSize.value, UI.objectSize.value);
        drawString(spriteBatch, ""+boss.getHP(), x+100, secondRow);
        spriteBatch.end();
    }

    @Override
    public void handleInput() {

    }

    private void drawString(SpriteBatch sb, String s, float x, float y) {
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(c == '/') c = 10;
            else if(c >= '0' && c <= '9') c -= '0';
            else continue;
            sb.draw(font[c], x + i * UI.objectSize.value /2, y + UI.objectSize.value /5f, UI.objectSize.value /2, UI.objectSize.value /2);
        }
    }
}
