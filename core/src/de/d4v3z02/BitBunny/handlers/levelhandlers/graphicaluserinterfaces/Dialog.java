package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.informer.enums.Fonts;
import de.d4v3z02.BitBunny.informer.enums.KeyValues;
import de.d4v3z02.BitBunny.main.GameManager;

/**
 * Created by dayvie on 01.07.2016.
 */
public class Dialog extends IngameUserInterface {

    private String dialogContent;
    private Texture texture;

    public Dialog(UserInterfaceManager userInterfaceManager, Player player, String dialogContent){
        super(userInterfaceManager, player);
        this.dialogContent = dialogContent;
        this.texture = ContentManager.getInstance().get("res/images/textfield.png", Texture.class);
        player.freeze();
    }

    @Override
    public void render() {
        Vector2 pos = player.getPosition();
        spriteBatch.begin();
        int boxHeight = 200;
        spriteBatch.draw(texture, 0, GameManager.GAME_WORLD_HEIGHT*2/3, GameManager.GAME_WORLD_WIDTH, boxHeight);
        Fonts.SHOP.font.draw(spriteBatch, dialogContent, 50, GameManager.GAME_WORLD_HEIGHT*2/3+boxHeight-25);
        spriteBatch.end();
    }

    @Override
    public void handleInput(){
        if(inputHandler.isPressed(KeyValues.JUMP)){
            player.unfreeze();
            userInterfaceManager.closeLastGUI();
        }
    }
}
