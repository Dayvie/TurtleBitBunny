package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.handlers.levelhandlers.CombatText;

public class FloatingCombatTextHolder{
	
	private static FloatingCombatTextHolder singleton;
	private Array<CombatText> texts;
	
	private FloatingCombatTextHolder(){
		texts = new Array<>();
	}
	
	public static FloatingCombatTextHolder getInstance(){
		if(singleton == null){
			singleton = new FloatingCombatTextHolder();
		}
		return singleton;
	}
	
	public void update(float dt){
		for(CombatText floatingText: texts){
			floatingText.update(dt);
			if(floatingText.shouldBeRemoved()){
				texts.removeValue(floatingText, true);
			}
		}
	}
	
	public void clear(){
		texts.clear();
	}
	
	public void render(){
		for(CombatText floatingNumber: texts){
			floatingNumber.render();
		}
	}
	
	public void add(CombatText combatText){
		texts.add(combatText);
	}
}
