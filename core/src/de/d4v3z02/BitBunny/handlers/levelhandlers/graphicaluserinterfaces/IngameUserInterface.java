package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.handlers.inputhandlers.InGameInputHandler;

public abstract class IngameUserInterface {

	protected InGameInputHandler inputHandler;
	protected UserInterfaceManager userInterfaceManager;
	protected SpriteBatch spriteBatch;
	protected Player player;
	
	public IngameUserInterface(UserInterfaceManager userInterfaceManager, Player player){
		this.userInterfaceManager = userInterfaceManager;
		this.inputHandler = InGameInputHandler.getInstance();
		this.spriteBatch = userInterfaceManager.getSpriteBatch();
		this.player = player;
	}
	
	public void update(float dt){
		handleInput();
	}
	public abstract void render();
	public abstract void handleInput();
}
