package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.levelhandlers.EntityFactory;
import de.d4v3z02.BitBunny.main.GameManager;

public class PlayerHUD {
	
	private GameFile gameFile;
	private Player player;
	private TextureRegion[] blocks;
	private TextureRegion crystal;
	private Texture goldenCrystal;
	private TextureRegion[] font;
	private SpriteBatch spriteBatch;
	private ContentManager cm;
	private TextureAtlas heartAtlas;
	private AtlasRegion fullHeart;
	private AtlasRegion emptyHeart;
	private TextureRegion yellowKey;
	private final int firstRow;
	private final int secondRow;
	private int xOffset;
	private EntityFactory entityFactory;
	
	public PlayerHUD(Player player, SpriteBatch sb, EntityFactory entityFactory){
		this.entityFactory = entityFactory;
		xOffset = 100;
		firstRow = UI.firstRow.value;
		secondRow = UI.secondRow.value;
		this.player = player;
		this.spriteBatch = sb;
		this.gameFile = player.getGameFile();
		cm = ContentManager.getInstance();
		cm.preLoadSafely("res/maps/BOSSMAP.png", Texture.class);
		cm.preLoadSafely("res/images/goldencrystal.png", Texture.class);
		cm.preLoadSafely("res/images/hearts.pack",TextureAtlas.class);
		cm.preLoadSafely("res/images/hud.png",Texture.class);
		Texture tex = cm.get("res/images/hud.png", Texture.class);
		blocks = new TextureRegion[3];
		for (int i = 0; i < blocks.length; i++) {
			blocks[i] = new TextureRegion(tex, 32 + i * 16, 0 , 16, 16);
		}
		crystal = new TextureRegion(tex, 80, 0, 16, 16);
		font = new TextureRegion[11];
		for(int i = 0; i < 6; i++) {
			font[i] = new TextureRegion(tex, 32 + i * 9, 16, 9, 9);
		}
		for(int i = 0; i < 5; i++) {
			font[i + 6] = new TextureRegion(tex, 32 + i * 9, 25, 9, 9);
		}
		heartAtlas = cm.get("res/images/hearts.pack");
		fullHeart = heartAtlas.findRegion("hud_heartFull");
		emptyHeart = heartAtlas.findRegion("hud_heartEmpty");
		goldenCrystal = cm.get("res/images/goldencrystal.png");
		tex = cm.get("res/maps/BOSSMAP.png", Texture.class);
		yellowKey = new TextureRegion(tex, 34*16, 2*16, 16, 16);
	}
	
	private void renderHearts(){
		int heartSize = UI.objectSize.value;
		int offset = 400 + xOffset;
		int playerHP = player.getHP();
		int i = 0;
		for(i = 0; i< playerHP; i++){
			spriteBatch.draw(fullHeart, offset + i * heartSize, firstRow, heartSize, heartSize);
		}
		for(int j = 0; j< player.getMaxHp() - playerHP; j++){
			spriteBatch.draw(emptyHeart, offset + i * heartSize+ j * heartSize, firstRow, heartSize, heartSize);
		}
	}
	
	public void render(){
		spriteBatch.begin();
		renderHearts();
		int goldenCrystalXPos = xOffset+200;
		spriteBatch.draw(crystal, xOffset+ UI.objectSize.value /2, secondRow, UI.objectSize.value, UI.objectSize.value);
		spriteBatch.draw(crystal, xOffset+ UI.objectSize.value /2, firstRow, UI.objectSize.value, UI.objectSize.value);
		spriteBatch.draw(goldenCrystal, goldenCrystalXPos, secondRow, UI.objectSize.value, UI.objectSize.value);
		renderYellowKey();
		drawString(spriteBatch, gameFile.getCollectedCrystals() + " / " + entityFactory.getMaxCrystalCount(), xOffset+125, firstRow+3);
		drawString(spriteBatch, ""+gameFile.getPlayerCrystalNum(), xOffset + 125, secondRow+3);
		drawString(spriteBatch, ""+gameFile.getGold(), goldenCrystalXPos + 100, secondRow+3);
		spriteBatch.end();
	}
	
	private void renderYellowKey(){
		int goldenKeyXPos = xOffset+500;
		spriteBatch.draw(yellowKey, goldenKeyXPos, secondRow, UI.objectSize.value, UI.objectSize.value);
		drawString(spriteBatch, player.getGameFile().getKeyAmount()+"", goldenKeyXPos + 100, secondRow+3);
	}
	
	private void drawString(SpriteBatch sb, String s, float x, float y) {
		for(int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if(c == '/') c = 10;
			else if(c >= '0' && c <= '9') c -= '0';
			else continue;
			sb.draw(font[c], x + i * UI.objectSize.value /2, y + UI.objectSize.value /5f, UI.objectSize.value /2, UI.objectSize.value /2);
		}
	}
}