package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import de.d4v3z02.BitBunny.handlers.levelhandlers.CombatText;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class RisingCombatText extends CombatText {
	
	public RisingCombatText(String text, BitmapFont font, SpriteBatch sb, Vector2 position){
		super(text, font, sb, position);
	}
	
	@Override
	public void update(float dt){
		lifetime += dt;		
		if(lifetime > 2){
			removeFlag = true;
		}
	}
	
	@Override
	public void render(){
		sb.begin();
		font.draw(sb, combatText, position.x * Numbers.PPM.value + (lifeConstant* lifetime)/4 + 16,
			position.y * Numbers.PPM.value + (lifeConstant* lifetime) + 16 );
		sb.end();
	}
}
