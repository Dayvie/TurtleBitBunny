package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.informer.enums.Fonts;
import de.d4v3z02.BitBunny.informer.enums.KeyValues;
import de.d4v3z02.BitBunny.informer.enums.Numbers;
import de.d4v3z02.BitBunny.informer.enums.Shopitem;
import de.d4v3z02.BitBunny.main.GameManager;

public class ShopWindow extends IngameUserInterface {

	private Body body;
	private GameFile gameFile;
	private Array<Shopitem> items;
	private String pointer;	
	private int pointerIndex;
	private int itemsSize;
	private float playerx;
	private float playery;
	private float pointery;
	private Map<Float, Shopitem> ypos;
	
	public ShopWindow(Player player, UserInterfaceManager userinterFaceManager){
		super(userinterFaceManager, player);
		ypos = new HashMap<>();
		this.items = new Array<>();
		this.player = player;
		this.body = player.getBody();
		this.gameFile = player.getGameFile();
		player.freeze();
		loadItems(gameFile.getLastLevel());
		pointer = "-";
		playerx = GameManager.GAME_WORLD_WIDTH/2/Numbers.PPM.value;
		playery = GameManager.GAME_WORLD_HEIGHT/2/Numbers.PPM.value;
		pointery = playery;
		itemsSize = items.size-1;
		createUi();
	}
	
	private void loadItems(int level){
		if(level < 8){
			items.add(Shopitem.QUIT);
			items.add(Shopitem.HEART);
			if(!(gameFile.getLevelData().getBoughtItems().contains(Shopitem.HEARTCONTAINER))){
				items.add(Shopitem.HEARTCONTAINER);
			}
		}
		
	}
	
	private void createUi(){
		Float y = playery;
		for(Shopitem item: items){
			ypos.put(y, item);
			y += 32;
		}
	}
	
	private void buy(){
		Shopitem bought = ypos.get(pointery);
		if(player.spendGold(bought.costs)){
			switch(bought){
			case HEARTCONTAINER:
				player.collectHearthcontainer();
				gameFile.updateHP(player);
				gameFile.buyItem(Shopitem.HEARTCONTAINER);
				player.unfreeze();
				gameFile.saveGameFile();
				userInterfaceManager.closeLastGUI();
				break;
			case HEART:
				player.increaseHP(1);
				gameFile.updateHP(player);
				break;
			case QUIT:
				player.unfreeze();
				gameFile.saveGameFile();
				userInterfaceManager.closeLastGUI();
				break;
			default:
				break;
			}
		}
	}
	
	private void renderItems(){
		float y = playery;
		float x = playerx + 16;
		for(Shopitem item: items){
			Fonts.SHOP.font.draw(spriteBatch, item.name, x, y);
			if(item.costs != 0){
				Fonts.SHOP.font.draw(spriteBatch, ""+item.costs, x+270, y);
			}
			y += 32;
		}
	}

	@Override
	public void render() {
		spriteBatch.begin();
		Fonts.SHOP.font.draw(spriteBatch, pointer, playerx, pointery);
		renderItems();
		spriteBatch.end();
	}
	
	@Override
	public void handleInput() {
		if(inputHandler.isPressed(KeyValues.NEXT)){ //w
			if(pointerIndex+ 1<= itemsSize){
				pointerIndex +=1 ;
				pointery += 32;
			}
		}
		if(inputHandler.isPressed(KeyValues.SPECIAL)){ //s
			if(pointerIndex-1>= 0){
				pointerIndex -=1 ;
				pointery -= 32;
			}
		}
		if(inputHandler.isPressed(KeyValues.JUMP)){
			buy();
		}
	}
}
