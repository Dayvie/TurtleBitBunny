package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import de.d4v3z02.BitBunny.handlers.levelhandlers.CombatTextNumber;
import de.d4v3z02.BitBunny.informer.enums.Numbers;

public class StaticCombatTextNumber extends CombatTextNumber {

	public StaticCombatTextNumber(int number, BitmapFont font, SpriteBatch sb, Vector2 position) {
		super(number, font, sb, position);
	}
	
	public void update(float dt){
		lifetime += dt;		
		if(lifetime > 1){
			removeFlag = true;
		}
	}
	
	public void render(){
		sb.begin();
		font.draw(sb, this.combatText, position.x * Numbers.PPM.value + 16,
			position.y* Numbers.PPM.value + 32 );
		sb.end();
	}
}
