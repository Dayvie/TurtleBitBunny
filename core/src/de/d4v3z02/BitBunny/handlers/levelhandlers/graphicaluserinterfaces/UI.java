package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import de.d4v3z02.BitBunny.main.GameManager;

public enum UI {
		
	objectSize(64), bigObjectSize(64),
	firstRow((int) (GameManager.GAME_WORLD_HEIGHT-GameManager.GAME_WORLD_HEIGHT*0.12)),
	secondRow((int)(GameManager.GAME_WORLD_HEIGHT-GameManager.GAME_WORLD_HEIGHT*0.20));
	
	private UI(int value){
			this.value = value;
	}
	
	public final int value;
}
