/**
 * 
 */
package de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces;

import java.util.Stack;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.handlers.inputhandlers.InGameInputHandler;

/**
 * @author David
 *	Class for GUI'S which are triggered ingame
 */
public class UserInterfaceManager {

	private Stack<IngameUserInterface> activeUserInterfaces;
	private SpriteBatch spriteBatch;
	protected InGameInputHandler inputHandler;
	
	public UserInterfaceManager(Player player){
		this.spriteBatch = player.getSpriteBatch();
		inputHandler = InGameInputHandler.getInstance();
		activeUserInterfaces = new Stack<>();
	}
	
	public SpriteBatch getSpriteBatch(){
		return this.spriteBatch;
	}
	
	public void update(float dt){
		if(!activeUserInterfaces.isEmpty()){
			activeUserInterfaces.peek().update(dt);
		}
	}
	
	public void render(){
		if(!activeUserInterfaces.isEmpty()){
			activeUserInterfaces.peek().render();
		}
	}
	
	public void activateGUI(IngameUserInterface ui){
		activeUserInterfaces.push(ui);
	}
	
	public void closeLastGUI(){
		if(!activeUserInterfaces.isEmpty()){
			activeUserInterfaces.pop();
		}
	}
}
