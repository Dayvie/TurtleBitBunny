package de.d4v3z02.BitBunny.informer.enums;

/**
 * Created by dayvie on 23.06.2016.
 */
public enum Assets {

    BOSSMAP("res/maps/BOSSMAP.png"), GREENLANESONG("res/music/Green_Lane_Avenue.mp3"),
    BOWSERSONG("res/music/Bowser.mp3"), GEMS("res/images/gems.png"),
    BUNNIES("res/images/bunnies.png"), TEXTFIELD("res/images/textfield.png"),
    HEARTSATLAS("res/images/hearts.pack");

    public String path;

    Assets(String path){
        this.path = path;
    }
}
