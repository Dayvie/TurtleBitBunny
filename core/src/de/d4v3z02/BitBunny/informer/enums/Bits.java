package de.d4v3z02.BitBunny.informer.enums;

public enum Bits {
	GROUND((short)2), PLAYER((short)4), RED((short)8), GREEN((short)16),
	BLUE((short)32), CRYSTAL((short)64), POWERUP((short) 128), ENEMY((short)256),
	DEADLYCOLLISIONFORENEMY((short)512),
	PASSABLE((short)1024),  BOTTOM_PLATFORM((short)16), FOOT((short)2048), SENSOR((short)2048); 
	
	public final short value;
	
	private Bits(short i){
		this.value = i;
	}
}