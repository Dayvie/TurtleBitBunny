package de.d4v3z02.BitBunny.informer.enums;


public enum BlockTypes {
	 GRAY("gray", Bits.GROUND), PASSABLE("passable", Bits.PASSABLE);
	
	public final String loadName;
	public final Bits categoryBit;
	
	private BlockTypes(String loadName, Bits categoryBit){
		this.loadName = loadName;
		this.categoryBit = categoryBit;
	}
}