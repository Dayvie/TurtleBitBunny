package de.d4v3z02.BitBunny.informer.enums;
public enum CharacterState {
	STANDING, RUNNING, WALKING, FROZEN, INVINCIBLE;
}