package de.d4v3z02.BitBunny.informer.enums;

import com.badlogic.gdx.graphics.g2d.BitmapFont;

public enum ElementalTypes {
	PHYSICAL(Fonts.ENEMYDAMAGE.font), FIRE(Fonts.FIRE.font), FROST(Fonts.FROST.font),
	POISON(Fonts.POISON.font), CURSE(Fonts.CURSE.font);
	
	public BitmapFont font;
	
	private ElementalTypes(BitmapFont font){
		this.font = font;
	}
}