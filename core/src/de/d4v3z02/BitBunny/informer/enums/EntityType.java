package de.d4v3z02.BitBunny.informer.enums;



import de.d4v3z02.BitBunny.entities.characters.enemies.*;
import de.d4v3z02.BitBunny.entities.entitiesonmap.BlueBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.BossBlockade;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Checkpoint;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Emerald;
import de.d4v3z02.BitBunny.entities.entitiesonmap.GreenBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.MovingPlattform;
import de.d4v3z02.BitBunny.entities.entitiesonmap.RedBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.RemovedByTriggerBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.TrapBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Sapphire;
import de.d4v3z02.BitBunny.entities.entitiesonmap.Spike;
import de.d4v3z02.BitBunny.entities.entitiesonmap.SwitchBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.TimerBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.Edge;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.Goal;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.PlayerGoesBackSecretExit;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.PlayerGoesBackSensor;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.PlayerWentBackSpawnPoint;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.SecretPlayerWentBackSpawnPoint;
import de.d4v3z02.BitBunny.entities.entitiesonmap.areas.SecretZoneGoal;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.Crystal;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.Heartcontainer;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.Key;
import de.d4v3z02.BitBunny.entities.entitiesonmap.identifiableentity.KeyBlock;
import de.d4v3z02.BitBunny.entities.entitiesonmap.powerup.BlueCherry;
import de.d4v3z02.BitBunny.entities.entitiesonmap.powerup.FireCherry;
import de.d4v3z02.BitBunny.entities.entitiesonmap.powerup.Heart;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggers.Trigger;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionDown;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionLeft;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionNull;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionRight;
import de.d4v3z02.BitBunny.entities.entitiesonmap.triggersold.DirectionUp;

/**
 * @author David Yesil
 * An EntityType represents the name of the entity and the name in the map editor. Maps maplayers  in the editor
 * to classes.
 */
public enum EntityType{
	// Convention: 2 types per line
	CRYSTAL("crystal", Crystal.class), SICI("sici", Sici.class),
	JUMPINGSICI("jsici", JumpingSici.class), RUNNINGSICI("rsici", RunningSici.class),
	BAT("fsici", Bat.class), SHOOTINGSICI("ssici", ShootingSici.class),
	SHOOTINGSICIRIGHT("srsici", ShootingSici.class),
	WALKINGSHOOTINGSICI("wssici", WSSici.class), CHECKPOINT("cp", Checkpoint.class),
	SWITCHBLOCK("sb", SwitchBlock.class), FIRECHERRY("firecherry", FireCherry.class),
	SICIBOSS("bsici", BigSiciBoss.class), BLUECHERRY("bc", BlueCherry.class),
	HEARTCONTAINER("hc", Heartcontainer.class), GOAL("goal", Goal.class),
	BGOAL("bgoal", PlayerGoesBackSensor.class), SGOAL("sgoal", SecretZoneGoal.class),
	SBGOAL("sbgoal", PlayerGoesBackSecretExit.class), EAREA("earea", PlayerWentBackSpawnPoint.class),
	SEAREA("searea", SecretPlayerWentBackSpawnPoint.class), RED("red", RedBlock.class),
	BLUE("blue", BlueBlock.class), GREEN("green", GreenBlock.class),
	EMERALD("em", Emerald.class), SAPPHIRE("saph", Sapphire.class),
	HEART("heart", Heart.class), EDGE("edge", Edge.class),
	SPIKE("spike", Spike.class), MOVINGPLATTFORM("mp", MovingPlattform.class),
	DIRUP("up", DirectionUp.class), DIRDOWN("down", DirectionDown.class),
	DIRLEFT("left", DirectionLeft.class), DIRRIGHT("right", DirectionRight.class),
	DIRNULL("null", DirectionNull.class), TIMERBLOCK("tb", TimerBlock.class),
	REMOVEBLOCK("rb", RemovedByTriggerBlock.class), SICIBOSSBLOCKADE("bb", BossBlockade.class),
	BATLEFT("flsici", Bat.class), KEY("key", Key.class), KEYBLOCK("keyblock", KeyBlock.class),
	BATRIGHT("lfsici", Bat.class), SHOPTRIGGER("shoptrigger", Trigger.class),
	SHOOTINGSICIBOSS("shootingsiciboss", ShootingSiciBoss.class), BOSSTRIGGER("bosstrigger", Trigger.class),
	TRAPBLOCK("trap", TrapBlock.class);
	
	public final String layerName;
	public final Class<?> blueprint;
	
	private EntityType(String layerName, Class<?> blueprint){
		this.layerName = layerName;
		this.blueprint = blueprint;
	}
}