package de.d4v3z02.BitBunny.informer.enums;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.UI;

public enum Fonts {
	/*
	 * Class for Fonts which are often used ingame and therefore make sense to be static
	 * Convention: Backspace after 3 fonts
	 */
	DAMAGE(15, Color.RED), HEAL(15, Color.GREEN), FIRE(15, Color.ORANGE),
	CURSE(15, Color.PURPLE), ENEMYDAMAGE(15, Color.WHITE), PLAYERMONEY(15, Color.YELLOW),
	POISON(15, Color.GREEN), FROST(15, Color.CYAN), PHYSICAL(15, Color.GRAY),
	SHOP(30, Color.WHITE), BOSS(UI.objectSize.value, Color.RED);
	
	private Fonts(int size, Color innerColor){
		this.font = buildFont(size, innerColor);
	}
	
	public int size;
	public BitmapFont font;
	
	private BitmapFont buildFont(int size, Color innerColor){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("res/images/Courneuf-Outline.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		parameter.shadowColor = Color.WHITE;
		parameter.color = innerColor;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		BitmapFont font = generator.generateFont(parameter);
        generator.dispose();
        return font;
	}
}