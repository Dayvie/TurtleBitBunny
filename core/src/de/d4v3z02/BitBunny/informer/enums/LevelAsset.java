package de.d4v3z02.BitBunny.informer.enums;

import com.badlogic.gdx.math.Vector2;

public enum LevelAsset {

	/*
	 * Convention: ENUM(Background, Background Music, Spawnposition)
	 */
	FIRST("res/images/bgs.png", "res/music/Green_Lane_Avenue.mp3", new Vector2(60 / Numbers.PPM.value, 120/ Numbers.PPM.value)),
	SECOND("res/images/bgs.png", "res/music/Green_Lane_Avenue.mp3", null),
	THIRD("res/images/bgsevening.png", "res/music/Green_Lane_Avenue.mp3", new Vector2(60 / Numbers.PPM.value, 120/ Numbers.PPM.value)),
	FOURTH(null, null, null),
	FIFTH("res/images/bgsevening.png", "res/music/Green_Lane_Avenue.mp3", new Vector2(80 / Numbers.PPM.value, 80/ Numbers.PPM.value)),
	SIXTH("res/images/bgsevening.png", "res/music/Bowser.mp3", new Vector2(1100 / Numbers.PPM.value, 80/ Numbers.PPM.value)),
	SEVENTH("res/images/bgs.png", "res/music/Wind.mp3", new Vector2(60 / Numbers.PPM.value, 50/ Numbers.PPM.value)),
	EIGTH("res/images/bgsevening.png", "res/music/Bowser.mp3", new Vector2(860 / Numbers.PPM.value, 80/ Numbers.PPM.value));
	
	public Vector2 spawn;
	public String bg;
	public String music;
	
	LevelAsset(String bg, String music, Vector2 spawn){
		this.spawn = spawn;
		this.bg = bg;
		this.music = music;
	}
}