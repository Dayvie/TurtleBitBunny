package de.d4v3z02.BitBunny.informer.enums;
/**
 * @author David
 *	PPM: Pixel per Meter
 *	STEP: FPS
 */
public enum Numbers {
	
	PPM(100f), STEP(1 / 60f), CHARACTERSPEED(1f), CHARACTERRUNNINGSPEED(2f),
	SAWNIC(2f), HALFTILESIZE_WORLD(0.16f);
	
	public final float value;
	
	private Numbers(float dt){
		this.value = dt;
	}
}
