package de.d4v3z02.BitBunny.informer.enums;

public enum Shopitem{
	
	QUIT("Quit Shop", 0, ItemLimitation.NONE), HEART("Heart", 15, ItemLimitation.NONE),
	HEARTCONTAINER("Heartcontainer", 200, ItemLimitation.UNIQUE);
	
	public static int IDcounter = 0;
	public String name;
	public int costs;
	public int ID;
	public ItemLimitation limitation;
	
	private Shopitem(String name, int costs, ItemLimitation limitation){
		this.name = name;
		this.costs = costs;
		this.ID = generateID();
		this.limitation = limitation;
	}
	
	private int generateID(){
		int ID = IDcounter;
		IDcounter++;
		return ID;
	}
	
	enum ItemLimitation{
		NONE, LIMITED, UNIQUE;
	}
}
