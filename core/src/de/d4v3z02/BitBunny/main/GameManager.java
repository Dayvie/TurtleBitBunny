package de.d4v3z02.BitBunny.main;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.d4v3z02.BitBunny.handlers.BoundedCamera;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.inputhandlers.InGameInputProcessor;
import de.d4v3z02.BitBunny.informer.enums.LevelAsset;
import de.d4v3z02.BitBunny.states.PlayScreen;
import de.d4v3z02.BitBunny.states.TitleScreen;

public class GameManager extends Game {
	
	public static final String TITLE = "BitBunny";
	public static final int VIDEO_WIDTH = 1920	;
	public static final int VIDEO_HEIGHT = 1080;
	public static final int GAME_WORLD_WIDTH = 1920; //320
	public static final int GAME_WORLD_HEIGHT = 1080; //240
	
	private LevelAsset currentAsset;
	private SpriteBatch spriteBatch;
	private Viewport viewport;
	private BoundedCamera gameCam;
	private OrthographicCamera hudCam;
	private ContentManager contentManager = ContentManager.getInstance();
	private FPSLogger fpsLogger = new FPSLogger();
	private InGameInputProcessor inputProcessor = InGameInputProcessor.getInstance();

	public void create() {
		inputProcessor.registerGameScreenManager(this);
		Gdx.input.setInputProcessor(inputProcessor);
		spriteBatch = new SpriteBatch();
		gameCam = new BoundedCamera();
		gameCam.setToOrtho(false, GAME_WORLD_WIDTH, GAME_WORLD_HEIGHT);
		hudCam = new OrthographicCamera();
		hudCam.setToOrtho(false, GAME_WORLD_WIDTH, GAME_WORLD_HEIGHT);	
		viewport = new ExtendViewport(GAME_WORLD_WIDTH, GAME_WORLD_HEIGHT, gameCam);
		viewport.apply();
		setScreen(new TitleScreen(this));
	}

	public void startLevel(GameFile gameFile){
		chooseAssetSet(gameFile);
		this.setScreen(new PlayScreen(this, gameFile, currentAsset));
	}
	
	public LevelAsset getCurrentAsset(){
		return currentAsset;
	}
	
	private void chooseAssetSet(GameFile gameFile){
		int levelAssetNumber = 0;
		for(LevelAsset levelAsset: LevelAsset.values()){
			levelAssetNumber++;
			if(levelAssetNumber == gameFile.getLastLevel()){
				this.currentAsset = levelAsset;
				contentManager.loadLevel(currentAsset);
				return;
			}
			if(levelAssetNumber == 1000){
				throw new RuntimeException("LevelAsset not found");
			}
		}
	}
	
	public void render() {	
		super.render();
		fpsLogger.log();
	}
	
	public void dispose() {
		contentManager.clear();
		spriteBatch.dispose();
		this.screen.hide();
		this.screen.dispose();
	}
	
	public SpriteBatch getSpriteBatch() {
		return spriteBatch;
	}
	
	public Viewport getViewport(){
		return this.viewport;
	}
	
	public BoundedCamera getCamera() {
		return gameCam;
	}
	
	public OrthographicCamera getHUDCamera() {
		return hudCam;
	}
	
	public void resize(int w, int h) {
	}
	
	public void pause() {
		
	}
	
	public void resume() {
		
	}	
}