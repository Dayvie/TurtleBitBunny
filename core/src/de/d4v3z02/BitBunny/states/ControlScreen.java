package de.d4v3z02.BitBunny.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.main.GameManager;

public class ControlScreen extends GameScreen {

	private TextureAtlas atlas;
	private TextButtonStyle textButtonStyle;
	private ContentManager cm;
	private Stage stage;
	private TextButton gameFileScreenButton;
	private MusicHandler mh;
	private Label keyboardControlsTitle;
	private Array<Label> keyboardControls;
	private Array<Label> keyboardControlsKeys;
	private Array<Image> buttonImages;
	private BitmapFont font;
	private LabelStyle labelStyle;
	private float keyControlsY;
	
	public ControlScreen(GameManager gameScreenManager, TextButtonStyle textButtonStyle) {
		super(gameScreenManager);
		buttonImages = new Array<>();
		this.keyboardControlsKeys = new Array<>();
		this.keyboardControls = new Array<>();
		this.textButtonStyle = textButtonStyle;
		keyControlsY = gameManager.GAME_WORLD_HEIGHT/2-50;
		mh = MusicHandler.getInstance();
		cm = ContentManager.getInstance();
		stage = new Stage(gameManager.getViewport());
		cm.preLoadSafely("res/images/buttons.pack", TextureAtlas.class);
		atlas = cm.get("res/images/buttons.pack");
		Gdx.input.setInputProcessor(stage);
	}
	
	private void buildFont(int size){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("res/images/Courneuf-Outline.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		parameter.shadowColor = Color.WHITE;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
        generator.dispose();
	}

	@Override
	public void show() {
		buildFont(50);
		labelStyle = new LabelStyle(font, Color.WHITE);
		generateGameFileButton();
		createKeyBoardControlsLabel();
		buildFont(30);
		labelStyle = new LabelStyle(font, Color.WHITE);
		createKeyBoardControls();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 255, 255, 1);
		update(delta);
		stage.draw();
	}
	
	private  void update(float dt){
		stage.act(dt);
	}
	
	private void createKeyBoardControls(){
		createButtonDescriptions();
		createKeyboardKeys();
		createButtonImages();
	}
	
	private void createButtonDescriptions(){
		String[] gameMechanicsDescriptions = new String[4];
		gameMechanicsDescriptions[0] = "RUN";
		gameMechanicsDescriptions[1] = "JUMP";
		gameMechanicsDescriptions[2] = "SWITCHMODE";
		gameMechanicsDescriptions[3] = "USEMODE";
		float y = keyControlsY;
		for(int i = 0; i < gameMechanicsDescriptions.length;i++){
			Label label  = new Label(gameMechanicsDescriptions[i], labelStyle);
			label.setPosition(gameManager.GAME_WORLD_WIDTH/2+200, y, 1);
	        stage.addActor(label);
	        keyboardControls.add(label);
	        y+=40;
		}
	}
	
	private void createKeyboardKeys(){
		String[] keysKeyBoard = new String[4];
		keysKeyBoard[0] = "L-SHIFT";
		keysKeyBoard[1] = "SPACE";
		keysKeyBoard[2] = "W";
		keysKeyBoard[3] = "S";
		float y = keyControlsY;
		for(int i = 0; i < keysKeyBoard.length;i++){
			Label label  = new Label(keysKeyBoard[i], labelStyle);
			label.setPosition(gameManager.GAME_WORLD_WIDTH/2, y, 1);
	        stage.addActor(label);
	        keyboardControlsKeys.add(label);
	        y+=40;
		}
	}
	
	private void createButtonImages(){
		Image[] buttonImages = new Image[4];
		AtlasRegion a = atlas.findRegion("xboxControllerButtonA");
		AtlasRegion b = atlas.findRegion("xboxControllerButtonB");
		AtlasRegion x = atlas.findRegion("xboxControllerButtonX");
		AtlasRegion yButton = atlas.findRegion("xboxControllerButtonY");
		buttonImages[0] = new Image(x);
		buttonImages[1] = new Image(a); 
		buttonImages[2] = new Image(yButton);
		buttonImages[3] = new Image(b);
		float y = keyControlsY-14;
		for(int i = 0; i < buttonImages.length;i++){
			buttonImages[i].setScale(0.35f);
			buttonImages[i].setPosition(gameManager.GAME_WORLD_WIDTH/2-150, y);
	        stage.addActor(buttonImages[i]);
	        this.buttonImages.add(buttonImages[i]);
	        y+=40;
		}
	}
	
	private void createKeyBoardControlsLabel(){
		LabelStyle style = new LabelStyle(font, Color.WHITE);
		keyboardControlsTitle  = new Label("Controls", style);
		keyboardControlsTitle.setPosition(gameManager.GAME_WORLD_WIDTH/2, gameManager.GAME_WORLD_HEIGHT-100, 1);
        stage.addActor(keyboardControlsTitle);
	}
	
	private void generateGameFileButton(){
		gameFileScreenButton = new TextButton("Gamefiles", textButtonStyle);
		gameFileScreenButton.setWidth(150);
		gameFileScreenButton.setHeight(50);
		gameFileScreenButton.setPosition(gameManager.GAME_WORLD_WIDTH/2-350, 100, 1);
		gameFileScreenButton.addListener(new ClickListener(){
			public void clicked(InputEvent e, float x , float y){
				mh.playSound((Music)cm.get("res/sfx/crystal.mp3"), 0.4f);
					gameManager.setScreen(new SelectGameFileScreen(gameManager));
			}
		});
		stage.addActor(gameFileScreenButton);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}
}