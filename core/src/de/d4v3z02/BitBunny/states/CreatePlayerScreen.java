package de.d4v3z02.BitBunny.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;

import de.d4v3z02.BitBunny.entities.characters.Player.PowerUpState;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFileManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.main.GameManager;

public class CreatePlayerScreen extends GameScreen {
	
	private GameFileManager gfm;
	private BitmapFont font;
	private TextField createPlayerField;
	private Stage stage;
	private Label label;
	private MusicHandler mh;
	private ContentManager cm;
	
	public CreatePlayerScreen(GameManager gameScreenManager) {
		super(gameScreenManager);
		gfm = GameFileManager.getInstance();
		cm = ContentManager.getInstance();
		buildFont(40);
		mh = MusicHandler.getInstance();
		stage = new Stage(gameManager.getViewport());
		Gdx.input.setInputProcessor(stage);
		TextFieldStyle textFieldStyle = new TextFieldStyle();
		Skin skin = new Skin();
		Pixmap pixmap = new Pixmap(100, 100, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));
		textFieldStyle.cursor = skin.newDrawable("white", Color.DARK_GRAY);
		textFieldStyle.selection = skin.newDrawable("white", Color.CYAN);
		textFieldStyle.background = skin.newDrawable("white", Color.DARK_GRAY);
		textFieldStyle.messageFont = font;
		textFieldStyle.font = font;
		textFieldStyle.fontColor = Color.WHITE;
		createPlayerField = new TextField("", textFieldStyle);
		createPlayerField.setMaxLength(24);
		createPlayerField.setWidth(1000);
		createPlayerField.setHeight(100);
		createPlayerField.setPosition(gameManager.GAME_WORLD_WIDTH/2, 200, 1);
		createPlayerField.setTextFieldListener(new TextFieldListener(){
			@Override
			public void keyTyped(TextField textField, char c) {
				if(c == '\r'){
					mh.playSound((Music)cm.get("res/sfx/crystal.mp3"), 0.4f);
					String playerName = replaceInvalidCharacters(textField.getText());
					gfm.addGameFile(playerName, new GameFile(playerName, 1, 1, PowerUpState.NONE));
					gameManager.setScreen(new SelectGameFileScreen(gameManager));
				}
			}
		});
		stage.addActor(createPlayerField);
		buildFont(160);
		createLabel();
	}
	
	private String replaceInvalidCharacters(String testMe){
		String newtestMe = testMe.replaceAll("^[.\\\\/:*?\"<>|]?[\\\\/:*?\"<>|]*", "x");
		return newtestMe.substring(1);
	}
	
	private void buildFont(int size){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("res/images/Courneuf-Outline.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		parameter.shadowColor = Color.WHITE;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
        generator.dispose();
	}
	
	private void createLabel(){
		LabelStyle style = new LabelStyle(font, Color.WHITE);
        label  = new Label("Choose a Playername", style);
        label.setPosition(gameManager.GAME_WORLD_WIDTH/2, gameManager.GAME_WORLD_HEIGHT-100, 1);
        stage.addActor(label);
	}

	@Override
	public void show() {
	}

	@Override
	public void render(float dt) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 255, 255, 1);
		update(dt);
		stage.draw();
	}
	
	private void update(float dt){
		stage.act(dt);
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}

}