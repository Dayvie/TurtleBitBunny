package de.d4v3z02.BitBunny.states;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import de.d4v3z02.BitBunny.handlers.BoundedCamera;
import de.d4v3z02.BitBunny.main.GameManager;

public abstract class GameScreen implements Screen{
	
	protected GameManager gameManager;
	protected SpriteBatch sb;
	protected BoundedCamera gameCam;
	protected OrthographicCamera hudCam;
	
	public GameScreen(GameManager gameScreenManager) {
		this.gameManager = gameScreenManager;
		sb = gameScreenManager.getSpriteBatch();
		gameCam = gameScreenManager.getCamera();
		hudCam = gameScreenManager.getHUDCamera();
	}
}