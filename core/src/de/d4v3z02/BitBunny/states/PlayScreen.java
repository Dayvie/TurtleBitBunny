package de.d4v3z02.BitBunny.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.d4v3z02.BitBunny.entities.Entity;
import de.d4v3z02.BitBunny.entities.Projectile;
import de.d4v3z02.BitBunny.entities.characters.Player;
import de.d4v3z02.BitBunny.handlers.BoundedCamera;
import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFile.SpawnPosition;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFileManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.handlers.inputhandlers.InGameInputHandler;
import de.d4v3z02.BitBunny.handlers.inputhandlers.InGameInputProcessor;
import de.d4v3z02.BitBunny.handlers.levelhandlers.*;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.FloatingCombatTextHolder;
import de.d4v3z02.BitBunny.handlers.levelhandlers.graphicaluserinterfaces.PlayerHUD;
import de.d4v3z02.BitBunny.informer.enums.CharacterState;
import de.d4v3z02.BitBunny.informer.enums.LevelAsset;
import de.d4v3z02.BitBunny.informer.enums.Numbers;
import de.d4v3z02.BitBunny.main.GameManager;

public class PlayScreen extends GameScreen {

	private final ColorHolder blockColorHolder = ColorHolder.getInstance();
	private final MusicHandler musicHandler = MusicHandler.getInstance();
    private final InGameInputHandler inGameInputHandler = InGameInputHandler.getInstance();
    private final ContactManager contactManager = ContactManager.getInstance();
    private final ContentManager contentManager = ContentManager.getInstance();
    private final FloatingCombatTextHolder floatingCombatTextHolder = FloatingCombatTextHolder.getInstance();
    private final PlayerDamageHandler playerDamageHandler = PlayerDamageHandler.getInstance();
    private final GameFileManager gameFileManager = GameFileManager.getInstance();
    private final EntityService entityService = EntityService.getInstance();

    private final Array<Projectile> activeEnemyProjectiles = new Array<>();

	private boolean debugModeOn;
	private World world = new World(new Vector2(0, -9.81f), true);
	private Box2DDebugRenderer debugRenderer;
	private BoundedCamera degbugCam;
	private Player player;
	private PlayerHUD hud;
	private BackgroundRenderer backgroundRenderer;
	private Array<Projectile> activePlayerProjectiles;
	private Viewport viewport;
	private Pool<Projectile> fireballpool;
	private GameFile gameFile;

	private LevelAsset levelAsset;
	private TiledMapHandler tiledMapHandler;

	public PlayScreen(GameManager gameScreenManager, GameFile gameFile, LevelAsset levelAsset) {
		super(gameScreenManager);
		this.levelAsset = levelAsset;
		this.gameFile = gameFile;
		viewport = gameScreenManager.getViewport();
		Gdx.input.setInputProcessor(InGameInputProcessor.getInstance());
		world.setContactListener(contactManager);
		tiledMapHandler = new TiledMapHandler(gameFile.getLastLevel(), world);
		if (debugModeOn) {
			degbugCam = new BoundedCamera();
			debugRenderer = new Box2DDebugRenderer();
		}
		buildLevel();
		playMusic();
		buildPlayer();
		inGameInputHandler.registerPlayer(player);

		hud = new PlayerHUD(player, this.sb, this.entityService.getEntityFactory());
	}

	private void buildPlayer() {
		player = new Player(world, sb, gameFile);
		contactManager.setPlayer(player);
		fireballpool = player.getFireBallPool();
		activePlayerProjectiles = player.getActiveProjectiles();
	}

	private void playMusic() {
		if (musicHandler.isPlayingMusicEqual(gameManager.getCurrentAsset().music)) {
			return;
		}
		musicHandler.stopMusic();
		musicHandler.playLoopingMusic(gameManager.getCurrentAsset().music);
		musicHandler.setVolume(0.2f);
		contentManager.preLoadSafely("res/sfx/smallFireball.mp3", Music.class);
	}

	private void buildLevel() throws IllegalArgumentException {
		gameCam.setBounds(0, tiledMapHandler.getTiledMapWidth() * tiledMapHandler.getTileSize(), 0,
				tiledMapHandler.getTiledMapHeight() * tiledMapHandler.getTileSize());
		this.entityService.createEntities(gameFile, activeEnemyProjectiles, world, sb, tiledMapHandler);
		backgroundRenderer = new BackgroundRenderer(levelAsset, gameCam, sb);
		if (debugModeOn) {
			setCam();
		}
	}

	private void setCam() {
		degbugCam.setToOrtho(false, GameManager.GAME_WORLD_WIDTH / Numbers.PPM.value,
				GameManager.GAME_WORLD_HEIGHT / Numbers.PPM.value);
		degbugCam.setBounds(0, (tiledMapHandler.getTiledMapWidth() * tiledMapHandler.getTileSize()) / Numbers.PPM.value,
				0, (tiledMapHandler.getTiledMapHeight() * tiledMapHandler.getTileSize()) / Numbers.PPM.value);
	}

	private void updateProjectiles(float dt) {
		for (Projectile fireball : activePlayerProjectiles) {
			if (fireball.shouldBeRemoved()) {
				activePlayerProjectiles.removeValue(fireball, false);
				fireballpool.free(fireball);
			} else {
				fireball.update(dt);
			}
		}
		for (Projectile fireball : activeEnemyProjectiles) {
			if (fireball.shouldBeRemoved()) {
				activeEnemyProjectiles.removeValue(fireball, false);
				fireball.getBody().setUserData(null);
				world.destroyBody(fireball.getBody());
				// fireball = null;
			} else {
				fireball.update(dt);
			}
		}
	}

	private void renderProjectiles() {
		for (Projectile fireball : activePlayerProjectiles) {
			fireball.render();
		}
		for (Projectile fireball : activeEnemyProjectiles) {
			fireball.render();
		}
	}

	public void update(float dt) {
		contactManager.update(dt);
		inGameInputHandler.update();
		world.step(Numbers.STEP.value, 6, 3);
		if (player.getState() != CharacterState.FROZEN) {
			this.entityService.updateEntities(dt, world);
		}
		floatingCombatTextHolder.update(dt);
		contentManager.update();
		player.update(dt);
		checkForPlayerDeath();
		checkForPlayerLeavingScreen();
		this.entityService.activateInactiveEnemies(player);
		updateProjectiles(dt);
		playerDamageHandler.update(dt);
	}

	public void dispose() {
		if (debugModeOn) {
			debugRenderer.dispose();
		}
		world.dispose();
	}

	private void checkForPlayerDeath() {
		if (player.getBody().getPosition().y < 0) {
			musicHandler.playSound(contentManager.get("res/sfx/hit.wav", Music.class));
			restartLevel();
		}
		if (player.shouldBeRemoved()) {
			musicHandler.playSound(contentManager.get("res/sfx/hit.wav", Music.class));
			restartLevel();
		}
	}

	private void restartLevel() {
		gameFile.updateHP(player);
		gameFile.fillHP(player);
		gameFile.saveGameFile();
		blockColorHolder.reset();
		floatingCombatTextHolder.clear();
		contactManager.resetContacts();
		gameManager.setScreen(new PlayScreen(gameManager, gameFile, levelAsset));
	}

	private void savePlayerData(int progress) {
		gameFile.setLastLevel(gameFile.getLastLevel() + progress);
		gameFile.setLastPowerUp(player.getPowerUp());
		gameFile.updateHP(player);
		gameFileManager.saveAll();
	}

	private void checkForPlayerLeavingScreen() {
		switch (contactManager.playerGoes()) {
		case AHEAD:
			playerTriggersExit();
			break;
		case BACK:
			playerGoesBackFromNormalLevel();
			break;
		case NOWHERE:
			return;
		case SECRET:
			playerGoesBackFromSecretLevel();
			break;
		case SECRETBACK:
			playerTriggersSecretExit();
			break;
		default:
			break;
		}
	}

	private void playerTriggersSecretExit() {
		gameFile.setSpawnPosition(SpawnPosition.SECRETEND);
		savePlayerData(-1);
		universialLevelEnding();
	}

	private void playerGoesBackFromSecretLevel() {
		gameFile.setSpawnPosition(SpawnPosition.NORMAL);
		savePlayerData(1);
		universialLevelEnding();
	}

	private void playerGoesBackFromNormalLevel() {
		gameFile.setSpawnPosition(SpawnPosition.END);
		savePlayerData(-2);
		universialLevelEnding();
	}

	private void playerTriggersExit() {
		gameFile.setSpawnPosition(SpawnPosition.NORMAL);
		savePlayerData(2);
		universialLevelEnding();
	}

	private void universialLevelEnding() {
		gameFile.setCheckpointID(0);
		contactManager.resetContacts();
		activePlayerProjectiles.clear();
		fireballpool.clear();
		blockColorHolder.reset();
		floatingCombatTextHolder.clear();
		gameManager.startLevel(gameFile);
	}

	@Override
	public void resize(int w, int h) {
		viewport.update(w, h, true);
		gameCam.setPosition(player.getPosition().x * Numbers.PPM.value + gameCam.viewportWidth / 4,
				gameCam.viewportHeight / 2);
	}

	@Override
	public void render(float deltaTime) {
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gameCam.update();
		gameCam.setPosition(player.getPosition().x * Numbers.PPM.value + gameCam.viewportWidth / 4,
				player.getPosition().y * Numbers.PPM.value);
		update(deltaTime);
		sb.setProjectionMatrix(hudCam.combined);
		backgroundRenderer.render();
		tiledMapHandler.getOrthogonalTiledMapRenderer().setView(gameCam);
		tiledMapHandler.getOrthogonalTiledMapRenderer().render();
		sb.setProjectionMatrix(gameCam.combined);
		player.render();
		entityService.renderEntities();
		floatingCombatTextHolder.render();
		renderProjectiles();
		sb.setProjectionMatrix(hudCam.combined);
		hud.render();
		contactManager.render();
		if (debugModeOn) {
			degbugCam.setPosition(player.getPosition().x + GameManager.GAME_WORLD_WIDTH / 4 / Numbers.PPM.value,
					GameManager.GAME_WORLD_HEIGHT / 2 / Numbers.PPM.value);
			degbugCam.update();
			debugRenderer.render(world, degbugCam.combined);
		}
	}

	@Override
	public void show() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}
}