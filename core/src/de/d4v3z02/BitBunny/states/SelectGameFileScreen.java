package de.d4v3z02.BitBunny.states;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.GameFileManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.main.GameManager;

public class SelectGameFileScreen extends GameScreen {

	private GameFileManager gameFileManager;
	private TextButtonStyle textButtonStyle;
	private BitmapFont font;
	private Stage stage;
	private Array<TextButton> playerSaves;
	private Array<TextButton> deleteButtons;
	private TextButton createPlayer;
	private TextButton controls;
	private ContentManager contentManager;
	private MusicHandler musicHandler;
	private int yBottomRow;
	private int buttonHeight;
	
	public SelectGameFileScreen(GameManager gameScreenManager) {
		super(gameScreenManager);
		buttonHeight = 100;
		yBottomRow = buttonHeight/2;
		contentManager = ContentManager.getInstance();
		musicHandler = MusicHandler.getInstance();
		gameFileManager = GameFileManager.getInstance(); 
		buildSkin();
		stage = new Stage(gameManager.getViewport());
		Gdx.input.setInputProcessor(stage);
	}
	
	private void buildDeleteButtons(){
		deleteButtons = new Array<TextButton>(playerSaves.size);
		int y = 400;
		for (TextButton playerSaveButton : playerSaves) {
			TextButton deleteButton = new TextButton("DEL", textButtonStyle);
			deleteButton.setWidth(100);
			deleteButton.setHeight(buttonHeight);
			deleteButton.setPosition(gameManager.GAME_WORLD_WIDTH/2+340, y, 1);
			deleteButton.setName("del " + playerSaveButton.getName());
			deleteButtons.add(deleteButton);
			y+=120;
		}
	}
	
	private void buildPlayerSaveButtons(){
		List<String> allPlayerNames = gameFileManager.getAllPlayerNames();
		playerSaves = new Array<TextButton>(allPlayerNames.size());
		int y = 400;
		for (String s : allPlayerNames) {
			TextButton b = new TextButton(s, textButtonStyle);
			b.setWidth(500);
			b.setHeight(buttonHeight);
			b.setPosition(gameManager.GAME_WORLD_WIDTH/2, y, 1);
			b.setName(s);
			playerSaves.add(b);
			y+=120;
		}
	}
	
	private void buildSkin(){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("res/images/Courneuf-Outline.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 40;
		parameter.shadowColor = Color.WHITE;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
		font.getData().markupEnabled = true;
		textButtonStyle = new TextButtonStyle();
		Skin skin = new Skin();
		Pixmap pixmap = new Pixmap(100, 100, Format.RGBA8888);
		pixmap.setColor(Color.WHITE);
		pixmap.fill();
		skin.add("white", new Texture(pixmap));
		textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.down = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.checked = skin.newDrawable("white", Color.BLUE);
		textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
		textButtonStyle.font = font;
        generator.dispose();
	}

	@Override
	public void render(float dt) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 255, 255, 1);
		update(dt);
		stage.draw();
	}
	
	private void update(float dt){
		stage.act(dt);
	}
	
	@Override
	public void show() {
		contentManager.preLoadSafely("res/music/lobby.mp3", Music.class);
		musicHandler.playLoopingMusic("res/music/lobby.mp3");
		musicHandler.setVolume(0.05f);
		generateControlButton();
		geneRateQuitButton();
		buildPlayerSaveButtons();
		showAllSaveStateButtons();
		buildDeleteButtons();
		showAllDeleteButtons();
		generateCreatePlayerButton();
	}
	
	private void generateCreatePlayerButton(){
		contentManager.preLoadSafely("res/sfx/crystal.mp3", Music.class);
		createPlayer = new TextButton("Create Player", textButtonStyle);
		createPlayer.setWidth(500);
		createPlayer.setHeight(buttonHeight);
		createPlayer.setPosition(gameManager.GAME_WORLD_WIDTH/2, yBottomRow, 1);
		createPlayer.addListener(new ClickListener(){
			public void clicked(InputEvent e, float x , float y){
				musicHandler.playSound((Music)contentManager.get("res/sfx/crystal.mp3"), 0.4f);
				if(playerSaves.size < 4 ){
					gameManager.setScreen(new CreatePlayerScreen(gameManager));
				}
			}
		});
		stage.addActor(createPlayer);
	}
	
	private void generateControlButton(){
		contentManager.preLoadSafely("res/sfx/crystal.mp3", Music.class);
		controls = new TextButton("Controls", textButtonStyle);
		controls.setWidth(300);
		controls.setHeight(buttonHeight);
		controls.setPosition(gameManager.GAME_WORLD_WIDTH/2+800, yBottomRow, 1);
		controls.addListener(new ClickListener(){
			public void clicked(InputEvent e, float x , float y){
				musicHandler.playSound((Music)contentManager.get("res/sfx/crystal.mp3"), 0.4f);
				if(playerSaves.size < 4 ){
					gameManager.setScreen(new ControlScreen(gameManager, textButtonStyle));
				}
			}
		});
		stage.addActor(controls);
	}

	private void geneRateQuitButton(){
		contentManager.preLoadSafely("res/sfx/crystal.mp3", Music.class);
		controls = new TextButton("Quit Game", textButtonStyle);
		controls.setWidth(300);
		controls.setHeight(buttonHeight);
		controls.setPosition(gameManager.GAME_WORLD_WIDTH/2-800, yBottomRow, 1);
		controls.addListener(new ClickListener(){
			public void clicked(InputEvent e, float x , float y){
				musicHandler.playSound((Music)contentManager.get("res/sfx/crystal.mp3"), 0.4f);
				if(playerSaves.size < 4 ){
					Gdx.app.exit();
				}
			}
		});
		stage.addActor(controls);
	}
	
	private void showAllDeleteButtons(){
		contentManager.preLoadSafely("res/sfx/hit.wav", Music.class);
		int i = 0;
		List<String> playerNames = gameFileManager.getAllPlayerNames();
		for(TextButton delButton : deleteButtons){
			String playerName = playerNames.get(i);
			delButton.addListener(new ClickListener(){
				public void clicked(InputEvent e, float x , float y){
					gameFileManager.deleteGameFile(playerName);
					stage.unfocus(delButton);
					delButton.remove();
					musicHandler.playSound(contentManager.get("res/sfx/hit.wav"), 0.4f);
					delButton.clear();
					deleteButtons.removeValue(delButton, false);
					for (TextButton textButton : playerSaves) {
						if(textButton.getName().equals(playerName)){
							stage.unfocus(textButton);
							textButton.remove();
							textButton.clear();
							playerSaves.removeValue(textButton, false);	
						}
					}
				}		
			});
			i++;
			stage.addActor(delButton);
		}
	}
	
	private  void showAllSaveStateButtons(){
		for(final TextButton playerSave : playerSaves){
			playerSave.addListener(new ClickListener(){
				public void clicked(InputEvent e, float x , float y){
					musicHandler.stopMusic();
					gameManager.startLevel(gameFileManager.getGameFile(playerSave.getName()));
				}
			});
			stage.addActor(playerSave);
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {	
		deleteButtons.clear();
		playerSaves.clear();
	}
}