package de.d4v3z02.BitBunny.states;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

import de.d4v3z02.BitBunny.handlers.filehandlers.ContentManager;
import de.d4v3z02.BitBunny.handlers.filehandlers.MusicHandler;
import de.d4v3z02.BitBunny.handlers.inputhandlers.InGameInputHandler;
import de.d4v3z02.BitBunny.handlers.inputhandlers.InGameInputProcessor;
import de.d4v3z02.BitBunny.informer.enums.KeyValues;
import de.d4v3z02.BitBunny.main.GameManager;

public class TitleScreen extends GameScreen {

	private Image bigAssBunny;
	private Stage stage;
	private Music music;
	private Label titleLabel;
	private Label startLabel;
	private BitmapFont font;
	private ContentManager cm;
	private InGameInputProcessor inGameInputProcessor;
	private InGameInputHandler inGameInputHandler;
	private Image aButton;
	private TextureAtlas atlas;
	private Texture tex;
	private float accumulator;
	private boolean gameReady;
	private MusicHandler mh;
	private Label madeBy;
	
	public TitleScreen(GameManager gameScreenManager) {
		super(gameScreenManager);
		this.stage = new Stage(gameManager.getViewport());
		mh = MusicHandler.getInstance();
		inGameInputProcessor = InGameInputProcessor.getInstance();
		Gdx.input.setInputProcessor(inGameInputProcessor);
		cm = ContentManager.getInstance();
		inGameInputHandler = InGameInputHandler.getInstance();
		cm.load("res/images/buttons.pack", TextureAtlas.class);
		cm.load("res/images/bockt2.png", Texture.class);
		cm.load("res/images/hud.png", Texture.class);
		cm.load("res/music/Chemical.mp3", Music.class);
		cm.finishLoading();
		music = cm.get("res/music/Chemical.mp3");
		mh.playLoopingMusic(music);
		mh.setVolume(0.05f);
		atlas = cm.get("res/images/buttons.pack");
		AtlasRegion atlasRegion = atlas.findRegion("xboxControllerButtonA");
		aButton = new Image(atlasRegion);
		tex = cm.get("res/images/bockt2.png");
		bigAssBunny = new Image(tex);
		createStartLabelStyle();
		createTitleLabelStyle();
		createMadeByLabelStyle();
	}
	
	private void createTitleLabelStyle(){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("res/images/Courneuf-Outline.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 200;
		parameter.shadowColor = Color.WHITE;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
		font.getData().markupEnabled = true;
        LabelStyle style = new LabelStyle(font, Color.WHITE);
        titleLabel  = new Label("32-BIT BUNNY", style);
        generator.dispose();
	}
	
	private void createStartLabelStyle(){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("res/images/Courneuf-Outline.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 160;
		parameter.shadowColor = Color.WHITE;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
		font.getData().markupEnabled = true;
        LabelStyle style = new LabelStyle(font, Color.WHITE);
        startLabel  = new Label("PRESS   OR Space", style);
        generator.dispose();
	}
	
	private void createMadeByLabelStyle(){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("res/images/Courneuf-Outline.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 60;
		parameter.shadowColor = Color.WHITE;
		parameter.color = Color.WHITE;
		parameter.borderColor = Color.BLACK;
		parameter.borderWidth = 2;
		font = generator.generateFont(parameter);
		font.getData().markupEnabled = true;
        LabelStyle style = new LabelStyle(font, Color.WHITE);
        madeBy  = new Label("Made by D4V3Z02", style);
        generator.dispose();
	}

	@Override
	public void show() {
		stage.addActor(madeBy);
		madeBy.setPosition((float) (gameManager.GAME_WORLD_WIDTH*0.85), 100, 3);
		stage.addActor(bigAssBunny);
		bigAssBunny.setOrigin(bigAssBunny.getWidth()*1.3f, bigAssBunny.getHeight());
		bigAssBunny.addAction(sequence(fadeOut(0), alpha(1), scaleTo(.1f, .1f),
                parallel(fadeIn(2f, Interpolation.pow2),
                        scaleTo(2f, 2f, 2.5f, Interpolation.pow5),
                        moveTo(stage.getWidth() / 2 , stage.getHeight() / 2 , 2f, Interpolation.swing)),
                delay(1.5f), fadeOut(1.25f), fadeIn(2f, Interpolation.pow2)));//, run(transitionRunnable)
		stage.addActor(startLabel);
		stage.addActor(titleLabel);
		startLabel.setPosition(gameManager.GAME_WORLD_WIDTH/2, gameManager.GAME_WORLD_HEIGHT/3.2f, 3);
		startLabel.addAction(sequence(fadeOut(0), delay(7), fadeIn(1), forever(sequence(fadeOut(1), fadeIn(1)))));
		titleLabel.addAction(sequence(fadeOut(0), delay(7), fadeIn(1)));
		aButton.addAction(sequence(fadeOut(0), delay(7), fadeIn(1), forever(sequence(fadeOut(1), fadeIn(1)))));
		titleLabel.setPosition(gameManager.GAME_WORLD_WIDTH/2, gameManager.GAME_WORLD_HEIGHT/1.2f, 3);
		stage.addActor(aButton);
		aButton.setScale(1.2f);
		aButton.setPosition(gameManager.GAME_WORLD_WIDTH/2.22f, gameManager.GAME_WORLD_HEIGHT/3.78f, 3);
	}

	@Override
	public void render(float dt) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 255, 255, 1);
		update(dt);
		stage.draw();
	}
	
	private void update(float dt){
		stage.act(dt);
		handleInput();
		cm.update();
		inGameInputHandler.update();
		accumulate(dt);
	}
	
	private void accumulate(float dt){
		if(!gameReady){
			accumulator += dt;
			if (accumulator > 0.8f){
				accumulator = 0;
				gameReady = true;
			}
		}			
	}
	
	private void handleInput(){
		if(gameReady){
			if(inGameInputHandler.isPressed(KeyValues.JUMP)){
				mh.stopMusic();
	            gameManager.setScreen(new SelectGameFileScreen(this.gameManager));
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose(){
		bigAssBunny.setVisible(false);
		bigAssBunny.clearActions();
		bigAssBunny.clear();
		bigAssBunny.remove();
		atlas.dispose();
		music.stop();
		tex.dispose();
		music.dispose();
		cm.clear();
		stage.clear();
	}
}