package de.d4v3z02.BitBunny.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.d4v3z02.BitBunny.main.GameManager;

public class Launcher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = GameManager.VIDEO_WIDTH;
		config.height = GameManager.VIDEO_HEIGHT;
		config.title = GameManager.TITLE;
		config.fullscreen = false;
		config.vSyncEnabled = true;
		new LwjglApplication(new de.d4v3z02.BitBunny.main.GameManager(), config);
	}
}
